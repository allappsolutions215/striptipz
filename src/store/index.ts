import {all} from 'redux-saga/effects';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import createSagaMiddleware from 'redux-saga';

//axios interceptor
import axiosInterceptor from '../package/axios/axiosInterceptor';

//Reducer
import authReducer from './Auth/AuthReducer';
import userReducer from './User/UserReducer';
import profileReducer from './Profile/ProfileReducer';
import dashboardReducer from './Dashboard/DashboardReducer';
import paymentReducer from './Payment/PaymentReducer';
import notificationReducer from './Notification/NotificationReducer';

import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, persistReducer} from 'redux-persist';

//Combine reducers
export const rootReducer = combineReducers({
  auth: authReducer,
  user: userReducer,
  profile: profileReducer,
  dashboard: dashboardReducer,
  payment: paymentReducer,
  notification: notificationReducer
});

import {
  signInSaga,
  signUpSaga,
  verifyOtpSaga,
  forgotPassStep1Saga,
  forgotPassStep3Saga,
  registerProfileSaga,
  changePasswordSaga,
} from './Auth/AuthSaga';

import {userSaga} from './User/UserSaga';

import {
  notificationSaga,
  deleteNotificationSaga
} from './Notification/NotificationSaga';

import {
  addPhotoSaga,
  updateDancerProfileSaga,
  updatePatronProfileSaga,
  showHeadersSaga,
  getDancerDetailsSaga,
  getPhotosSaga,
  getMyProfileSaga,
  changeUserType
} from './Profile/ProfileSaga';

import {
  addAmountSaga,
  dancerMoneyTransfSaga,
  getAvailableWalletBalSaga,
  patronTransHistorySaga,
  tipByPatronSaga,
  getPPAccessTokenSaga,
  getPaymentDataSaga,
  createProductForSubscriptionSaga,
  createSubscriptionPlanSaga,
  createSubscriptionSaga,
  addTransactionSaga,
  connentDisconnectPayPalSaga,
  createSubscriptionToServerSaga,
  cancelSubscriptionSaga,
} from './Payment/PaymentSaga';

import {
  getDancersListSaga,
  getTipsListSaga,
  getUserTypeSaga,
  updateLocationSaga,
  onlineOfflineSaga,
} from './Dashboard/DashboardSaga';
//Reducer Saga
export function* rootSaga() {
  yield all([
    //AUTH SAGAs
    signInSaga(),
    signUpSaga(),
    verifyOtpSaga(),
    forgotPassStep1Saga(),
    forgotPassStep3Saga(),
    registerProfileSaga(),
    changePasswordSaga(),
    //USER SAGAs
    userSaga(),
    //Notification Saga
    notificationSaga(),
    deleteNotificationSaga(),
    //PROFILE SAGAs
    addPhotoSaga(),
    updateDancerProfileSaga(),
    updatePatronProfileSaga(),
    showHeadersSaga(),
    getDancerDetailsSaga(),
    getPhotosSaga(),
    getMyProfileSaga(),
    changeUserType(),
    //DASHBOARD SAGAs
    getDancersListSaga(),
    getTipsListSaga(),
    getUserTypeSaga(),
    updateLocationSaga(),
    onlineOfflineSaga(),
    //PAYMENT SAGAS
    addAmountSaga(),
    dancerMoneyTransfSaga(),
    getAvailableWalletBalSaga(),
    patronTransHistorySaga(),
    tipByPatronSaga(),
    getPPAccessTokenSaga(),
    getPaymentDataSaga(),
    createProductForSubscriptionSaga(),
    createSubscriptionPlanSaga(),
    createSubscriptionSaga(),
    addTransactionSaga(),
    connentDisconnectPayPalSaga(),
    createSubscriptionToServerSaga(),
    cancelSubscriptionSaga(),
  ]);
}

// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: 'root',
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  whitelist: ['user'],
  // Blacklist (Don't Save Specific Reducers)
  //blacklist: ['auth']
};
// Middleware: Redux Persist Persisted Reducer
const persistedReducer = persistReducer(persistConfig, rootReducer);

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  persistedReducer,
  compose(applyMiddleware(sagaMiddleware)),
);

// then run the saga
sagaMiddleware.run(rootSaga);

//register axios interceptor
axiosInterceptor(store.dispatch);

//load pre data
//calling here but we need tot call this on splash
//to collect all static data beign used in app

// TODO: dispatcher to store data from async storage to store

export default store;
