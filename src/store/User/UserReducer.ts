import {UserActionTypes} from './UserAction';

interface StateI {
  userResponse: any;
  userError: any;

  addTipDenominationResponse: any;
  addTipDenominationError: any;
}

const initialState: StateI = {
  userResponse: undefined,
  userError: undefined,

  addTipDenominationResponse: undefined,
  addTipDenominationError: undefined,
};

const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    case UserActionTypes.USER_SUCCESS:
      return {
        ...state,
        userResponse: action.payload,
        userError: undefined,
      };

    case UserActionTypes.USER_FAILURE:
      return {
        ...state,
        userResponse: undefined,
        userError: action.payload,
      };

      default:
      return state;
  }
};
export default reducers;
