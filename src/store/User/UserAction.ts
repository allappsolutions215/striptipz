export const UserActionTypes = {
  USER_REQUEST: 'USER_REQUEST',
  USER_SUCCESS: 'USER_SUCCESS',
  USER_FAILURE: 'USER_FAILURE',
};

const userAction = (payload: any) => {
  return {type: UserActionTypes.USER_REQUEST, payload};
};


export default {
  userAction,
};
