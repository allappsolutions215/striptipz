import {call, put, takeLatest} from 'redux-saga/effects';

import {UserActionTypes} from './UserAction';

// -------------*******------------- USER YIELD -------------*******------------- \\
function* userYield(action: any) {
  try {

    yield put({
      type: UserActionTypes.USER_SUCCESS,
      payload: action.payload,
    });
    
  } catch (error) {
    console.log('ERROR ****** userYield ', error.response);
    yield put({
      type: UserActionTypes.USER_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* userSaga() {

  yield takeLatest(UserActionTypes.USER_REQUEST, userYield);
}
