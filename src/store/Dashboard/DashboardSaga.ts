import {call, put, takeLatest} from 'redux-saga/effects';

import {DashboardActionTypes} from './DashboardAction';
import {DashboardApiRequest} from './DashboardApi';

// -------------*******------------- GET DASHBOARD FOR PATRON (DANCERS LIST) YIELD -------------*******------------- \\
function* getDancersListYield(action: any) {
  try {
    const res = yield call(
      DashboardApiRequest.getDancersListRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ getDancersListYield', res);
    if (res && res.status) {
      yield put({
        type: DashboardActionTypes.GET_DANCERS_LIST_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: DashboardActionTypes.GET_DANCERS_LIST_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getDancersListYield ', error.response);
    yield put({
      type: DashboardActionTypes.GET_DANCERS_LIST_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getDancersListSaga() {
  yield takeLatest(
    DashboardActionTypes.GET_DANCERS_LIST_REQUEST,
    getDancersListYield,
  );
}

// -------------*******------------- GET DASHBOARD FOR DANCER (TIPS LIST) YIELD -------------*******------------- \\
function* getTipsListYield(action: any) {
  try {
    const res = yield call(DashboardApiRequest.getTipsListRequest, action.payload);
    console.log('SUCCESS +++++++++++ getTipsListYield', res);
    if (res && res.status) {
      yield put({
        type: DashboardActionTypes.GET_TIPS_LIST_SUCCESS,
        payload: res.data,
      });
    } else {
      yield put({
        type: DashboardActionTypes.GET_TIPS_LIST_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getTipsListYield ', error.response);
    yield put({
      type: DashboardActionTypes.GET_TIPS_LIST_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getTipsListSaga() {
  yield takeLatest(
    DashboardActionTypes.GET_TIPS_LIST_REQUEST,
    getTipsListYield,
  );
}

// -------------*******------------- GET USER TYPE BY TOKEN (Logged in User) YIELD -------------*******------------- \\
function* getUserTypeYield() {
  try {
    const res = yield call(DashboardApiRequest.getUserTypeRequest);
    console.log('SUCCESS +++++++++++ getUserTypeYield', res);
    if (res && res.status) {
      yield put({
        type: DashboardActionTypes.GET_USER_TYPE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: DashboardActionTypes.GET_USER_TYPE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getUserTypeYield ', error.response);
    yield put({
      type: DashboardActionTypes.GET_USER_TYPE_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getUserTypeSaga() {
  yield takeLatest(
    DashboardActionTypes.GET_USER_TYPE_REQUEST,
    getUserTypeYield,
  );
}

// -------------*******------------- UPDATE_LOCATION YIELD -------------*******------------- \\
function* updateLocationYield(action: any) {
  try {
    const res = yield call(
      DashboardApiRequest.updateLocationRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ updateLocationYield', res);
    if (res && res.status) {
      yield put({
        type: DashboardActionTypes.UPDATE_LOCATION_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: DashboardActionTypes.UPDATE_LOCATION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** updateLocationYield ', error.response);
    yield put({
      type: DashboardActionTypes.UPDATE_LOCATION_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* updateLocationSaga() {
  yield takeLatest(
    DashboardActionTypes.UPDATE_LOCATION_REQUEST,
    updateLocationYield,
  );
}

// -------------*******------------- ONLINE_OFFLINE YIELD -------------*******------------- \\
function* onlineOfflineYield(action: any) {
  try {
    const res = yield call(
      DashboardApiRequest.onlineOfflineRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ onlineOfflineYield', res);
    if (res && res.status) {
      yield put({
        type: DashboardActionTypes.ONLINE_OFFLINE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: DashboardActionTypes.ONLINE_OFFLINE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** onlineOfflineYield ', error.response);
    yield put({
      type: DashboardActionTypes.ONLINE_OFFLINE_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* onlineOfflineSaga() {
  yield takeLatest(
    DashboardActionTypes.ONLINE_OFFLINE_REQUEST,
    onlineOfflineYield,
  );
}
