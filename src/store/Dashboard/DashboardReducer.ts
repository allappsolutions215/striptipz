import {DashboardActionTypes} from './DashboardAction';

interface StateI {
  dancersListResponse: any;
  dancersListError: any;

  tipsListResponse: any;
  tipsListError: any;

  userTypeResponse: any;
  userTypeError: any;

  updateLocationResponse: any;
  updateLocationError: any;

  onlineOfflineResponse: any;
  onlineOfflineError: any;
}

const initialState: StateI = {
  dancersListResponse: undefined,
  dancersListError: undefined,

  tipsListResponse: undefined,
  tipsListError: undefined,

  userTypeResponse: undefined,
  userTypeError: undefined,

  updateLocationResponse: undefined,
  updateLocationError: undefined,

  onlineOfflineResponse: undefined,
  onlineOfflineError: undefined,
};

const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    case DashboardActionTypes.GET_DANCERS_LIST_SUCCESS:
      return {
        ...state,
        dancersListResponse: action.payload,
        dancersListError: undefined,
      };

    case DashboardActionTypes.GET_DANCERS_LIST_FAILURE:
      return {
        ...state,
        dancersListResponse: undefined,
        dancersListError: action.payload,
      };


    case DashboardActionTypes.GET_TIPS_LIST_SUCCESS:
      return {
        ...state,
        tipsListResponse: action.payload,
        tipsListError: undefined,
      };

    case DashboardActionTypes.GET_TIPS_LIST_FAILURE:
      return {
        ...state,
        tipsListResponse: undefined,
        tipsListError: action.payload,
      };

    case DashboardActionTypes.GET_USER_TYPE_SUCCESS:
      return {
        ...state,
        userTypeResponse: action.payload,
        userTypeError: undefined,
      };

    case DashboardActionTypes.GET_USER_TYPE_FAILURE:
      return {
        ...state,
        userTypeResponse: undefined,
        userTypeError: action.payload,
      };

    case DashboardActionTypes.UPDATE_LOCATION_SUCCESS:
      return {
        ...state,
        updateLocationResponse: action.payload,
        updateLocationError: undefined,
      };

    case DashboardActionTypes.UPDATE_LOCATION_FAILURE:
      return {
        ...state,
        updateLocationResponse: undefined,
        updateLocationError: action.payload,
      };

    case DashboardActionTypes.ONLINE_OFFLINE_SUCCESS:
      return {
        ...state,
        onlineOfflineResponse: action.payload,
        onlineOfflineError: undefined,
      };

    case DashboardActionTypes.ONLINE_OFFLINE_FAILURE:
      return {
        ...state,
        onlineOfflineResponse: undefined,
        onlineOfflineError: action.payload,
      };

    default:
      return state;
  }
};
export default reducers;
