import axios from '../../package/axios';

const getDancersListRequest = (payload: any) => {
  return axios.get(`dashboard-for-patrone/?range=${payload.range}`);
};

const getTipsListRequest = (payload: any) => {
  return axios.get(`dashboard-for-dancer?date=${payload}`);
};

const getUserTypeRequest = () => {
  return axios.get('get-user_type');
};

const updateLocationRequest = (payload: any) => {
  //TODO CHANGE: TYPO BY BACKEND log****
  return axios.post('update-lat-log', payload);
};

const onlineOfflineRequest = (payload: any) => {
  return axios.post(
    `online-offline-status?is_online=${payload.is_online === true ? 1 : 0}`,
    payload,
  );
};

export const DashboardApiRequest = {
  getDancersListRequest,
  getTipsListRequest,
  getUserTypeRequest,
  updateLocationRequest,
  onlineOfflineRequest,
};
