export const DashboardActionTypes = {
  GET_DANCERS_LIST_REQUEST: 'GET_DANCERS_LIST_REQUEST',
  GET_DANCERS_LIST_SUCCESS: 'GET_DANCERS_LIST_SUCCESS',
  GET_DANCERS_LIST_FAILURE: 'GET_DANCERS_LIST_FAILURE',

  GET_TIPS_LIST_REQUEST: 'GET_TIPS_LIST_REQUEST',
  GET_TIPS_LIST_SUCCESS: 'GET_TIPS_LIST_SUCCESS',
  GET_TIPS_LIST_FAILURE: 'GET_TIPS_LIST_FAILURE',

  GET_USER_TYPE_REQUEST: 'GET_USER_TYPE_REQUEST',
  GET_USER_TYPE_SUCCESS: 'GET_USER_TYPE_SUCCESS',
  GET_USER_TYPE_FAILURE: 'GET_USER_TYPE_FAILURE',

  UPDATE_LOCATION_REQUEST: 'UPDATE_LOCATION_REQUEST',
  UPDATE_LOCATION_SUCCESS: 'UPDATE_LOCATION_SUCCESS',
  UPDATE_LOCATION_FAILURE: 'UPDATE_LOCATION_FAILURE',

  ONLINE_OFFLINE_REQUEST: 'ONLINE_OFFLINE_REQUEST',
  ONLINE_OFFLINE_SUCCESS: 'ONLINE_OFFLINE_SUCCESS',
  ONLINE_OFFLINE_FAILURE: 'ONLINE_OFFLINE_FAILURE',
};

const getDancersListAction = (payload: any) => {
  return {type: DashboardActionTypes.GET_DANCERS_LIST_REQUEST, payload};
};

const geTipsListAction = (payload: any) => {
  return {type: DashboardActionTypes.GET_TIPS_LIST_REQUEST, payload};
};

const getUserTypeAction = () => {
  return {type: DashboardActionTypes.GET_USER_TYPE_REQUEST};
};

const updateLocationAction = (payload: any) => {
  console.log(363636,'payload',payload);
  
  return {type: DashboardActionTypes.UPDATE_LOCATION_REQUEST, payload};
};

const onlineOfflineAction = (payload: any) => {
  return {type: DashboardActionTypes.ONLINE_OFFLINE_REQUEST, payload};
};

export default {
  getDancersListAction,
  geTipsListAction,
  getUserTypeAction,
  updateLocationAction,
  onlineOfflineAction,
};
