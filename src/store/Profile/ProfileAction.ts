export const ProfileActionTypes = {
  ADD_PHOTO_REQUEST: 'ADD_PHOTO_REQUEST',
  ADD_PHOTO_SUCCESS: 'ADD_PHOTO_SUCCESS',
  ADD_PHOTO_FAILURE: 'ADD_PHOTO_FAILURE',

  UPDATE_DANCER_PROFILE_REQUEST: 'UPDATE_DANCER_PROFILE_REQUEST',
  UPDATE_DANCER_PROFILE_SUCCESS: 'UPDATE_DANCER_PROFILE_SUCCESS',
  UPDATE_DANCER_PROFILE_FAILURE: 'UPDATE_DANCER_PROFILE_FAILURE',

  UPDATE_PATRON_PROFILE_REQUEST: 'UPDATE_PATRON_PROFILE_REQUEST',
  UPDATE_PATRON_PROFILE_SUCCESS: 'UPDATE_PATRON_PROFILE_SUCCESS',
  UPDATE_PATRON_PROFILE_FAILURE: 'UPDATE_PATRON_PROFILE_FAILURE',

  SHOW_HEADERS_REQUEST: 'SHOW_HEADERS_REQUEST',
  SHOW_HEADERS_SUCCESS: 'SHOW_HEADERS_SUCCESS',
  SHOW_HEADERS_FAILURE: 'SHOW_HEADERS_FAILURE',

  VIEW_DANCER_DETAILS_REQUEST: 'VIEW_DANCER_DETAILS_REQUEST',
  VIEW_DANCER_DETAILS_SUCCESS: 'VIEW_DANCER_DETAILS_SUCCESS',
  VIEW_DANCER_DETAILS_FAILURE: 'VIEW_DANCER_DETAILS_FAILURE',

  GET_PHOTOS_REQUEST: 'GET_PHOTOS_REQUEST',
  GET_PHOTOS_SUCCESS: 'GET_PHOTOS_SUCCESS',
  GET_PHOTOS_FAILURE: 'GET_PHOTOS_FAILURE',

  GET_MY_PROFILE_REQUEST: 'GET_MY_PROFILE_REQUEST',
  GET_MY_PROFILE_SUCCESS: 'GET_MY_PROFILE_SUCCESS',
  GET_MY_PROFILE_FAILURE: 'GET_MY_PROFILE_FAILURE',

  CHANGE_USER_TYPE_REQUEST: 'CHANGE_USER_TYPE_REQUEST',
  CHANGE_USER_TYPE_SUCCESS: 'CHANGE_USER_TYPE_SUCCESS',
  CHANGE_USER_TYPE_FAILURE: 'CHANGE_USER_TYPE_ILURE',
};

const addPhotoAction = (payload: any) => {
  var data = new FormData();
  var filename = payload.uri.replace(/^.*[\\\/]/, '');
  data.append('image[]', {
    uri: payload.uri,
    name: filename,
    type: 'image/' + filename.split('.').pop(),
  });

  let newPayLoad = {
    data: data,
    index: payload.index,
  };
  return { type: ProfileActionTypes.ADD_PHOTO_REQUEST, newPayLoad };
};

const updateDancerProfileAction = (payload: any) => {
  return { type: ProfileActionTypes.UPDATE_DANCER_PROFILE_REQUEST, payload };
};

const updatePatronProfileAction = (payload: any) => {
  return { type: ProfileActionTypes.UPDATE_PATRON_PROFILE_REQUEST, payload };
};

const showHeadersAction = () => {
  return { type: ProfileActionTypes.SHOW_HEADERS_REQUEST };
};

const viewDancerDetailsAction = (payload: any) => {
  return { type: ProfileActionTypes.VIEW_DANCER_DETAILS_REQUEST, payload };
};

const getPhotosAction = () => {
  return { type: ProfileActionTypes.GET_PHOTOS_REQUEST };
};

const getMyProfileAction = () => {
  return { type: ProfileActionTypes.GET_MY_PROFILE_REQUEST };
};

const changeUserType = (payload: any) => {

  return { type: ProfileActionTypes.CHANGE_USER_TYPE_REQUEST, payload };
};



export default {
  addPhotoAction,
  updatePatronProfileAction,
  updateDancerProfileAction,
  showHeadersAction,
  viewDancerDetailsAction,
  getPhotosAction,
  getMyProfileAction,
  changeUserType
};
