import { ProfileActionTypes } from './ProfileAction';

interface StateI {
  addPhotoResponse: any;
  addPhotoError: any;

  updateDancerProfileResponse: any;
  updateDancerProfileError: any;

  updatePatronProfileResponse: any;
  updatePatronProfileError: any;

  showHeadersResponse: any;
  showHeadersError: any;

  getPhotosResponse: any;
  getPhotosError: any;

  dancerDetailsResponse: any;
  dancerDetailsError: any;

  getMyProfileResponse: any;
  getMyProfileError: any;

  changeUserTypeResponse: any;
  changeUserTypeError: any;
}

const initialState: StateI = {
  addPhotoResponse: undefined,
  addPhotoError: undefined,

  updateDancerProfileResponse: undefined,
  updateDancerProfileError: undefined,

  updatePatronProfileResponse: undefined,
  updatePatronProfileError: undefined,

  showHeadersResponse: undefined,
  showHeadersError: undefined,

  getPhotosResponse: undefined,
  getPhotosError: undefined,

  getMyProfileResponse: undefined,
  getMyProfileError: undefined,

  changeUserTypeResponse: undefined,
  changeUserTypeError: undefined,

  dancerDetailsResponse: undefined,
  dancerDetailsError: undefined,
};

const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    case ProfileActionTypes.ADD_PHOTO_SUCCESS:
      return {
        ...state,
        addPhotoResponse: action.payload,
        addPhotoError: undefined,
      };

    case ProfileActionTypes.ADD_PHOTO_FAILURE:
      return {
        ...state,
        addPhotoResponse: undefined,
        addPhotoError: action.payload,
      };

    case ProfileActionTypes.UPDATE_DANCER_PROFILE_SUCCESS:
      return {
        ...state,
        updateDancerProfileResponse: action.payload,
        updateDancerProfileError: undefined,
      };

    case ProfileActionTypes.UPDATE_DANCER_PROFILE_FAILURE:
      return {
        ...state,
        updateDancerProfileResponse: undefined,
        updateDancerProfileError: action.payload,
      };

    case ProfileActionTypes.UPDATE_PATRON_PROFILE_SUCCESS:
      return {
        ...state,
        updatePatronProfileResponse: action.payload,
        updatePatronProfileError: undefined,
      };

    case ProfileActionTypes.UPDATE_PATRON_PROFILE_FAILURE:
      return {
        ...state,
        updatePatronProfileResponse: undefined,
        updatePatronProfileError: action.payload,
      };

    case ProfileActionTypes.SHOW_HEADERS_SUCCESS:
      return {
        ...state,
        showHeadersResponse: action.payload,
        showHeadersError: undefined,
      };

    case ProfileActionTypes.SHOW_HEADERS_FAILURE:
      return {
        ...state,
        showHeadersResponse: undefined,
        showHeadersError: action.payload,
      };

    case ProfileActionTypes.GET_PHOTOS_SUCCESS:
      return {
        ...state,
        getPhotosResponse: action.payload,
        getPhotosError: undefined,
      };

    case ProfileActionTypes.GET_PHOTOS_FAILURE:
      return {
        ...state,
        getPhotosResponse: undefined,
        getPhotosError: action.payload,
      };
    case ProfileActionTypes.GET_MY_PROFILE_SUCCESS:
      return {
        ...state,
        getMyProfileResponse: action.payload,
        getMyProfileError: undefined,
      };

    case ProfileActionTypes.GET_MY_PROFILE_FAILURE:
      return {
        ...state,
        getMyProfileResponse: undefined,
        getMyProfileError: action.payload,
      };

    case ProfileActionTypes.VIEW_DANCER_DETAILS_SUCCESS:
      return {
        ...state,
        dancerDetailsResponse: action.payload,
        dancerDetailsError: undefined,
      };

    case ProfileActionTypes.VIEW_DANCER_DETAILS_FAILURE:
      return {
        ...state,
        dancerDetailsResponse: undefined,
        dancerDetailsError: action.payload,
      };

    case ProfileActionTypes.CHANGE_USER_TYPE_SUCCESS:
      return {
        ...state,
        changeUserTypeResponse: action.payload.data,
        changeUserTypeError: undefined,
      };

    case ProfileActionTypes.CHANGE_USER_TYPE_FAILURE:
      return {
        ...state,
        changeUserTypeResponse: undefined,
        changeUserTypeError: action.payload,
      };

    default:
      return state;
  }
};
export default reducers;
