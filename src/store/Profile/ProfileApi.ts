import axios from '../../package/axios';

const addPhotoRequest = (postData: any) => {
  console.log('add image for dancer', JSON.stringify(postData));
  return axios.post('add-image-for-dancer', postData);
};

const updatePatronProfileRequest = (postData: any) => {
  console.log('post data', postData)
  return axios.post('update-profile-for-patrone', postData);
};

const updateDancerProfileRequest = (postData: any) => {
  return axios.post('update-profile-for-dancer', postData);
};

const showHeadersRequest = () => {
  return axios.get('show_headers?h=2');
};

const getDancerDetailsRequest = (data: any) => {
  return axios.get(`view-deatail-dancer/${data}`);
};

const getPhotosRequest = () => {
  return axios.get('get-image-for-dancer');
};
const getMyProfileRequest = () => {
  return axios.get('my-profile');
};

const changeUserType = (data: any) => {
  return axios.post(`change-user-type?user_type=${data}`);
};

export const ProfileApiRequest = {
  addPhotoRequest,
  updatePatronProfileRequest,
  updateDancerProfileRequest,
  showHeadersRequest,
  getDancerDetailsRequest,
  getPhotosRequest,
  getMyProfileRequest,
  changeUserType
};
