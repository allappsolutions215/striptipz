import { call, put, takeLatest } from 'redux-saga/effects';

import { ProfileActionTypes } from './ProfileAction';
import { ProfileApiRequest } from './ProfileApi';

// -------------*******------------- ADD PHOTO YIELD -------------*******------------- \\
function* addPhotoYield(action: any) {
  try {
    let res = yield call(
      ProfileApiRequest.addPhotoRequest,
      action.newPayLoad.data,
    );
    console.log('SUCCESS +++++++++++ addPhotoYield', res);
    if (res && res.status) {
      res.index = action.newPayLoad.index;
      yield put({
        type: ProfileActionTypes.ADD_PHOTO_SUCCESS,
        payload: res,
      });
    } else {

      console.log('error-=-=', res)
      res.index = action.newPayLoad.index;
      yield put({
        type: ProfileActionTypes.ADD_PHOTO_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    console.log('error -=-=-=>', error);

    yield put({
      type: ProfileActionTypes.ADD_PHOTO_FAILURE,
      payload: error,
    });
  }
}
export function* addPhotoSaga() {
  yield takeLatest(ProfileActionTypes.ADD_PHOTO_REQUEST, addPhotoYield);
}

// -------------*******------------- UPDATE PATRON PROFILE YIELD -------------*******------------- \\
function* updatePatronProfileYield(action: any) {
  try {
    const res = yield call(
      ProfileApiRequest.updatePatronProfileRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ updatePatronProfileYield', res);
    if (res && res.status) {
      yield put({
        type: ProfileActionTypes.UPDATE_PATRON_PROFILE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.UPDATE_PATRON_PROFILE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: ProfileActionTypes.UPDATE_PATRON_PROFILE_FAILURE,
      payload: error,
    });
  }
}
export function* updatePatronProfileSaga() {
  yield takeLatest(
    ProfileActionTypes.UPDATE_PATRON_PROFILE_REQUEST,
    updatePatronProfileYield,
  );
}

// -------------*******------------- UPDATE DANCER PROFILE YIELD -------------*******------------- \\
function* updateDancerProfileYield(action: any) {
  try {
    const res = yield call(
      ProfileApiRequest.updateDancerProfileRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ updateDancerProfileYield', res);
    if (res && res.status) {
      yield put({
        type: ProfileActionTypes.UPDATE_DANCER_PROFILE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.UPDATE_DANCER_PROFILE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** updateDancerProfileYield ', error);
    yield put({
      type: ProfileActionTypes.UPDATE_DANCER_PROFILE_FAILURE,
      payload: error,
    });
  }
}
export function* updateDancerProfileSaga() {
  yield takeLatest(
    ProfileActionTypes.UPDATE_DANCER_PROFILE_REQUEST,
    updateDancerProfileYield,
  );
}

// -------------*******------------- SHOW HEADERS YIELD -------------*******------------- \\
function* showHeadersYield() {
  try {
    const res = yield call(ProfileApiRequest.showHeadersRequest);
    console.log('SUCCESS +++++++++++ showHeadersYield', res);
    if (res && res.status) {
      yield put({
        type: ProfileActionTypes.SHOW_HEADERS_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.SHOW_HEADERS_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: ProfileActionTypes.SHOW_HEADERS_FAILURE,
      payload: error,
    });
  }
}
export function* showHeadersSaga() {
  yield takeLatest(ProfileActionTypes.SHOW_HEADERS_REQUEST, showHeadersYield);
}

// -------------*******------------- VIEW_DANCER_DETAILS YIELD -------------*******------------- \\
function* getDancerDetailsYield(action: any) {
  try {
    const res = yield call(
      ProfileApiRequest.getDancerDetailsRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ getDancerDetailsYield', res);
    if (res && res.status) {
      yield put({
        type: ProfileActionTypes.VIEW_DANCER_DETAILS_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.VIEW_DANCER_DETAILS_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getDancerDetailsYield ', error.response);
    yield put({
      type: ProfileActionTypes.VIEW_DANCER_DETAILS_FAILURE,
      payload: error,
    });
  }
}
export function* getDancerDetailsSaga() {
  yield takeLatest(
    ProfileActionTypes.VIEW_DANCER_DETAILS_REQUEST,
    getDancerDetailsYield,
  );
}

// -------------*******------------- UPDATE DANCER PROFILE YIELD -------------*******------------- \\
function* getPhotosYield() {
  try {
    const res = yield call(ProfileApiRequest.getPhotosRequest);
    console.log('SUCCESS +++++++++++ getPhotosYield', res);
    if (res && res.status) {
      yield put({
        type: ProfileActionTypes.GET_PHOTOS_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.GET_PHOTOS_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: ProfileActionTypes.GET_PHOTOS_FAILURE,
      payload: error,
    });
  }
}
export function* getPhotosSaga() {
  yield takeLatest(ProfileActionTypes.GET_PHOTOS_REQUEST, getPhotosYield);
}

// -------------*******------------- GET_MY_PROFILE YIELD -------------*******------------- \\
function* getMyProfileYield() {
  try {
    const res = yield call(ProfileApiRequest.getMyProfileRequest);
    console.log('SUCCESS +++++++++++ getMyProfileYield', res);
    if (res) {
      yield put({
        type: ProfileActionTypes.GET_MY_PROFILE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.GET_MY_PROFILE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: ProfileActionTypes.GET_MY_PROFILE_FAILURE,
      payload: error,
    });
  }
}
export function* getMyProfileSaga() {
  yield takeLatest(
    ProfileActionTypes.GET_MY_PROFILE_REQUEST,
    getMyProfileYield,
  );
}

// -------------*******------------- CHANGE USER TYPE -------------*******------------- \\

function* changeUserTypeYield(action: any) {

  try {
    const res = yield call(ProfileApiRequest.changeUserType, action.payload);
    console.log('SUCCESS +++++++++++ changeUserTypeYield', res);
    if (res) {
      yield put({
        type: ProfileActionTypes.CHANGE_USER_TYPE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: ProfileActionTypes.CHANGE_USER_TYPE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: ProfileActionTypes.CHANGE_USER_TYPE_FAILURE,
      payload: error,
    });
  }
}
export function* changeUserType() {
  yield takeLatest(
    ProfileActionTypes.CHANGE_USER_TYPE_REQUEST,
    changeUserTypeYield,
  );
}