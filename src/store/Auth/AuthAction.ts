export const AuthActionTypes = {
  SIGN_IN_REQUEST: 'SIGN_IN_REQUEST',
  SIGN_IN_SUCCESS: 'SIGN_IN_SUCCESS',
  SIGN_IN_FAILURE: 'SIGN_IN_FAILURE',

  SIGN_UP_REQUEST: 'SIGN_UP_REQUEST',
  SIGN_UP_SUCCESS: 'SIGN_UP_SUCCESS',
  SIGN_UP_FAILURE: 'SIGN_UP_FAILURE',

  VERIFY_OTP_REQUEST: 'VERIFY_OTP_REQUEST',
  VERIFY_OTP_SUCCESS: 'VERIFY_OTP_SUCCESS',
  VERIFY_OTP_FAILURE: 'VERIFY_OTP_FAILURE',

  FORGOT_STEP_1_REQUEST: 'FORGOT_STEP_1_REQUEST',
  FORGOT_STEP_1_SUCCESS: 'FORGOT_STEP_1_SUCCESS',
  FORGOT_STEP_1_FAILURE: 'FORGOT_STEP_1_FAILURE',

  REGISTER_PROFILE_REQUEST: 'REGISTER_PROFILE_REQUEST',
  REGISTER_PROFILE_SUCCESS: 'REGISTER_PROFILE_SUCCESS',
  REGISTER_PROFILE_FAILURE: 'REGISTER_PROFILE_FAILURE',

  FORGOT_STEP_3_REQUEST: 'FORGOT_STEP_3_REQUEST',
  FORGOT_STEP_3_SUCCESS: 'FORGOT_STEP_3_SUCCESS',
  FORGOT_STEP_3_FAILURE: 'FORGOT_STEP_3_FAILURE',

  CHANGE_PASSWORD_REQUEST: 'CHANGE_PASSWORD_REQUEST',
  CHANGE_PASSWORD_SUCCESS: 'CHANGE_PASSWORD_SUCCESS',
  CHANGE_PASSWORD_FAILURE: 'CHANGE_PASSWORD_FAILURE',
};

const signInAction = (payload: any) => {
  return {type: AuthActionTypes.SIGN_IN_REQUEST, payload};
};

const signUpAction = (payload: any) => {
  return {type: AuthActionTypes.SIGN_UP_REQUEST, payload};
};

const verifyOtpAction = (payload: any) => {
  return {type: AuthActionTypes.VERIFY_OTP_REQUEST, payload};
};

const forgotPassStep1Action = (payload: any) => {
  return {type: AuthActionTypes.FORGOT_STEP_1_REQUEST, payload};
};

const registerProfileAction = (payload: any) => {
  return {type: AuthActionTypes.REGISTER_PROFILE_REQUEST, payload};
};

const forgotPassStep3Action = (payload: any) => {
  return {type: AuthActionTypes.FORGOT_STEP_3_REQUEST, payload};
};

const changePasswordAction = (payload: any) => {
  return {type: AuthActionTypes.CHANGE_PASSWORD_REQUEST, payload};
};

export default {
  signInAction,
  signUpAction,
  verifyOtpAction,
  forgotPassStep1Action,
  registerProfileAction,
  forgotPassStep3Action,
  changePasswordAction,
};
