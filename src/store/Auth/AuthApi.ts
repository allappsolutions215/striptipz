import axios from '../../package/axios';

const signInRequest = (postData: any) => {
  return axios.post('login', postData);
};

const signUpRequest = (postData: any) => {
  return axios.post('register-mobile', postData);
};

const verifyOtpRequest = (postData: any) => {
  return axios.post('register-otp', postData);
};

const forgotPassStep1Request = (postData: any) => {
  return axios.post('forget-password-step1', postData);
};

const registerProfileRequest = (postData: any) => {
  return axios.post('register-profile', postData);
};

const forgotPassStep3Request = (postData: any) => {
  console.log('post data for chagne password', postData)
  return axios.post('forget-password-step3', postData);
};

const changePasswordRequest = (postData: any) => {
  return axios.post('change-password', postData);
};

export const AuthApiRequest = {
  signInRequest,
  signUpRequest,
  verifyOtpRequest,
  forgotPassStep1Request,
  registerProfileRequest,
  forgotPassStep3Request,
  changePasswordRequest,
};
