import { call, put, takeLatest } from 'redux-saga/effects';

import { AuthActionTypes } from './AuthAction';
import { AuthApiRequest } from './AuthApi';
import axios from '../../package/axios';

// -------------*******------------- SIGN IN YIELD -------------*******------------- \\
function* signInYield(action: any) {
  try {
    let res = yield call(AuthApiRequest.signInRequest, action.payload);

    if (res && res.status) {

      axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${res.data.token}`;
      axios.defaults.headers.common[
        'Content-Type'
      ] = `application/x-www-form-urlencoded`;
      
      yield put({
        type: AuthActionTypes.SIGN_IN_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.SIGN_IN_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** signInYield ', error);
    yield put({
      type: AuthActionTypes.SIGN_IN_FAILURE,
      payload: error,
    });
  }
}
export function* signInSaga() {
  yield takeLatest(AuthActionTypes.SIGN_IN_REQUEST, signInYield);
}

// -------------*******------------- SIGN IN YIELD -------------*******------------- \\
function* signUpYield(action: any) {
  try {
    const res = yield call(AuthApiRequest.signUpRequest, action.payload);
    console.log('SUCCESS +++++++++++ signUpYield', res);

    if (res && res.status) {
      yield put({
        type: AuthActionTypes.SIGN_UP_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.SIGN_UP_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** signUpYield ', error);
    yield put({
      type: AuthActionTypes.SIGN_UP_FAILURE,
      payload: error,
    });
  }
}
export function* signUpSaga() {
  yield takeLatest(AuthActionTypes.SIGN_UP_REQUEST, signUpYield);
}

// -------------*******------------- VERIFY OTP YIELD -------------*******------------- \\
function* verifyOtpYield(action: any) {
  try {
    const res = yield call(AuthApiRequest.verifyOtpRequest, action.payload);
    console.log('SUCCESS +++++++++++ verifyOtpYield', res);
    if (res && res.status) {
      yield put({
        type: AuthActionTypes.VERIFY_OTP_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.VERIFY_OTP_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** verifyOtpYield ', error);
    yield put({
      type: AuthActionTypes.VERIFY_OTP_FAILURE,
      payload: error,
    });
  }
}
export function* verifyOtpSaga() {
  yield takeLatest(AuthActionTypes.VERIFY_OTP_REQUEST, verifyOtpYield);
}

// -------------*******------------- FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) YIELD -------------*******------------- \\
function* forgotPassStep1Yield(action: any) {
  try {
    const res = yield call(
      AuthApiRequest.forgotPassStep1Request,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ forgotPassStep1Yield', res);
    if (res && res.status) {
      yield put({
        type: AuthActionTypes.FORGOT_STEP_1_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.FORGOT_STEP_1_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** forgotPassStep1Yield ', error);
    yield put({
      type: AuthActionTypes.FORGOT_STEP_1_FAILURE,
      payload: error,
    });
  }
}
export function* forgotPassStep1Saga() {
  yield takeLatest(AuthActionTypes.FORGOT_STEP_1_REQUEST, forgotPassStep1Yield);
}

// -------------*******------------- REGISTER PROFILE YIELD (register-profile COMMAN FOR BOTH (Dancer and Patron)) -------------*******------------- \\
function* registerProfileYield(action: any) {
  try {
    const res = yield call(
      AuthApiRequest.registerProfileRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ registerProfileYield', res);
    if (res && res.status) {
      axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${res.data.token}`;
      yield put({
        type: AuthActionTypes.REGISTER_PROFILE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.REGISTER_PROFILE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** registerProfileYield ', error);
    yield put({
      type: AuthActionTypes.REGISTER_PROFILE_FAILURE,
      payload: error,
    });
  }
}
export function* registerProfileSaga() {
  yield takeLatest(
    AuthActionTypes.REGISTER_PROFILE_REQUEST,
    registerProfileYield,
  );
}

// -------------*******------------- FORGOT PASSWORD STEP 3 YIELD -------------*******------------- \\
function* forgotPassStep3Yield(action: any) {
  try {
    const res = yield call(
      AuthApiRequest.forgotPassStep3Request,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ forgotPassStep3Yield', res);
    if (res && res.status) {
      yield put({
        type: AuthActionTypes.FORGOT_STEP_3_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.FORGOT_STEP_3_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    yield put({
      type: AuthActionTypes.FORGOT_STEP_3_FAILURE,
      payload: error,
    });
  }
}
export function* forgotPassStep3Saga() {
  yield takeLatest(AuthActionTypes.FORGOT_STEP_3_REQUEST, forgotPassStep3Yield);
}

// -------------*******------------- CHANGE PASSWORD YIELD -------------*******------------- \\
function* changePasswordYield(action: any) {
  try {
    const res = yield call(
      AuthApiRequest.changePasswordRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ changePasswordYield', res);
    if (res && res.status) {
      yield put({
        type: AuthActionTypes.CHANGE_PASSWORD_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: AuthActionTypes.CHANGE_PASSWORD_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** changePasswordYield ', error);
    yield put({
      type: AuthActionTypes.CHANGE_PASSWORD_FAILURE,
      payload: error,
    });
  }
}
export function* changePasswordSaga() {
  yield takeLatest(
    AuthActionTypes.CHANGE_PASSWORD_REQUEST,
    changePasswordYield,
  );
}
