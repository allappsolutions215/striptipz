import {AuthActionTypes} from './AuthAction';

interface StateI {
  signInResponse: any;
  signInError: any;

  signUpResponse: any;
  signUpError: any;

  verifyOtpResponse: any;
  verifyOtpError: any;

  forgotPassStep1Response: any;
  forgotPassStep1Error: any;

  registerProfileResponse: any;
  registerProfileError: any;

  forgotPassStep3Response: any;
  forgotPassStep3Error: any;

  changePasswordResponse: any;
  changePasswordError: any;
}

const initialState: StateI = {
  signInResponse: undefined,
  signInError: undefined,

  signUpResponse: undefined,
  signUpError: undefined,

  verifyOtpResponse: undefined,
  verifyOtpError: undefined,

  forgotPassStep1Response: undefined,
  forgotPassStep1Error: undefined,

  registerProfileResponse: undefined,
  registerProfileError: undefined,

  forgotPassStep3Response: undefined,
  forgotPassStep3Error: undefined,

  changePasswordResponse: undefined,
  changePasswordError: undefined,
};

const reducers = (state = initialState, action: any) => {

  switch (action.type) {
    case AuthActionTypes.SIGN_IN_SUCCESS:
      return {
        ...state,
        signInResponse: action.payload,
        signInError: undefined,
      };

    case AuthActionTypes.SIGN_IN_FAILURE:
      return {
        ...state,
        signInResponse: undefined,
        signInError: action.payload,
      };

    case AuthActionTypes.SIGN_UP_SUCCESS:
      return {
        ...state,
        signUpResponse: action.payload,
        signUpError: undefined,
      };

    case AuthActionTypes.SIGN_UP_FAILURE:
      return {
        ...state,
        signUpResponse: undefined,
        signUpError: action.payload,
      };

    case AuthActionTypes.VERIFY_OTP_SUCCESS:
      return {
        ...state,
        verifyOtpResponse: action.payload,
        verifyOtpError: undefined,
      };

    case AuthActionTypes.VERIFY_OTP_FAILURE:
      return {
        ...state,
        verifyOtpResponse: undefined,
        verifyOtpError: action.payload,
      };

    case AuthActionTypes.FORGOT_STEP_1_SUCCESS:
      return {
        ...state,
        forgotPassStep1Response: action.payload,
        forgotPassStep1Error: undefined,
      };

    case AuthActionTypes.FORGOT_STEP_1_FAILURE:
      return {
        ...state,
        forgotPassStep1Response: undefined,
        forgotPassStep1Error: action.payload,
      };

    case AuthActionTypes.REGISTER_PROFILE_SUCCESS:
      return {
        ...state,
        registerProfileResponse: action.payload,
        registerProfileError: undefined,
      };

    case AuthActionTypes.REGISTER_PROFILE_FAILURE:
      return {
        ...state,
        registerProfileResponse: undefined,
        registerProfileError: action.payload,
      };

    case AuthActionTypes.FORGOT_STEP_3_SUCCESS:
      return {
        ...state,
        forgotPassStep3Response: action.payload,
        forgotPassStep3Error: undefined,
      };

    case AuthActionTypes.FORGOT_STEP_3_FAILURE:
      return {
        ...state,
        forgotPassStep3Response: undefined,
        forgotPassStep3Error: action.payload,
      };

    case AuthActionTypes.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        changePasswordResponse: action.payload,
        changePasswordError: undefined,
      };

    case AuthActionTypes.CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        changePasswordResponse: undefined,
        changePasswordError: action.payload,
      };

    default:
      return state;
  }
};
export default reducers;
