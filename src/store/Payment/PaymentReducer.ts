import { PaymentActionTypes } from './PaymentAction';

interface StateI {
  addAmountResponse: any;
  addAmountError: any;

  addTransactionResponse: any;
  addTransactionError: any;

  walletBalanceResponse: any;
  walletBalanceError: any;

  tipByPatronResponse: any;
  tipByPatronError: any;

  patronTransHistoryResponse: any;
  patronTransHistoryError: any;

  dancerMoneyTransfResponse: any;
  dancerMoneyTransfError: any;

  getPPAccessTokenResponse: any;
  getPPAccessTokenError: any;

  getPaymentDataResponse: any;
  getPaymentDataError: any;

  connetDisconnentPayPalResponse: any;
  connetDisconnentPayPalError: any;

  createProductResponse: any;
  createProductError: any;

  createSubscriptionPlanResponse: any;
  createSubscriptionPlanError: any;

  createSubscriptionResponse: any;
  createSubscriptionError: any;

  createSubscriptionServerResponse: any;
  createSubscriptionServerError: any;

  cancelSubscriptionResponse: any;
  cancelSubscriptionError: any;
}

const initialState: StateI = {
  addAmountResponse: undefined,
  addAmountError: undefined,

  addTransactionResponse: undefined,
  addTransactionError: undefined,


  walletBalanceResponse: undefined,
  walletBalanceError: undefined,

  tipByPatronResponse: undefined,
  tipByPatronError: undefined,

  patronTransHistoryResponse: undefined,
  patronTransHistoryError: undefined,

  dancerMoneyTransfResponse: undefined,
  dancerMoneyTransfError: undefined,

  getPPAccessTokenResponse: undefined,
  getPPAccessTokenError: undefined,

  getPaymentDataResponse: undefined,
  getPaymentDataError: undefined,

  connetDisconnentPayPalResponse: undefined,
  connetDisconnentPayPalError: undefined,

  createProductResponse: undefined,
  createProductError: undefined,

  createSubscriptionPlanResponse: undefined,
  createSubscriptionPlanError: undefined,

  createSubscriptionResponse: undefined,
  createSubscriptionError: undefined,

  createSubscriptionServerResponse: undefined,
  createSubscriptionServerError: undefined,
};

const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    case PaymentActionTypes.ADD_AMOUNT_SUCCESS:
      return {
        ...state,
        addAmountResponse: action.payload,
        addAmountError: undefined,
      };

    case PaymentActionTypes.ADD_AMOUNT_FAILURE:
      return {
        ...state,
        addAmountResponse: undefined,
        addAmountError: action.payload,
      };

    case PaymentActionTypes.ADD_TRANSACTION_SUCCESS:
      return {
        ...state,
        addTransactionResponse: action.payload,
        addTransactionError: undefined,
      };

    case PaymentActionTypes.ADD_TRANSACTION_FAILURE:
      return {
        ...state,
        addTransactionResponse: undefined,
        addTransactionError: action.payload,
      };

    case PaymentActionTypes.AVAILABLE_WALLET_BALANCE_SUCCESS:
      return {
        ...state,
        walletBalanceResponse: action.payload,
        walletBalanceError: undefined,
      };

    case PaymentActionTypes.AVAILABLE_WALLET_BALANCE_FAILURE:
      return {
        ...state,
        walletBalanceResponse: undefined,
        walletBalanceError: action.payload,
      };

    case PaymentActionTypes.TIP_BY_PATRON_SUCCESS:
      return {
        ...state,
        tipByPatronResponse: action.payload,
        tipByPatronError: undefined,
      };

    case PaymentActionTypes.TIP_BY_PATRON_FAILURE:
      return {
        ...state,
        tipByPatronResponse: undefined,
        tipByPatronError: action.payload,
      };

    case PaymentActionTypes.PATRON_TRANS_HISTORY_SUCCESS:
      return {
        ...state,
        patronTransHistoryResponse: action.payload,
        patronTransHistoryError: undefined,
      };

    case PaymentActionTypes.PATRON_TRANS_HISTORY_FAILURE:
      return {
        ...state,
        patronTransHistoryResponse: undefined,
        patronTransHistoryError: action.payload,
      };

    case PaymentActionTypes.DANCER_MONEY_TRANSF_SUCCESS:
      return {
        ...state,
        dancerMoneyTransfResponse: action.payload,
        dancerMoneyTransfError: undefined,
      };

    case PaymentActionTypes.DANCER_MONEY_TRANSF_FAILURE:
      return {
        ...state,
        dancerMoneyTransfResponse: undefined,
        dancerMoneyTransfError: action.payload,
      };

    case PaymentActionTypes.PP_ACCESS_TOKEN_SUCCESS:
      return {
        ...state,
        getPPAccessTokenResponse: action.payload,
        getPPAccessTokenError: undefined,
      };

    case PaymentActionTypes.PP_ACCESS_TOKEN_FAILURE:
      return {
        ...state,
        getPPAccessTokenResponse: undefined,
        getPPAccessTokenError: action.payload,
      };

    case PaymentActionTypes.PP_PAYMENT_DATA_SUCCESS:
      return {
        ...state,
        getPaymentDataResponse: action.payload,
        getPaymentDataError: undefined,
      };

    case PaymentActionTypes.PP_PAYMENT_DATA_FAILURE:
      return {
        ...state,
        getPaymentDataResponse: undefined,
        getPaymentDataError: action.payload,
      };

    case PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_SUCCESS:
      return {
        ...state,
        connetDisconnentPayPalResponse: action.payload,
        connetDisconnentPayPalError: undefined,
      };

    case PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_FAILURE:
      return {
        ...state,
        connetDisconnentPayPalResponse: undefined,
        connetDisconnentPayPalError: action.payload,
      };

    case PaymentActionTypes.CREATE_PRODUCT_SUCCESS:
      return {
        ...state,
        createProductResponse: action.payload,
        createProductError: undefined,
      };

    case PaymentActionTypes.CREATE_PRODUCT_FAILURE:
      return {
        ...state,
        createProductResponse: undefined,
        createProductError: action.payload,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_SUCCESS:
      return {
        ...state,
        createSubscriptionPlanResponse: action.payload,
        createSubscriptionPlanError: undefined,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_FAILURE:
      return {
        ...state,
        createSubscriptionPlanResponse: undefined,
        createSubscriptionPlanError: action.payload,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_FAILURE:
      return {
        ...state,
        createSubscriptionPlanResponse: undefined,
        createSubscriptionPlanError: action.payload,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_SUCCESS:
      return {
        ...state,
        createSubscriptionResponse: action.payload,
        createSubscriptionError: undefined,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_FAILURE:
      return {
        ...state,
        createSubscriptionResponse: undefined,
        createSubscriptionError: action.payload,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_SUCCESS:
      return {
        ...state,
        createSubscriptionServerResponse: action.payload,
        createSubscriptionServerError: undefined,
      };

    case PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_FAILURE:
      return {
        ...state,
        createSubscriptionServerResponse: undefined,
        createSubscriptionServerError: action.payload,
      };

      case PaymentActionTypes.CANCEL_SUBSCRIPTION_SUCCESS:
        return {
          ...state,
          cancelSubscriptionResponse: action.payload,
          cancelSubscriptionError: undefined,
        };
  
      case PaymentActionTypes.CANCEL_SUBSCRIPTION_FAILURE:
        return {
          ...state,
          cancelSubscriptionResponse: undefined,
          cancelSubscriptionError: action.payload,
        };

      
      
      
    default:
      return state;
  }
};
export default reducers;




