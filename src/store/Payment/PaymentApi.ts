import config from '@app/config';
import axios from '../../package/axios';
import * as coreAxios from 'axios';

const addAmountRequest = (payload: any) => {
  return axios.post('add-amount', payload.data);

};

const addTransactionRequest = (payload: any) => {
  return axios.post(`add-transaction-details?user_id=${payload.user_id}&transaction_id=${payload.transaction_id}&email_id=${payload.email}&amount=${Number(payload.amount)}&status=${payload.status}`);
};

const getAvailableWalletBalRequest = () => {
  return axios.get('available-balance');
};

const tipByPatronRequest = (payload: any) => {

  console.log('recieve payload', payload)
  return axios.get(`give-tip-by-patrone/${payload.dancer_id}?amount=${payload.amount}`);
  // return axios.post(`give-tip-by-patrone/${payload.dancer_id}`, payload.body);
};

const patronTransHistoryRequest = (payload: any) => {
  return axios.get('transaction-patrone', payload);
};

const dancerMoneyTransfRequest = (payload: any) => {
  return axios.post('transfer-money-for-dancer', payload);
};

const connectDisconnentPaypalRequest = (payload: any) => {
  return axios.get(`is-paypal-connected?is_paypal_connect=${payload}`);
};

const createSubscriptionToServer = (payload: any) => {
  console.log('recieve payload', payload)

  return axios.post(`create-subscription`, payload.data);
};

// PAY PAL :
const getPPAccessTokenRequest = () => {
  return coreAxios.default.post(
    'https://api.sandbox.paypal.com/v1/oauth2/token',
    `grant_type=client_credentials`,
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      auth: {
        username: config.clientID,
        password: config.secret,
      },
    },
  );
};

const getPaymentDataRequest = (payload: any) => {
  return coreAxios.default.post(
    'https://api.sandbox.paypal.com/v1/payments/payment',
    payload.data,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${payload.access_token}`,
      },
    },
  );
};

const createProductForSubscription = (payload: any) => {
  return coreAxios.default.post(
    'https://api-m.sandbox.paypal.com/v1/catalogs/products',
    payload.data,
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${payload.access_token}`,
      },
    },
  );
};


const createSubscriptionPlan = (payload: any) => {
  return coreAxios.default.post(
    'https://api-m.sandbox.paypal.com/v1/billing/plans',
    payload.data,
    {
      headers: {
        'Content-Type': 'application/json',
        Prefer: 'return=representation',
        Accept: 'application/json',
        Authorization: `Bearer ${payload.access_token}`,
      },
    },
  );
};

const createSubscription = (payload: any) => {
  return coreAxios.default.post(
    'https://api-m.sandbox.paypal.com/v1/billing/subscriptions',
    payload.data,
    {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'PayPal-Request-Id': config.clientID,
        Prefer: 'return=representation',
        Authorization: `Bearer ${payload.access_token}`,
      },
    },
  );
};

const cancelSubscriptionApi = (payload: any) => {
  return coreAxios.default.post(
    `${payload.cancel_url}`,
    payload.data,
    {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'PayPal-Request-Id': config.clientID,
        Prefer: 'return=representation',
        Authorization: `Bearer ${payload.access_token}`,
      },
    },
  );
};


export const PaymentApiRequest = {
  addAmountRequest,
  getAvailableWalletBalRequest,
  tipByPatronRequest,
  patronTransHistoryRequest,
  dancerMoneyTransfRequest,
  getPPAccessTokenRequest,
  getPaymentDataRequest,
  createSubscriptionPlan,
  createSubscription,
  addTransactionRequest,
  connectDisconnentPaypalRequest,
  createProductForSubscription,
  createSubscriptionToServer,
  cancelSubscriptionApi
};
