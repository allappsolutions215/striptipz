import { call, put, takeLatest } from 'redux-saga/effects';

import { PaymentActionTypes } from './PaymentAction';
import { PaymentApiRequest } from './PaymentApi';

// -------------*******------------- ADD AMOUNT YIELD -------------*******------------- \\
function* addAmountYield(action: any) {
  try {
    const res = yield call(PaymentApiRequest.addAmountRequest, action.payload);
    console.log('SUCCESS +++++++++++ addAmountYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.ADD_AMOUNT_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.ADD_AMOUNT_FAILURE,
        payload: res,
      });
    }

  } catch (error) {
    console.log('SUCCESS +++++++++++ addAmountYield', error);
    yield put({

      type: PaymentActionTypes.ADD_AMOUNT_FAILURE,
      payload: error,
    });
  }
}
export function* addAmountSaga() {
  yield takeLatest(PaymentActionTypes.ADD_AMOUNT_REQUEST, addAmountYield);
}

// -------------*******------------- ADD TRANSACTION YIELD -------------*******------------- \\
function* addTransactionYield(action: any) {
  try {
    const res = yield call(PaymentApiRequest.addTransactionRequest, action.payload);
    console.log('SUCCESS +++++++++++ addTransactionYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.ADD_TRANSACTION_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.ADD_TRANSACTION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** addTransactionYield ', error);
    yield put({
      type: PaymentActionTypes.ADD_TRANSACTION_FAILURE,
      payload: error,
    });
  }
}
export function* addTransactionSaga() {
  yield takeLatest(PaymentActionTypes.ADD_TRANSACTION_REQUEST, addTransactionYield);
}

// -------------*******------------- AVAILABLE_WALLET_BALANCE YIELD -------------*******------------- \\
function* getAvailableWalletBalYield(action: any) {
  try {
    const res = yield call(PaymentApiRequest.getAvailableWalletBalRequest);
    console.log('SUCCESS +++++++++++ getAvailableWalletBalYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.AVAILABLE_WALLET_BALANCE_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.AVAILABLE_WALLET_BALANCE_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getAvailableWalletBalYield ', error.response);
    yield put({
      type: PaymentActionTypes.AVAILABLE_WALLET_BALANCE_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getAvailableWalletBalSaga() {
  yield takeLatest(
    PaymentActionTypes.AVAILABLE_WALLET_BALANCE_REQUEST,
    getAvailableWalletBalYield,
  );
}

// -------------*******------------- ADD AMOUNT YIELD -------------*******------------- \\
function* tipByPatronYield(action: any) {

  try {
    const res = yield call(
      PaymentApiRequest.tipByPatronRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ tipByPatronYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.TIP_BY_PATRON_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.TIP_BY_PATRON_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** tipByPatronYield ', error);
    yield put({
      type: PaymentActionTypes.TIP_BY_PATRON_FAILURE,
      payload: error,
    });
  }
}
export function* tipByPatronSaga() {

  yield takeLatest(PaymentActionTypes.TIP_BY_PATRON_REQUEST, tipByPatronYield);
}

// -------------*******------------- PATRON_TRANS_HISTORY YIELD -------------*******------------- \\
function* patronTransHistoryYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.patronTransHistoryRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ patronTransHistoryYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.PATRON_TRANS_HISTORY_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.PATRON_TRANS_HISTORY_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** patronTransHistoryYield ', error);
    yield put({
      type: PaymentActionTypes.PATRON_TRANS_HISTORY_FAILURE,
      payload: error,
    });
  }
}
export function* patronTransHistorySaga() {
  yield takeLatest(
    PaymentActionTypes.PATRON_TRANS_HISTORY_REQUEST,
    patronTransHistoryYield,
  );
}

// -------------*******------------- DANCER_MONEY_TRANSF YIELD -------------*******------------- \\
function* dancerMoneyTransfYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.dancerMoneyTransfRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ dancerMoneyTransfYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.DANCER_MONEY_TRANSF_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.DANCER_MONEY_TRANSF_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** dancerMoneyTransfYield ', error.response);
    yield put({
      type: PaymentActionTypes.DANCER_MONEY_TRANSF_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* dancerMoneyTransfSaga() {
  yield takeLatest(
    PaymentActionTypes.DANCER_MONEY_TRANSF_REQUEST,
    dancerMoneyTransfYield,
  );
}

// -------------*******------------- PAY PAL PP_ACCESS_TOKEN YIELD -------------*******------------- \\
function* getPPAccessTokenYield() {
  try {
    const res = yield call(PaymentApiRequest.getPPAccessTokenRequest);
    console.log('SUCCESS +++++++++++ getPPAccessTokenYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.PP_ACCESS_TOKEN_SUCCESS,
        payload: {
          data: {
            ...res.data
          },
          token_created: new Date(),
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.PP_ACCESS_TOKEN_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    debugger
    console.log('ERROR ****** getPPAccessTokenYield ', error);
    yield put({
      type: PaymentActionTypes.PP_ACCESS_TOKEN_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getPPAccessTokenSaga() {
  yield takeLatest(
    PaymentActionTypes.PP_ACCESS_TOKEN_REQUEST,
    getPPAccessTokenYield,
  );
}

// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* getPaymentDataYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.getPaymentDataRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ getPaymentDataYield', res);
    if (res && res.status) {
      const { id, links } = res.data;
      const approvalUrl = links.find((data) => data.rel == 'approval_url');

      yield put({
        type: PaymentActionTypes.PP_PAYMENT_DATA_SUCCESS,
        payload: {
          paymentId: id,
          approvalUrl: approvalUrl.href,
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.PP_PAYMENT_DATA_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    let temp = error.response;
    console.log('ERROR ****** getPaymentDataYield ', error.response);
    yield put({
      type: PaymentActionTypes.PP_PAYMENT_DATA_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* getPaymentDataSaga() {
  yield takeLatest(
    PaymentActionTypes.PP_PAYMENT_DATA_REQUEST,
    getPaymentDataYield,
  );
}

// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* createProductForSubscriptionYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.createProductForSubscription,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ createProductForSubscription', res);
    if (res && res.status) {

      yield put({
        type: PaymentActionTypes.CREATE_PRODUCT_SUCCESS,
        payload: {
          data: res
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.CREATE_PRODUCT_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** createProductForSubscription ', error);
    yield put({
      type: PaymentActionTypes.CREATE_PRODUCT_FAILURE,
      payload: error,
    });
  }
}
export function* createProductForSubscriptionSaga() {
  yield takeLatest(
    PaymentActionTypes.CREATE_PRODUCT_REQUEST,
    createProductForSubscriptionYield,
  );
}

// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* createSubscriptionPlanYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.createSubscriptionPlan,
      action.payload,
    );
    // console.log('SUCCESS +++++++++++ createSubscriptionPlan', res);
    if (res && res.status) {

      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_SUCCESS,
        payload: {
          data: res
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** createSubscriptionPlan ', error);
    yield put({
      type: PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_FAILURE,
      payload: error,
    });
  }
}
export function* createSubscriptionPlanSaga() {
  yield takeLatest(
    PaymentActionTypes.CREATE_SUBSCRIPTION_PLAN_REQUEST,
    createSubscriptionPlanYield,
  );
}

// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* createSubscriptionYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.createSubscription,
      action.payload,
    );

    if (res && res.status) {
      const { id, links } = res.data;
      const approvalUrl = links.find((data) => data.rel === 'approve');
      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_SUCCESS,
        payload: {
          data: res.data,
          plan_id: id,
          approvalUrl: approvalUrl.href,
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** createSubscriptionPlan ', error);
    yield put({
      type: PaymentActionTypes.CREATE_SUBSCRIPTION_FAILURE,
      payload: error,
    });
  }
}
export function* createSubscriptionSaga() {
  yield takeLatest(
    PaymentActionTypes.CREATE_SUBSCRIPTION_REQUEST,
    createSubscriptionYield,
  );
}

// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* createSubscriptionToServerYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.createSubscriptionToServer,
      action.payload,
    );

    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_SUCCESS,
        payload: {
          data: res.msg,
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_FAILURE,
      payload: error,
    });
  }
}
export function* createSubscriptionToServerSaga() {
  yield takeLatest(
    PaymentActionTypes.CREATE_SUBSCRIPTION_TO_SERVER_REQUEST,
    createSubscriptionToServerYield,
  );
}


// -------------*******------------- PAY PAL PP_PAYMENT_DATA YIELD -------------*******------------- \\
function* cancelSubscriptionYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.cancelSubscriptionApi,
      action.payload,
    );

    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.CANCEL_SUBSCRIPTION_SUCCESS,
        payload: {
          data: res,
        },
      });
    } else {
      yield put({
        type: PaymentActionTypes.CANCEL_SUBSCRIPTION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {

    yield put({
      type: PaymentActionTypes.CANCEL_SUBSCRIPTION_FAILURE,
      payload: error,
    });
  }
}
export function* cancelSubscriptionSaga() {
  yield takeLatest(
    PaymentActionTypes.CANCEL_SUBSCRIPTION_REQUEST,
    cancelSubscriptionYield,
  );
}

// -------------*******------------- PAY PAL connect/disconnect YIELD -------------*******------------- \\

function* connentDisconnectPayPalYield(action: any) {
  try {
    const res = yield call(
      PaymentApiRequest.connectDisconnentPaypalRequest,
      action.payload,
    );
    console.log('SUCCESS +++++++++++ connentDisconnectPayPalYield', res);
    if (res && res.status) {
      yield put({
        type: PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** connentDisconnectPayPalYield ', error);
    yield put({
      type: PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_FAILURE,
      payload: error,
    });
  }
}
export function* connentDisconnectPayPalSaga() {
  yield takeLatest(
    PaymentActionTypes.CONNECT_DISCONNECT_PAYPAL_REQUEST,
    connentDisconnectPayPalYield,
  );
}


