import axios from '../../package/axios';

const getNotificationRequest = () => {
  return axios.get(`get-dancer-notification`);
};

const deleteNotificationRequest = () => {
  return axios.get(`delete-dancer-notification`);
};

export const NotificationApiRequest = {
  getNotificationRequest,
  deleteNotificationRequest
};
