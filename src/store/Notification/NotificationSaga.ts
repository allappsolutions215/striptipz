import {call, put, takeLatest} from 'redux-saga/effects';

import {NotificationActionTypes} from './NotificationAction';

import {NotificationApiRequest} from './NotificationApi';

// -------------*******------------- USER YIELD -------------*******------------- \\
function* notificationYield() {
  try {
    const res = yield call(
      NotificationApiRequest.getNotificationRequest
    );
    console.log('SUCCESS +++++++++++ getDancersListYield', res);
    if (res && res.status) {
      yield put({
        type: NotificationActionTypes.NOTIFICATION_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: NotificationActionTypes.NOTIFICATION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getDancersListYield ', error.response);
    yield put({
      type: NotificationActionTypes.NOTIFICATION_FAILURE,
      payload: error.response.data,
    });
  }
}
export function* notificationSaga() {
  yield takeLatest(NotificationActionTypes.NOTIFICATION_REQUEST, notificationYield);
}

function* deleteNotificationYield() {
  try {
    const res = yield call(
      NotificationApiRequest.deleteNotificationRequest
    );
    console.log('SUCCESS +++++++++++ getDancersListYield', res);
    if (res && res.status) {
      yield put({
        type: NotificationActionTypes.DELETE_NOTIFICATION_SUCCESS,
        payload: res,
      });
    } else {
      yield put({
        type: NotificationActionTypes.DELETE_NOTIFICATION_FAILURE,
        payload: res,
      });
    }
  } catch (error) {
    console.log('ERROR ****** getDancersListYield ', error.response);
    yield put({
      type: NotificationActionTypes.DELETE_NOTIFICATION_FAILURE,
      payload: error.response,
    });
  }
}
export function* deleteNotificationSaga() {
  yield takeLatest(NotificationActionTypes.DELETE_NOTIFICATION_REQUEST, deleteNotificationYield);
}
