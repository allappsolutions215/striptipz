export const NotificationActionTypes = {
  NOTIFICATION_REQUEST: 'NOTIFICATION_REQUEST',
  NOTIFICATION_SUCCESS: 'NOTIFICATION_SUCCESS',
  NOTIFICATION_FAILURE: 'NOTIFICATION_FAILURE',

  DELETE_NOTIFICATION_REQUEST: 'DELETE_NOTIFICATION_REQUEST',
  DELETE_NOTIFICATION_SUCCESS: 'DELETE_NOTIFICATION_SUCCESS',
  DELETE_NOTIFICATION_FAILURE: 'DELETE_NOTIFICATION_FAILURE',

};

const notificationAction = () => {
  return {type: NotificationActionTypes.NOTIFICATION_REQUEST};
};

const deleteNotificationAction = () => {
  return {type: NotificationActionTypes.DELETE_NOTIFICATION_REQUEST};
};


export default {
  notificationAction,
  deleteNotificationAction
};
