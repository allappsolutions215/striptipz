import {NotificationActionTypes} from './NotificationAction';

interface StateI {
  notificationResponse: any;
  notificationError: any;

  deleteNotificationResponse: any;
  deleteNotificationError: any;
}

const initialState: StateI = {
  notificationResponse: undefined,
  notificationError: undefined,
  deleteNotificationResponse: undefined,
  deleteNotificationError: undefined
};

const reducers = (state = initialState, action: any) => {
  switch (action.type) {
    case NotificationActionTypes.NOTIFICATION_SUCCESS:
      return {
        ...state,
        notificationResponse: action.payload,
        notificationError: undefined,
      };

    case NotificationActionTypes.NOTIFICATION_FAILURE:
      return {
        ...state,
        notificationResponse: undefined,
        notificationError: action.payload,
      };

    case NotificationActionTypes.DELETE_NOTIFICATION_SUCCESS:
      return {
        ...state,
        deleteNotificationResponse: action.payload,
        deleteNotificationError: undefined,
      };

    case NotificationActionTypes.DELETE_NOTIFICATION_FAILURE:
      return {
        ...state,
        deleteNotificationResponse: undefined,
        deleteNotificationError: action.payload,
      };

    default:
      return state;
  }
};
export default reducers;
