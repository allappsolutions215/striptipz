import React, {useState, useEffect, useRef} from 'react';
import {Animated, View, StyleSheet, SafeAreaView, Platform} from 'react-native';
import R from '@app/res/R';
import Text from '@app/components/common/Text';

export interface PriceProps {
  onPress?: () => void;
  title: string;
  value?: string;
  startAnimation: boolean;
  stopAnimation: Function;
}

const NoInternetView: React.FC<any> = (props: any) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.container}>
        <Text variant={'title2'} style={styles.titleText}>
          {R.strings.error.no_internet}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export default NoInternetView;

const styles = StyleSheet.create({
  titleText: {
    color: R.color.white,
    paddingVertical: R.unit.scale(5),
  },
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: R.color.red,
    marginTop: Platform.OS == 'android' ? R.unit.scale(15) : 0,
  },
});
