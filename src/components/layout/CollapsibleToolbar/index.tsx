import R from '@app/res/R';
import React, {Component, useState} from 'react';
import {
  Animated,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import SafeArea from '../SafeArea';

const HEADER_EXPANDED_HEIGHT = 200; // that was 150
const HEADER_COLLAPSED_HEIGHT = 56;

const TITLE_EXPANDED_HEIGHT = 24;
const TITLE_COLLAPSED_HEIGHT = 16;
const {width: SCREEN_WIDTH} = Dimensions.get('screen');

export interface IProps {
  headerColor?: string;
  headerColorDark?: string;
  bottomBarColor?: string;
  image?: any;
  title?: string;
  backPress?: any;
}

const CollapsibleToolbar: React.FC<IProps> = (props) => {
  const [scrollY, setScrollY] = useState(new Animated.Value(0));
  const [open, setOpen] = useState(true);

  const headerHeight = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [HEADER_EXPANDED_HEIGHT, HEADER_COLLAPSED_HEIGHT],
    extrapolate: 'clamp',
  });

  const headerSlide = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [0, 32],
    extrapolate: 'clamp',
  });

  const headerTitleSize = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [TITLE_EXPANDED_HEIGHT, TITLE_COLLAPSED_HEIGHT],
    extrapolate: 'clamp',
  });

  const headerTitleOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_EXPANDED_HEIGHT - HEADER_COLLAPSED_HEIGHT],
    outputRange: [1, 0],
    extrapolate: 'clamp',
  });

  const onLayout = (event: any) => {
    const {x, y, height, width} = event.nativeEvent.layout;
    setOpen(height === HEADER_COLLAPSED_HEIGHT ? false : true);
  };

  const _renderToolbar = () => {
    return (
      <View style={{width: '100%'}}>
        <Image style={{height: 30, width: 30}} source={R.image.app_512()} />
      </View>
    );
  };

  return (
    <SafeArea
      statusBarColor={open ? props.headerColor : props.headerColorDark}
      bottomBarColor={
        props.bottomBarColor ? props.bottomBarColor : R.color.white
      }
      statusBarStyle={!open ? 'dark-content' : 'light-content'}>
      <View style={styles.container}>
        {_renderToolbar()}
        <Animated.View
          onLayout={(event) => onLayout(event)}
          style={[
            styles.header,
            {
              height: headerHeight,
              //backgroundColor:'white'
              backgroundColor: props.headerColor
                ? props.headerColor
                : R.color.black,
            },
          ]}>
          <Animated.Text
            style={[
              styles.headerTitle,
              styles.maxHeader,
              {
                paddingLeft: headerSlide,
                fontSize: headerTitleSize,
                opacity: headerTitleOpacity,
              },
            ]}>
            {props.title ? props.title : ' Title'}
          </Animated.Text>
          <View style={[styles.appBar, {height: 56}]}>
            {/* <TouchableOpacity onPress={() => props.backPress()}>
              <Image
                style={styles.backIcon}
                source={R.image.back()}
                resizeMode={'contain'}
              />
            </TouchableOpacity> */}

            {!open && (
              <Text style={styles.minHeader}>
                {props.title ? props.title : 'Title'}
              </Text>
            )}
          </View>

          <Animated.Image
            style={[
              styles.image,
              {opacity: imageOpacity, resizeMode: 'contain'},
            ]}
            source={props.image}
          />
        </Animated.View>
        <ScrollView
          contentContainerStyle={styles.scrollContainer}
          onScroll={Animated.event([
            {
              nativeEvent: {
                contentOffset: {
                  y: scrollY,
                },
              },
            },
          ])}
          scrollEventThrottle={16}>
          {props.children}
        </ScrollView>
      </View>
    </SafeArea>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: R.color.black,
  },
  scrollContainer: {
    paddingTop: HEADER_EXPANDED_HEIGHT,
  },
  header: {
    position: 'absolute',
    width: SCREEN_WIDTH,
    top: 0,
    left: 0,
    zIndex: 2,
  },
  title: {
    marginVertical: 16,
    fontWeight: 'bold',
    fontSize: 24,
  },
  headerTitle: {
    letterSpacing: 0,
    textAlign: 'center',
    position: 'absolute',
    bottom: 16,
    color: R.color.white,
    zIndex: 99,
  },
  maxHeader: {
    fontSize: 24,
    left: 16,
    lineHeight: 38,
    color: R.color.white,
  },
  minHeader: {
    fontSize: 16,
    paddingLeft: 16,
    color: R.color.black,
  },
  backIcon: {
    zIndex: 99,
    height: 20,
    width: 20,
  },
  image: {
    resizeMode: 'contain',
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  appBar: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 16,
    paddingTop: 16,
    backgroundColor: R.color.white,
  },
});

export default CollapsibleToolbar;
