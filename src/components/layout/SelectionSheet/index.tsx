import React, {FC, useRef} from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';
import {Switch} from 'react-native-switch';

import R from '@app/res/R';
import Text from '@app/components/common/Text';
import RBSheet from 'react-native-raw-bottom-sheet';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

export interface HeaderProps {
  setImageUri(uri: string): Function;
  refRBSheet: any;
}

const DancerHeader: FC<HeaderProps> = (props) => {
  const {setImageUri, refRBSheet} = props;
  //const refRBSheet = useRef();

  const openImagePicker = (type: string) => {
    if (type == 'camera') {
      launchCamera(
        {
          mediaType: 'photo',
          includeBase64: false,
          //  maxHeight: 200,
          //  maxWidth: 200,
        },
        (response) => {
          //{"fileName": "rn_image_picker_lib_temp_2e55ffb8-ddcb-480f-9e97-346a0f715695.jpg", "fileSize": 4005635, "height": 3880, "type": "image/jpeg",
          //"uri": "content://com.striptipz.imagepickerprovider/cacheDir/rn_image_picker_lib_temp_2e55ffb8-ddcb-480f-9e97-346a0f715695.jpg", "width": 5184}

          refRBSheet.current.close();
          if (response.uri) {
            setImageUri(response.uri);
          }
        },
      );
    } else {
      launchImageLibrary(
        {
          mediaType: 'photo',
          includeBase64: false,
          //  maxHeight: 200,
          //  maxWidth: 200,
        },
        (response) => {
          if (response.uri) {
            setImageUri(response.uri);
          }
          //{"fileName": "rn_image_picker_lib_temp_e086c13a-1f56-48a3-9d54-83a846d2d549.jpg", "fileSize": 4136509, "height": 3880, "type": "image/jpeg",
          //"uri": "content://com.striptipz.imagepickerprovider/cacheDir/rn_image_picker_lib_temp_e086c13a-1f56-48a3-9d54-83a846d2d549.jpg", "width": 5184 }
          refRBSheet.current.close();
        },
      );
    }
  };

  return (
    <RBSheet
      ref={refRBSheet}
      closeOnDragDown={true}
      closeOnPressMask={true}
      customStyles={{
        wrapper: {
          backgroundColor: 'transparent',
        },
        draggableIcon: {
          backgroundColor: '#000',
        },
      }}>
      <View style={styles.sheetContainer}>
        <TouchableOpacity
          onPress={() => openImagePicker('camera')}
          style={styles.touch}
          activeOpacity={0.8}>
          <View style={styles.optionContainer}>
            <Image
              style={styles.optionImg}
              resizeMode={'contain'}
              source={R.image.camera()}
            />
            <Text color={R.color.primary} style={styles.optionTxt}>
              {R.strings.profile.camera}
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => openImagePicker('gallery')}
          style={styles.touch}
          activeOpacity={0.8}>
          <View style={styles.optionContainer}>
            <Image
              style={styles.optionImg}
              resizeMode={'contain'}
              source={R.image.gallery()}
            />
            <Text color={R.color.primary} style={styles.optionTxt}>
              {R.strings.profile.gallery}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </RBSheet>
  );
};

const styles = StyleSheet.create({
  constainer: {
    width: '100%',
    height: R.unit.scale(200),
    paddingTop: R.unit.scale(10),
  },
  sheetContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignContent: 'center',
    flexDirection: 'row',
  },
  optionImg: {width: '80%', height: '50%'},
  optionTxt: {
    textAlign: 'center',
    marginTop: R.unit.scale(15),
    fontWeight: 'bold',
  },
  optionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  touch: {
    flex: 1,
  },
});

export default DancerHeader;
