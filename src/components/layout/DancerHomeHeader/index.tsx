import React, { FC } from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image,
  ImageBackground,
} from 'react-native';
import { Switch } from 'react-native-switch';

import R from '@app/res/R';
import Text from '@app/components/common/Text';
import config from '@app/config';

export interface HeaderProps {
  navigation: any;
  toggleOnline: Function;
  userResponse: any;
}

const DancerHeader: FC<HeaderProps> = (props) => {
  const { navigation, toggleOnline, userResponse } = props;
  console.log('user response from dancer header', userResponse)
  return (
    <ImageBackground style={styles.constainer} source={R.image.black_bg()}>
      <View>
        <View style={styles.subContainer}>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('MyAccount')}>
            <Image
              style={styles.imgProfile}
              source={
                userResponse &&
                  userResponse.images &&
                  userResponse.images[0] &&
                  userResponse.images[0].image
                  ? { uri: config.imageBaseUrl + userResponse.images[0].image }
                  : R.image.girl()
              }
            />
          </TouchableOpacity>

          <Text style={styles.text}>
            {userResponse && userResponse.name ? userResponse.name : 'NA'}
          </Text>
          <TouchableOpacity
            activeOpacity={0.8}
            onPress={() => navigation.navigate('AllNotification')}>
            <Image style={styles.img} source={R.image.bell()} />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          width: '90%',
          height: R.unit.scale(150),
          alignItems: 'center',
          paddingTop: R.unit.scale(10),
          justifyContent: 'space-evenly',
          alignSelf: 'center',
        }}>
        <Switch
          activeText={R.strings.common.online}
          inActiveText={R.strings.common.offline}
          value={
            userResponse && userResponse.is_online
              ? userResponse.is_online
              : false
          }
          onValueChange={(val) => toggleOnline(val)}
          switchWidthMultiplier={2.8}
          activeTextStyle={{ fontSize: R.unit.scale(10) }}
          inactiveTextStyle={{ fontSize: R.unit.scale(10) }}
          backgroundActive={R.color.primary}
        />
        <Text style={{ fontSize: R.unit.scale(20) }}>
          {R.strings.common.total_tips_earned}
        </Text>
        <Text
          style={{
            fontSize: R.unit.scale(30),
            color: R.color.primary,
            fontWeight: 'bold',
          }}>
          ${userResponse && userResponse.total_tip ? userResponse.total_tip : 0}
        </Text>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  constainer: {
    width: '100%',
    height: R.unit.scale(200),
    paddingTop: R.unit.scale(10),
  },
  subContainer: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
  },
  img: { height: 30, width: 30 },
  imgProfile: { height: 35, width: 35, borderRadius: 35 / 2 },
  text: {
    flex: 1,
    textAlign: 'left',
    color: 'white',
    marginStart: 15,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default DancerHeader;
