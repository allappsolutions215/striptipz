import React, { FC, useRef, useEffect, useState } from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  PermissionsAndroid,
  Dimensions,
  Platform,
  Alert,
  Linking,
} from 'react-native';
import { Switch } from 'react-native-switch';

import R from '@app/res/R';
import Text from '@app/components/common/Text';
import Image from '@app/components/common/Image';
import Geolocation from '@react-native-community/geolocation';

import MapView, {
  Marker,
  Circle,
  Polygon,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import config from '@app/config';
const { width, height } = Dimensions.get('window');
const SPACE_LAT = -0.3;
const SPACE_LNG = -0.00001;

export interface HeaderProps {
  navigation: any;
  currentLocation: Function;
  dancerList: any;
}

const PositionView: FC<HeaderProps> = (props) => {
  const circleRef1 = useRef(null);
  const circleRef2 = useRef(null);
  const circleRef3 = useRef(null);
  const circleRef4 = useRef(null);
  const circleRef5 = useRef(null);
  const google_map = useRef(null);

  const ASPECT_RATIO = width / height;
  const LATITUDE = 37.787471437115805;
  const LONGITUDE = -122.45422817766666;
  const LATITUDE_DELTA = 0.01;
  const LONGITUDE_DELTA = 0.01;
  // const LATITUDE_DELTA = 0.0922;
  // const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

  const [region, setRegion] = useState({
    latitude: -37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  const _setRegion = (region: any) => {
    if (ready) {
      setTimeout(() => {
        if (google_map && google_map.current) {
          google_map.current.animateToRegion(region);
        }
      }, 10);
    }
  };
  const [ready, setReady] = useState(true);
  const [markers, setMarkers] = useState(props.dancerList);
  const [currentPostion, setCurrectPostion] = useState();

  const [circle, setCircle] = useState({
    center: {
      latitude: LATITUDE,
      longitude: LONGITUDE,
    },
    radius: 200000,
  });

  useEffect(() => {
    const props = { strokeColor: R.color.primary };

    getLocationPermission();
  }, []);

  useEffect(() => {
    // TODO EMPR DELETE LOG
    if (props.dancerList) {
      var data = [];
      let demo_data = { "distance": 15.470805976615924, "id": 56, "image": "dancer/eVnvdPE5lbBinoWaHjrcHJ3pfGHgKeAck70G8wUT.jpg", "latitude": 38.351595, "longitude": -122.302285, "name": "Demo Hc" }
      let demo_data1 = { "distance": 4.766545203704639, "id": 62, "image": "dancer/CWZBgxL4lVmC1Vm4KyXwyZkUTrnRSAmZiofeEZ3b.jpg", "latitude": 36.649027, "longitude": -122.335192, "name": "Demo Andreaa" }
      data.push(demo_data)
      data.push(demo_data1)

      props.dancerList.map((item: any, index: any) => {
        data.push({
          ...item,
          latitude: Number(item.latitude) + SPACE_LAT,
          longitude: Number(item.longitude) + SPACE_LNG,
        });
      });
      if (currentPostion) {
        data.push(currentPostion);
      }


      setMarkers(data);
    }
  }, [props.dancerList]);

  const onMapReady = (e: any) => {
    if (!ready) {
      setReady(true);
    }
  };

  const onRegionChange = (region: any) => {
  };

  const onRegionChangeComplete = (region: any) => {
  };

  const getLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      getCurrentPosition();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Device current location permission',
            message: 'Allow app to get your current location',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getCurrentPosition();
        } else {
        }
      } catch (err) {
        console.warn(143, err);
      }
    }
  };

  const getCurrentPosition = () => {
    try {
      Platform.OS === 'ios' && Geolocation.requestAuthorization();
      Geolocation.getCurrentPosition(
        (position) => {

          setCircle({
            center: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            },
            radius: 200000,
          });

          setCurrectPostion({
            latitude: position.coords.latitude + SPACE_LAT,
            longitude: position.coords.longitude + SPACE_LNG,
            name: 'You',
          });

          setMarkers([
            ...markers,
            {
              latitude: position.coords.latitude + SPACE_LAT,
              longitude: position.coords.longitude + SPACE_LNG,
              name: 'You',
            },
          ]);

          props.currentLocation({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });

          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          _setRegion(region);
        },
        (error) => {
          console.warn(187, error);
          if (error.code == 2) {
            // Linking.openSettings();
            Alert.alert(R.strings.app_name, R.strings.common.turn_location_on);
          } else {
            alert(error.message);
          }
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
      );
    } catch (e) {
      alert(e.message || '');
    }
  };

  const _renderProfileIcon = () => {
    return (
      <View style={styles.profileIconContainer}>
        <Image
          resizeMode={'contain'}
          imageStyles={styles.profileIcon}
          source={R.image.user()}
        />
      </View>
    );
  };

  const onMapPress = (e: any) => {

    setMarkers([
      ...markers,
      {
        coordinate: e.nativeEvent.coordinate,
        //name: `foo${id++}`,
        name: 'Alexxx',
      },
    ]);
  };

  const mapStyle = [
    {
      elementType: 'geometry',
      stylers: [
        {
          color: R.color.black,
        },
      ],
    },
    // {
    //   elementType: 'all',
    //   stylers: [
    //     {color: R.color.black},
    //     {visibility: 'on'},
    //     {backgroundColor: R.color.black},
    //     {zIndex: 9999},
    //   ],
    // },
    { elementType: 'labels.icon', stylers: [{ visibility: 'off' }] },
    { elementType: 'labels.text', stylers: [{ visibility: 'off' }] },
  ];

  const _renderCircle = (ref: any, radius: number, strokeWidth: any) => {
    return (
      <Circle
        ref={ref}
        onLayout={() =>
          ref.current.setNativeProps({
            strokeColor: R.color.primary,
          })
        }
        center={circle.center}
        radius={radius}
        strokeColor="green"
        strokeWidth={strokeWidth}
      />
    );
  };

  return (
    <View style={styles.constainer}>
      {/* {_renderProfileIcon()} */}

      <MapView
        customMapStyle={mapStyle}
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        initialRegion={region}
        // showsUserLocation
        followsUserLocation
        //mapType={'satellite'}
        scrollEnabled={false}
        zoomEnabled={false}
        maxZoomLevel={6.5}
        // minZoomLevel={18}
        showsMyLocationButton={false}
        //onPress={onMapPress}
        ref={google_map}
        //data={markers}
        //renderMarker={renderMarker}
        onMapReady={onMapReady}
        onRegionChange={onRegionChange}
        onRegionChangeComplete={onRegionChangeComplete}
        // style={StyleSheet.absoluteFill}
        //textStyle={{ color: '#bc8b00' }}
        containerStyle={{
          backgroundColor: R.color.black,
          borderColor: '#BC8B00',
        }}>
        {_renderCircle(circleRef1, circle.radius, 0.8)}

        {_renderCircle(circleRef2, 160000, 1)}

        {_renderCircle(circleRef3, 120000, 1.5)}

        {_renderCircle(circleRef4, 80000, 2)}

        {_renderCircle(circleRef5, 40000, 3)}

        {markers.map((marker, index) => {
          return (
            <Marker
              title={marker && marker.name}
              key={index}
              // key={marker && marker.name}
              onPress={() => {
                marker && marker.name == 'You'
                  ? props.navigation.navigate('MyAccount')
                  : props.navigation.navigate('DancerProfile', { dancerData: marker })
              }}
              coordinate={{
                latitude: marker && marker.latitude,
                longitude: marker && marker.longitude,
              }}>
              {marker && marker.name == 'You' ? (
                <Image
                  resizeMode={'contain'}
                  imageStyles={styles.profileIcon}
                  containerStyles={styles.profileIconContainer}
                  source={R.image.user()}
                />
              ) : (
                <Image
                  source={
                    marker && marker.image
                      ? { uri: config.imageBaseUrl + marker.image }
                      : R.image.girl()
                  }
                  resizeMode="cover"
                  imageStyles={{
                    width: 50,
                    height: 50,
                    borderRadius: 25,
                    zIndex: 99,
                  }}
                  containerStyles={{
                    width: 50,
                    height: 50,
                    borderRadius: 25,
                  }}
                />
              )}
            </Marker>
          );
        })}
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  constainer: { flex: 0.8 },
  root: { width: '100%', height: '100%' },
  absoluteRoot: {
    width: '100%',
    height: '100%',
  },
  profileIcon: { width: R.unit.scale(30), height: R.unit.scale(30), zIndex: 99 },
  profileIconContainer: {
    width: R.unit.scale(45),
    height: R.unit.scale(45),
    borderRadius: R.unit.scale(45 / 2),
    backgroundColor: R.color.primary,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  rendomStyle: {
    width: R.unit.scale(45),
    height: R.unit.scale(45),
    position: 'absolute',
    top: Math.floor(Math.random() * 50),
    left: '50%',
  },

  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default PositionView;
