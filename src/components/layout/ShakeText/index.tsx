import React, {useState, useEffect, useRef} from 'react';
import {Animated, View, StyleSheet, Image as ImageR} from 'react-native';
import R from '@app/res/R';
import Text from '@app/components/common/Text';

export interface PriceProps {
  onPress?: () => void;
  title: string;
  value?: string;
  startAnimation: boolean;
  stopAnimation: Function;
}

const AccountItem: React.FC<any> = (props: any) => {
  const shakeAnimation = new Animated.Value(0);
  const {title, stopAnimation} = props;
  var {startAnimation} = props;

  useEffect(() => {
    if (startAnimation) {
      startShake();
    }
  }, [startAnimation]);

  const startShake = () => {
    Animated.sequence([
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }),
    ]).start(() => {
      stopAnimation();
    });
  };
  return (
    <Animated.Text
      style={[styles.titleText, {transform: [{translateX: shakeAnimation}]}]}>
      {title}
    </Animated.Text>
  );
};

export default AccountItem;

const styles = StyleSheet.create({
  titleText: {margin: R.unit.scale(10), color: R.color.black},
});
