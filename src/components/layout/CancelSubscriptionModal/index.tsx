//main react imports
import React, {FC} from 'react';
import {View, StyleSheet, Modal} from 'react-native';

//resources
import R from '@app/res/R';
import Image from '@app/components/common/Image';
import Text from '@app/components/common/Text';
import Button from '@app/components/common/Button';

export interface IProps {
  isVisible: boolean;
  onPress: Function;
}

const CancelSubscriptionModal: FC<IProps> = (props) => {
  const {isVisible, onPress} = props;

  return (
    <Modal
      style={styles.modal}
      animationType={'fade'}
      transparent={true}
      visible={isVisible}>
      <View style={styles.container}>
        <View style={styles.subContainer}>
          <Text
            variant={'title1'}
            color={R.color.black}
            style={styles.titleText}>
            {R.strings.payPal.are_you_sure}
          </Text>
          <View style={styles.buttonContainer}>
            <Button
              btnWrapperStyles={styles.button}
              onPress={() => onPress(true)}
              text={R.strings.common.yes}
            />
            <Button
              btnWrapperStyles={styles.buttonNo}
              onPress={() => onPress(false)}
              text={R.strings.common.no}
              color={R.color.black}
            />
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default CancelSubscriptionModal;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
    flex: 1,
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
  },
  modal: {
    justifyContent: 'flex-end',
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    flex: 1,
  },
  titleText: {textAlign: 'center', lineHeight: R.unit.scale(22)},
  subContainer: {
    width: '90%',
    alignSelf: 'center',
    backgroundColor: R.color.white,
    borderRadius: R.unit.scale(5),
    paddingVertical: R.unit.scale(25),
  },
  button: {
    flex: 1,
    marginHorizontal: R.unit.scale(15),
  },
  buttonNo: {
    flex: 1,
    marginHorizontal: R.unit.scale(15),
    backgroundColor: R.color.white,
    borderWidth: 1,
    borderColor: R.color.black,
  },
});
