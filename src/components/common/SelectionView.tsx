import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image as ImageR,
} from 'react-native';
import R from '@app/res/R';
import Image from '@app/components/common/Image';
import Text from '@app/components/common/Text';

export interface PriceProps {
  checkedIndex: string;
  disabled?: boolean;
  icon: any;
  title: string;
  firstText: string;
  secondText: string;
  onPress: () => void;
}

const SelectionView: React.FC<any> = (props: any) => {
  const {checkedIndex, onPress, firstText, secondText, title, disabled} = props;
  return (
    <View style={styles.container}>
      {title && (
        <Text color={R.color.white} variant="title4" style={styles.title}>
          {title}
        </Text>
      )}
      <View style={styles.subContainer}>
        <TouchableOpacity
          style={{flex: 1}}
          disabled={disabled}
          activeOpacity={1}
          onPress={() => onPress(firstText)}>
          <View
            style={[
              checkedIndex.toLowerCase() == firstText.toLowerCase()
                ? styles.rootSelected
                : styles.rootUnselected,
              ,
              {
                borderLeftWidth: R.unit.scale(1),
                borderTopLeftRadius: R.unit.scale(7),
                borderBottomLeftRadius: R.unit.scale(7),
              },
            ]}>
            {checkedIndex.toLowerCase() == firstText.toLowerCase() && (
              <ImageR
                style={{height: R.unit.scale(12), width: R.unit.scale(12)}}
                resizeMode={'contain'}
                source={R.image.right_icon()}
              />
            )}

            {firstText && (
              <Text color={R.color.white} variant="content" style={styles.text}>
                {firstText}
              </Text>
            )}
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{flex: 1}}
          disabled={disabled}
          activeOpacity={1}
          onPress={() => onPress(secondText)}>
          <View
            style={[
              checkedIndex.toLowerCase() == secondText.toLowerCase()
                ? styles.rootSelected
                : styles.rootUnselected,
              {
                borderRightWidth: R.unit.scale(1),
                borderTopRightRadius: R.unit.scale(7),
                borderBottomRightRadius: R.unit.scale(7),
              },
            ]}>
            {checkedIndex == secondText && (
              <ImageR
                style={{height: R.unit.scale(12), width: R.unit.scale(12)}}
                resizeMode={'contain'}
                source={R.image.right_icon()}
              />
            )}

            {secondText && (
              <Text color={R.color.white} variant="content" style={styles.text}>
                {secondText}
              </Text>
            )}
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SelectionView;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    marginTop: R.unit.scale(20),
  },
  subContainer: {flexDirection: 'row', height: R.unit.scale(45)},
  rootSelected: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: R.color.secondary,
    justifyContent: 'center',
  },
  rootUnselected: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: R.unit.scale(1),
    justifyContent: 'center',
    borderColor: R.color.white,
  },
  text: {
    marginLeft: R.unit.scale(5),
  },
  title: {fontWeight: 'bold', marginBottom: R.unit.scale(10)},
  imgContainer: {
    height: R.unit.scale(18),
    width: R.unit.scale(18),
    borderRadius: R.unit.scale(4),
    borderColor: R.color.white,
    borderWidth: R.unit.scale(1),
    justifyContent: 'center',
    alignItems: 'center',
    padding: R.unit.scale(3),
  },
});
