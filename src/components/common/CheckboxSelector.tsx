import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image as ImageR,
} from 'react-native';
import R from '@app/res/R';
import Image from '@app/components/common/Image';
import Text from '@app/components/common/Text';

export interface PriceProps {
  isChecked?: boolean;
  disabled?: boolean;
  icon: any;
  title: string;
  highlightText: string;
  onPress: () => void;
}

const CheckboxSelector: React.FC<any> = (props: any) => {
  const {isChecked, onPress, highlightText, title, disabled} = props;
  return (
    <TouchableOpacity
      disabled={disabled}
      activeOpacity={1}
      onPress={() => onPress()}>
      <View style={styles.root}>
        <View style={styles.imgContainer}>
          {isChecked && (
            <ImageR
              style={{height: R.unit.scale(13), width: R.unit.scale(13)}}
              resizeMode={'contain'}
              source={R.image.right_icon()}
            />
          )}
        </View>

        <Text color={R.color.white} variant="title7" style={styles.text}>
          {title}{' '}
          {highlightText && (
            <Text color={R.color.yellow} variant="title7" style={styles.text}>
              {highlightText}
            </Text>
          )}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default CheckboxSelector;

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: R.unit.scale(15),
  },
  text: {
    marginLeft: R.unit.scale(15),
  },
  imgContainer: {
    height: R.unit.scale(18),
    width: R.unit.scale(18),
    borderRadius: R.unit.scale(4),
    borderColor: R.color.white,
    borderWidth: R.unit.scale(1),
    justifyContent: 'center',
    alignItems: 'center',
    padding: R.unit.scale(3),
  },
});
