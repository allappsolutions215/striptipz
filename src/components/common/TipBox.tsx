import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TextInputProps,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native';

import R from '../../res/R';
import Text from './Text';

interface PropsI extends TextInputProps {
  onChangeText?: (text: any) => void;
  onHandleClear?: () => void;
  onVerifiedClick?: () => void;

  containerStyle?: object;
  inputContainer?: object;
  style?: object;
  rightComponent?: any;
  textButton?: boolean;
  isSelected: boolean;
  textButtonPressed?: () => void;

  showCancelText?: boolean;
  isVerified?: boolean;

  textInputHeading?: any;

  info?: string;
  error?: string;

  forwardedRef?: any;
  rightSectionStyle?: any;

  text?: string;
  boldText?: string;
  subText?: string;
  textVariant?: string;
  variant?: 'contained' | 'outlined' | 'text';
  size?: 'sm' | 'md' | 'lg';
  gutterBottom?: number;
  gutterTop?: number;
  leftComponent?: any;

  backgroundColor?: string;
  borderColor?: string;
  // text props
  color?: string;
  font?: 'light' | 'regular' | 'medium' | 'bold' | 'semibold';
  boldTextFont?: 'light' | 'regular' | 'medium' | 'bold' | 'semibold';
  transform?: 'none' | 'uppercase' | 'lowercase' | 'capitalize';
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  alignVertical?: 'auto' | 'top' | 'bottom' | 'center';
  // styles
  btnWrapperStyles?: any;
  btnTextStyles?: any;
  btnSubTextStyles?: any;
  leftSectionStyle?: any;
  loading?: any;
  disabled?: boolean;
  onPress: Function;
  activeOpacity?: any;
}

const TipBox: React.FC<PropsI> = (props) => {
  const {
    text,
    containerStyle,
    borderColor = R.color.white,
    backgroundColor,
    disabled,
    gutterTop,
    gutterBottom,
    variant,
    size = 'lg',
    loading,
    onPress,
    activeOpacity = 0.8,
    isSelected,
    ...touchableProps
  } = props;

  return !loading ? (
    <TouchableOpacity
      {...touchableProps}
      activeOpacity={activeOpacity}
      style={{
        minHeight: R.unit.scale(60),
        ...styles.container,
        ...styles[`btn_${variant}`],
        marginBottom: R.unit.scale(gutterBottom),
        marginTop: R.unit.scale(gutterTop),
        opacity: disabled === true ? 0.8 : 1,
        backgroundColor: isSelected ? R.color.black : R.color.transparent,
        borderColor: isSelected ? R.color.black : R.color.white,
        ...containerStyle,
      }}
      onPress={() => (disabled !== true || !loading ? onPress() : undefined)}>
      {text ? (
        <Text variant={text == R.strings.home.plus ? 'title' : 'title3'}>
          {text}
        </Text>
      ) : (
        <View
          style={{
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.crossUp} />
          <View style={styles.crossFlat} />
        </View>
      )}
    </TouchableOpacity>
  ) : (
    <View
      style={{
        minHeight: R.unit.scale(sizes[size]),
        ...styles.container,
        ...styles[`btn_${variant}`],
        marginBottom: R.unit.scale(gutterBottom),
        marginTop: R.unit.scale(gutterTop),
        opacity: disabled === true ? 0.8 : 1,
        backgroundColor,
        borderColor,
        ...containerStyle,
      }}>
      <ActivityIndicator size="large" color={R.color.white} />
    </View>
  );
};
export default TipBox;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: R.unit.scale(5),
    borderWidth: R.unit.scale(0.5),
    borderColor: R.color.white,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: R.unit.scale(7),
  },
  crossUp: {
    backgroundColor: R.color.white,
    height: R.unit.scale(60),
    width: R.unit.scale(0.5),
  },
  crossFlat: {
    backgroundColor: R.color.white,
    height: R.unit.scale(0.5),
    width: R.unit.scale(60),
    position: 'absolute',
  },
});
