import React from 'react';

import R from '@app/res/R';

import {StyleSheet, Image} from 'react-native';

const Card: React.FC<any> = (props) => {
  const {card} = props;

  return (
    <Image
      style={{
        flex: 1,
        transform: [{rotate: '90deg'}],
      }}
      source={R.image.dollar()}
      resizeMode="cover"
    />
  );
};

const styles = StyleSheet.create({});

export default Card;
