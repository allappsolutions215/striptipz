import React, {useState, useEffect} from 'react';
import {
  TextInput as TextInputR,
  View,
  StyleSheet,
  TextInputProps,
} from 'react-native';

import R from '../../res/R';
import Text from './Text';

interface PropsI extends TextInputProps {
  onChangeText?: (text: any) => void;
  onHandleClear?: () => void;
  onVerifiedClick?: () => void;

  containerStyle?: object;
  inputContainer?: object;
  style?: object;
  rightComponent?: any;
  textButton?: boolean;
  textButtonPressed?: () => void;

  showCancelText?: boolean;
  isVerified?: boolean;

  textInputHeading?: any;

  info?: string;
  error?: string;

  forwardedRef?: any;
  rightSectionStyle?: any;
}

const BorderInput: React.FC<PropsI> = (props) => {
  const [value, setValue] = useState('');

  useEffect(() => {
    // update value when receives from parent
    setValue(props.value);
  }, [props.value]);

  const handleChangeText = (text) => {
    setValue(text);

    if (props.onChangeText) {
      props.onChangeText(text);
    }
  };

  const handleClear = () => {
    handleChangeText('');
    if (props.onHandleClear) {
      props.onHandleClear();
    }
  };

  const handleVerifiedClick = () => {
    handleChangeText('');
  };
  return (
    <View
      style={{
        ...styles.container,
        ...props.containerStyle,
      }}>
      <View
        style={{
          ...styles.inputContainer,
          ...props.inputContainer,
        }}>
        <TextInputR
          {...props}
          ref={props.forwardedRef}
          style={{
            ...styles.input,
            ...props.style,
            paddingRight: props.rightComponent ? R.unit.scale(35) : 0,
          }}
          selectionColor={R.color.white}
          placeholderTextColor={
            props.placeholderTextColor
              ? props.placeholderTextColor
              : R.color.white
          }
          keyboardType={props.keyboardType ? props.keyboardType : 'default'}
          returnKeyType={'done'}
          placeholder={props.placeholder ? props.placeholder : ''}
          onChangeText={(e: any) => handleChangeText(e)}
          secureTextEntry={
            props.secureTextEntry ? props.secureTextEntry : false
          }
        />
        {props.rightComponent ? (
          <View style={{...styles.rightSection, ...props.rightSectionStyle}}>
            {props.rightComponent}
          </View>
        ) : null}
      </View>
      {props.error && (
        <Text
          color={R.color.red}
          variant="content"
          style={{
            width: '90%',
            textAlign: 'left',
            marginTop: R.unit.scale(5),
            marginHorizontal: R.unit.scale(20),
          }}>
          {'' + props.error}
        </Text>
      )}
    </View>
  );
};
export default BorderInput;

const styles = StyleSheet.create({
  container: {
    width: '80%',
  },
  inputContainer: {
    flexDirection: 'row',
    height: R.unit.scale(45),
    borderWidth: R.unit.scale(1),
    borderColor: 'white',
    borderRadius: R.unit.scale(7),
    justifyContent: 'flex-start',
    paddingHorizontal: R.unit.scale(15),
    alignItems: 'center',
    marginTop: R.unit.scale(20),
  },
  input: {color: R.color.white, flex: 1, height: '100%'},
  textButton: {
    color: R.color.primary,
    fontFamily: R.font.Regular,
    fontSize: R.unit.scale(10),
  },
  textInputHeading: {
    color: R.color.fontColor1,
  },
  rightSection: {
    flexGrow: 0,
    flexDirection: 'row',
    alignItems: 'center',
    marginEnd: R.unit.scale(5),
  },
});
