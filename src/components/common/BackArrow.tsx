import React, {useState, useEffect} from 'react';
import {
  TextInput as TextInputR,
  View,
  StyleSheet,
  Image,
  TextInputProps,
  TouchableOpacity,
} from 'react-native';

import R from '../../res/R';
import Text from './Text';

interface PropsI extends TextInputProps {
  navigation: any;
}

const BackArrow: React.FC<PropsI> = (props) => {
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={() => props.navigation.goBack()}>
      <View style={styles.touchableView}>
        <Image style={styles.img} source={R.image.back()} />
      </View>
    </TouchableOpacity>
  );
};
export default BackArrow;

const styles = StyleSheet.create({
  touchable: {
    position: 'absolute',
    top: 15,
    left: 20,
    zIndex: 999,
  },
  img: {
    height: R.unit.scale(15),
    width: R.unit.scale(18),
    tintColor: R.color.white,
  },
  touchableView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
