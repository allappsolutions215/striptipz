import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TextInputProps,
  TouchableOpacity,
  ActivityIndicator,
  Image,
} from 'react-native';

import R from '../../res/R';

interface PropsI extends TextInputProps {
  onChangeText?: (text: any) => void;
  onHandleClear?: () => void;
  onVerifiedClick?: () => void;

  containerStyle?: object;
  inputContainer?: object;
  style?: object;
  rightComponent?: any;
  textButton?: boolean;
  textButtonPressed?: () => void;

  showCancelText?: boolean;
  isVerified?: boolean;

  textInputHeading?: any;

  info?: string;
  error?: string;

  forwardedRef?: any;
  rightSectionStyle?: any;

  text?: string;
  boldText?: string;
  subText?: string;
  textVariant?: string;
  variant?: 'contained' | 'outlined' | 'text';
  size?: 'sm' | 'md' | 'lg';
  gutterBottom?: number;
  gutterTop?: number;
  leftComponent?: any;

  backgroundColor?: string;
  backgroundColorLine?: string;
  borderColor?: string;
  // text props
  color?: string;
  font?: 'light' | 'regular' | 'medium' | 'bold' | 'semibold';
  boldTextFont?: 'light' | 'regular' | 'medium' | 'bold' | 'semibold';
  transform?: 'none' | 'uppercase' | 'lowercase' | 'capitalize';
  align?: 'auto' | 'left' | 'right' | 'center' | 'justify';
  alignVertical?: 'auto' | 'top' | 'bottom' | 'center';
  // styles
  btnWrapperStyles?: any;
  btnTextStyles?: any;
  btnSubTextStyles?: any;
  leftSectionStyle?: any;
  loading?: any;
  disabled?: boolean;
  onPress: Function;
  activeOpacity?: any;
  image: any;
}

const PhotoBox: React.FC<PropsI> = (props) => {
  const {
    image,
    containerStyle,
    borderColor = R.color.white,
    backgroundColor,
    disabled,
    gutterTop,
    gutterBottom,
    variant,
    size = 'lg',
    loading,
    onPress,
    activeOpacity = 0.8,
    backgroundColorLine,
    ...touchableProps
  } = props;

  const sizes = {sm: 28, md: 32, lg: 40};
  const borderRadius = {sm: 15, md: 25, lg: 35};
  const fontSizes = {sm: 10, md: 12, lg: 14};

  return (
    <TouchableOpacity
      {...touchableProps}
      activeOpacity={activeOpacity}
      style={{
        minHeight: R.unit.scale(sizes[size]),
        ...styles.container,
        ...styles[`btn_${variant}`],
        marginBottom: R.unit.scale(gutterBottom),
        marginTop: R.unit.scale(gutterTop),
        opacity: disabled === true ? 0.4 : 1,
        backgroundColor,
        borderColor,
        ...containerStyle,
      }}
      onPress={() => (!image ? onPress() : undefined)}>
      {image ? (
        <View style={{...styles.container, ...containerStyle}}>
          {loading && (
            <View style={styles.loadingContainer}>
              <ActivityIndicator size="large" color={R.color.white} />
            </View>
          )}
          <Image
            style={styles.imgContainer}
            source={{uri: image}}
            resizeMode={'cover'}
          />
        </View>
      ) : (
        <View
          style={{
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              ...styles.crossUp,
              backgroundColor: backgroundColorLine
                ? backgroundColorLine
                : R.color.white,
            }}
            // style={styles.crossUp}
          />
          <View
            style={{
              ...styles.crossFlat,
              backgroundColor: backgroundColorLine
                ? backgroundColorLine
                : R.color.white,
            }}
          />
        </View>
      )}
    </TouchableOpacity>
  );
  //  : (
  //   <View
  //     style={{
  //       minHeight: R.unit.scale(sizes[size]),
  //       ...styles.container,
  //       ...styles[`btn_${variant}`],
  //       marginBottom: R.unit.scale(gutterBottom),
  //       marginTop: R.unit.scale(gutterTop),
  //       opacity: disabled === true ? 0.8 : 1,
  //       backgroundColor,
  //       borderColor,
  //       ...containerStyle,
  //     }}>
  //     <ActivityIndicator size="large" color={R.color.white} />
  //   </View>
  // );
};
export default PhotoBox;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    borderWidth: R.unit.scale(0.5),
    borderColor: R.color.white,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    borderRadius: R.unit.scale(7),
  },
  crossUp: {
    backgroundColor: R.color.white,
    height: R.unit.scale(60),
    width: R.unit.scale(0.5),
  },
  crossFlat: {
    backgroundColor: R.color.white,
    height: R.unit.scale(0.5),
    width: R.unit.scale(60),
    position: 'absolute',
  },
  imgContainer: {
    height: '100%',
    width: '100%',
    borderRadius: R.unit.scale(7),
  },
  loadingContainer: {
    backgroundColor: R.color.whiteTrasparent,
    position: 'absolute',
    width: R.unit.scale(80),
    height: R.unit.scale(80),
    zIndex: 999,
    borderRadius: R.unit.scale(40),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
