import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image as ImageR,
} from 'react-native';
import R from '@app/res/R';
import Image from '@app/components/common/Image';
import Text from '@app/components/common/Text';

export interface PriceProps {
  onPress?: () => void;
  title: string;
  value?: string;
}

const AccountItem: React.FC<any> = (props: any) => {
  const { onPress, title, value } = props;
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => (onPress ? onPress(title) : undefined)}>
      <View style={styles.container}>
        <Text variant={'title2'} style={styles.titleText}>
          {title}
        </Text>
        {value ? (
          <Text variant={'_19'} style={styles.valueText}>
            {value}
          </Text>
        )
          :
          null
        }
      </View>
    </TouchableOpacity>
  );
};

export default AccountItem;

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: R.unit.scale(0.3),
    paddingBottom: R.unit.scale(20),
    marginTop: R.unit.scale(20),
    borderBottomColor: R.color.gray2,
  },
  titleText: { marginVertical: R.unit.scale(10), color: R.color.gray2 },
  valueText: { marginTop: R.unit.scale(5), color: R.color.black },
});
