import React, {FC} from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  Image as ImageR,
} from 'react-native';

import R from '@app/res/R';
import Text from '../common/Text';
import Image from '../common/Image';

export interface HeaderProps {
  text?: string;
  color?: string;
  navigation: any;
  tintColor?: string;
  showClearText?: boolean;
  showEditIcon?: boolean;
  onClearPress?: Function;
  onEditPress?: Function;
  hideBackButton?: boolean;
}

const AuthHeader: FC<HeaderProps> = (props) => {
  const {
    navigation,
    text,
    color,
    tintColor,
    showClearText,
    onClearPress,
    showEditIcon,
    onEditPress,
    hideBackButton,
  } = props;

  return (
    <View style={styles.constainer}>
      <View style={styles.subContainer}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <View style={styles.touchableView}>
            {!hideBackButton && (
              <ImageR
                style={{...styles.img, tintColor: tintColor}}
                source={R.image.back()}
              />
            )}
            {text && (
              <Text
                style={{
                  ...styles.text,
                  color: color ? color : R.color.white,
                }}>
                {text}
              </Text>
            )}
          </View>
        </TouchableOpacity>

        {showClearText && (
          <Text
            onPress={() =>
              onClearPress
                ? onClearPress()
                : alert(R.strings.notification.clear_all)
            }
            style={{
              ...styles.clearTextStyle,
              color: color ? color : R.color.black,
            }}>
            {R.strings.notification.clear_all}
          </Text>
        )}

        {showEditIcon && (
          <Image
            resizeMode={'cover'}
            style={styles.imgPencil}
            onPressImage={() => (onEditPress ? onEditPress() : undefined)}
            source={R.image.pencil_edit()}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  constainer: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: R.unit.scale(20),
    flexDirection: 'row',
    marginTop: R.unit.scale(15),
  },
  subContainer: {
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  img: {height: R.unit.scale(15), width: R.unit.scale(18)},
  imgPencil: {height: R.unit.scale(10), width: R.unit.scale(15)},
  text: {
    textAlign: 'center',
    color: 'white',
    marginStart: R.unit.scale(15),
    fontSize: R.unit.scale(20),
    fontWeight: 'bold',
  },
  clearTextStyle: {
    color: 'white',
    fontSize: R.unit.scale(14),
    alignSelf: 'flex-end',
    textAlign: 'right',
  },
  touchableView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default AuthHeader;
