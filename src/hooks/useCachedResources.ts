import {Ionicons} from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as React from 'react';

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        //SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'Barlow-Regular': require('../../assets/fonts/Barlow-Regular.ttf'),
          'Barlow-Light': require('../../assets/fonts/Barlow-Light.ttf'),
          'Barlow-Bold': require('../../assets/fonts/Barlow-Bold.ttf'),
          'Barlow-Italic': require('../../assets/fonts/Barlow-Italic.ttf'),
          'Barlow-Semibold': require('../../assets/fonts/Barlow-SemiBold.ttf'),
          'Barlow-Medium': require('../../assets/fonts/Barlow-Medium.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        //SplashScreen.hideAsync();
      }
    }
    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
