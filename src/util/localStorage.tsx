// import * as Cookies from 'js-cookie';
import AsyncStorage from '@react-native-community/async-storage';

export const cookiesStorage = async (key: any, data: any) => {
    try {

        await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (error) {
        // Error saving data
        console.log('error in cookies -=-=-=>', error);
    }
};

export const updateCookiesStorage = async (key, data) => {
    try {
        // await Cookies.set(
        //     key,
        //     JSON.stringify(data)
        // );
        console.log('cookies store success')

    } catch (error) {
        // Error retrieving data
        console.log('error in cookies -=-=-=>', error)
    }
};

export const retrieveCookiesStorage = async (key: any) => {

    let value = await AsyncStorage.getItem(key) || undefined;

    console.log('get cookies store success', key, value);
    if (value) {

        let data = JSON.parse(value);
        console.log('get cookies store success, parse', data);

        // We have data!!
        return data;
    } else {

        return value;
    }
};

export const deleteCookiesStorage = async (key: any) => {
    try {
        // await Cookies.remove(key);

        console.log('cookies store delete success')

    } catch (error) {
        // Error retrieving data
        console.log('error in cookies -=-=-=>', error);
    }
};