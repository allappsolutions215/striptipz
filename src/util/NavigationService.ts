import * as React from 'react';

const navigationRef = React.createRef();

function navigate(name: string, params?: any) {
  navigationRef.current?.navigate(name, params);
}

function goBack(name: string, params?: any) {
  navigationRef.current?.goBack(name, params);
}

function stackFirst(name: string, params?: any) {
  navigationRef.current?.stackFirst(name, params);
}
function replace(name: string, params?: any) {
  navigationRef.current?.replace(name, params);
}

function reset(name: string, params?: any) {
  navigationRef.current?.reset({
    index: 0,
    routes: [{name: name, params: params}],
  });
}

export {navigationRef, navigate, goBack, stackFirst, replace,reset};
