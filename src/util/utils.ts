import R from '@app/res/R';

const getMediaType = (url: string) => {
  return url.includes('.mp4') ||
    url.includes('.mov') ||
    url.includes('.avi') ||
    url.includes('.m4v')
    ? 'video'
    : url.includes('.mpeg-4') ||
      url.includes('.aac') ||
      url.includes('.alac') ||
      url.includes('.m4a') ||
      url.includes('.mp3')
    ? 'audio'
    : 'image';
};

const getErrorMessage = (data: any) => {
  if (!data || data.length === 0 || data === null) {
    return R.strings.error.something_went_wrong;
  } else if (data.msg) {
    return data.msg;
  } else if (data.errors) {
    return data.errors;
  } else if (data.error) {
    return data.error;
  } else if (data.message) {
    return data.message;
  } else if (data.status) {
    return data.status;
  } else {
    return R.strings.error.something_went_wrong;
  }
};

export const utils = {
  getMediaType,
  getErrorMessage,
};
