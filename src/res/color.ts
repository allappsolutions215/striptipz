export const hexToRGB = (hex: string = '#000000', alpha: number) => {
  if (hex.includes('#')) {
    let r = parseInt(hex.slice(1, 3), 16);
    let g = parseInt(hex.slice(3, 5), 16);
    let b = parseInt(hex.slice(5, 7), 16);

    if (alpha) {
      return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
    } else {
      return 'rgb(' + r + ', ' + g + ', ' + b + ')';
    }
  } else {
    const hexSplit = hex.split(',');
    if (hexSplit.length === 4 || hexSplit.length === 3) {
      let color = '';

      color += hexSplit[0];
      color += ',';
      color += hexSplit[1];
      color += ',';
      color += hexSplit[2];
      color += ',';
      color += alpha;
      color += ')';

      return color;
    }
  }
};

const color: Color = {
  // theme primary color
  primaryHex: '#2C8BE9',
  primary: '#BB3706',
  primaryHeader: '#1E84EA',
  secondary: '#000000',
  tertiary: '#8E81A1',
  fontPrimary: '#FFFFFF',
  fontSecondary: '#C26B6A',
  fontTertiary: '#000000',
  gray1: '#999999',
  dimGray: '#FAFAFA',
  dimGray1: '#EEEEEE',

  fontColor1: '#7C7C7C',
  fontColor2: '#838383',
  fontColor3: '#7E7E7E',
  lightPink: '#E0B5B4',
  greenColor: '#3B9166',

  gray2: '#4B4B4B',
  gray3: '#A5A5A5',
  gray4: '#707070',
  gray5: '#F3F3F3',
  gray6: '#B9B9B9',
  gray7: '#979797',
  gray8: '#EFEFEF',
  gray9: '#E8E6EC',

  // generic colors
  white: '#FFFFFF',
  black: '#000000',

  //Useless
  fontColor4: '#6DA06F',
  fontColor5: '#0DA460',
  fontColor6: '#3A3A3A',
  fontColor7: '#32B278',
  // theme secondary color

  // additional theme colors
  blue: '#2196F3',
  redLight: '#FFF0E4',
  red: '#FF0000',
  whiteTrasparent: hexToRGB('#FFFFFF', 0.2),
  transparent: 'transparent',
  yellow: '#FFD900',
  black2: '#333333',
  black3: '#383838',
  black4: '#242424',

  // informative/action colors
  danger: '#E62E2D',
  success: '#28a745',
  warning: '#ffc107',
  info: '#17a2b8',

  colorStartRating: '#FFBB43',
  // gray colors for font and background

  gray10: '#212121',
  gray11: '#F7F7F7',
  gray12: '#010101',
  gray13: '#C5C5C5',
  gray14: '#EDEDED',
  gray15: '#DFDFDF',
  gray16: '#D5D5D5',
  gray17: '#BEBFC1',
  gray18: '#8F8F8F',
  gray19: '#F2F2F2',
  gray20: '#909CA1',
  gray21: '#686868',
  gray22: '#8E8E8E',
  gray23: '#EAEAEA',
  gray24: '#7B7B7B',
  gray25: '#DEDEDE',
  gray26: '#E8E8E8',
  gray27: '#BCC5D3',
  gray28: '#4C5264',

  cardGradient: ['#BB3706', '#5BA8ED'],
  chatGradient: ['#BB3706', '#BB3706', '#5BA8ED'],
  chatGradientWhite: ['#EEEEEE', '#EEEEEE'],

  blueSeen: '#4CC5F7',
};
export interface Color {
  // theme primary color
  primaryHex: string;
  primary: string;
  primaryHeader: string;
  secondary: string;
  tertiary: string;
  fontPrimary: string;
  fontSecondary: string;
  fontTertiary: string;
  lightPink: string;
  greenColor: string;
  black2: string;

  fontColor1: string;
  fontColor2: string;
  fontColor3: string;
  fontColor4: string;
  fontColor5: string;
  fontColor6: string;
  fontColor7: string;
  // theme secondary color

  // additional theme colors
  blue: string;
  redLight: string;
  red: string;
  whiteTrasparent: string;
  transparent: string;
  yellow: string;
  // informative/action colors
  danger: string;
  success: string;
  warning: string;
  info: string;

  // generic colors
  white: string;
  black: string;

  // gray colors for font and background
  gray1: string;
  gray2: string;
  gray3: string;
  gray4: string;
  gray5: string;
  gray6: string;
  dimGray: string;
  dimGray1: string;

  gray7: string;
  gray8: string;
  gray9: string;
  gray10: string;
  gray11: string;
  gray12: string;
  gray13: string;
  gray14: string;
  gray15: string;
  gray16: string;
  gray17: string;
  gray18: string;
  gray19: string;
  gray20: string;
  gray21: string;
  gray22: string;
  gray23: string;
  gray24: string;
  gray25: string;
  gray26: string;
  gray27: string;
  gray28: string;

  black3: string;
  black4: string;
  colorStartRating: string;
  cardGradient: string[];
  blueSeen: string;
  chatGradient: string[];
  chatGradientWhite: string[];
}

export default color;
