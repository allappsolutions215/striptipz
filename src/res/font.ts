const font: Font = {
  Light: 'SFProText-Light',
  Regular: 'SFProText-Regular',
  Medium: 'SFProText-Medium',
  Semibold: 'SFProText-Semibold',
  Bold: 'SFProText-Bold',
  Italic: 'SFProText-Italic',

  // Light: 'Barlow-Light',
  // Regular: 'Barlow-Regular',
  // Medium: 'Barlow-Medium',
  // Semibold: 'Barlow-Semibold',
  // Bold: 'Barlow-Bold',
  // Italic: 'Barlow-Italic',
};

export interface Font {
  Light: string;
  Regular: string;
  Medium: string;
  Semibold: string;
  Bold: string;
  Italic: string;
}

export default font;
