const image: any = {
  splash: () => require('./images/splash.png'),
  splash_black: () => require('./images/splash_black.png'),
  back: () => require('./images/back.png'),
  back_arrow: () => require('./images/left_arrow.png'),
  right_icon: () => require('./images/right_icon.png'),
  calendar: () => require('./images/calendar.png'),
  payPal: () => require('./images/paypal.png'),
  camera: () => require('./images/camera.png'),
  gallery: () => require('./images/gallery.png'),
  app_512: () => require('./images/app_512.png'),
  bell: () => require('./images/bell.png'),
  app_512_orange: () => require('./images/app_512_orange.png'),
  girl: () => require('./images/girl1.jpg'),
  girl2: () => require('./images/girl2.jpg'),
  black_bg: () => require('./images/black_bg.png'),
  plus: () => require('./images/plus.png'),
  user: () => require('./images/user.png'),
  circle_rings: () => require('./images/circle_rings.png'),
  dollar: () => require('./images/dollar.png'),
  pencil_edit: () => require('./images/pencil_edit.png'),
};

export default image;
