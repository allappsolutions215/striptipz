import React from 'react';

//import containers
import DancerDashboardContainer from '../../../container/DancerRole/Dashboard';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const DancerDashboardScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout statusBarColor={R.color.black}>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.black}}>
        <DancerDashboardContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default DancerDashboardScreen;
