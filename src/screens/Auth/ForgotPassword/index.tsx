import React from 'react';

//import containers
import ForgotPasswordContainer from '../../../container/Auth/ForgotPassword';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const ForgotPasswordScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <ForgotPasswordContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default ForgotPasswordScreen;
