import React from 'react';

//import containers
import AllSetContainer from '../../../container/Auth/AllSet';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const AllSetScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <AllSetContainer navigation={props.navigation} route={props.route} />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default AllSetScreen;
