import React from 'react';

//import containers
import AuthContainer from '@app/container/Auth';
import BaseLayout from '@app/components/layout/base';

const AuthScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <AuthContainer navigation={props.navigation} route={props.route} />
    </BaseLayout>
  );
};

export default AuthScreen;
