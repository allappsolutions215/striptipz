import React from 'react';
//import containers
import SignInContainer from '../../../container/Auth/SignIn';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const SignInScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <SignInContainer navigation={props.navigation} route={props.route} />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default SignInScreen;
