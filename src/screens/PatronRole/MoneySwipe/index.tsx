import React from 'react';

//import containers
import MoneySwipeContainer from '../../../container/PatronRole/MoneySwipe';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const MoneySwipeScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout statusBarColor={R.color.black}>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.black}}>
        <MoneySwipeContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default MoneySwipeScreen;
