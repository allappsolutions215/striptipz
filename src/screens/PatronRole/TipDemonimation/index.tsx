import React from 'react';

//import containers
import TipDenominationContainer from '../../../container/PatronRole/TipDenomination';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const TipDenominationScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout statusBarColor={R.color.primary}>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <TipDenominationContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default TipDenominationScreen;
