import React from 'react';

//import containers
import MyAccountContainer from '../../../container/Profile/MyAccount';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const MyAccountScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.black}}>
        <MyAccountContainer navigation={props.navigation} route={props.route} />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default MyAccountScreen;
