import React from 'react';

//import containers
import AddPhotosContainer from '../../../container/Profile/AddPhotos';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const AddPhotosScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <AddPhotosContainer navigation={props.navigation} route={props.route} />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default AddPhotosScreen;
