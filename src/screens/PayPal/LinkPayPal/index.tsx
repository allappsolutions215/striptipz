import React from 'react';

//import containers
import LinkPayPalContainer from '../../../container/PayPal/LinkPayPal';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const LinkPayPalScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <LinkPayPalContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default LinkPayPalScreen;
