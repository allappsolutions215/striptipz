import React from 'react';

//import containers
import TransactionHistoryContainer from '../../../container/PayPal/TransactionHistory';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const TransactionHistoryScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout statusBarColor={R.color.black}>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.black}}>
        <TransactionHistoryContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default TransactionHistoryScreen;
