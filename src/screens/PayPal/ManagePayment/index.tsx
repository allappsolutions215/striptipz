import React from 'react';

//import containers
import ManagePaymentContainer from '../../../container/PayPal/ManagePayment';
import BaseLayout from '../../../components/layout/base';
import {SafeAreaView} from 'react-native-safe-area-context';
import R from '@app/res/R';

const ManagePaymentScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout>
      <SafeAreaView style={{flex: 1, backgroundColor: R.color.primary}}>
        <ManagePaymentContainer
          navigation={props.navigation}
          route={props.route}
        />
      </SafeAreaView>
    </BaseLayout>
  );
};

export default ManagePaymentScreen;
