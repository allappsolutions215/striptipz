import React from 'react';

//import containers
import SplashContainer from '../../container/Splash';
import BaseLayout from '../../components/layout/base';
import R from '@app/res/R';

const SplashScreen = (props: any) => {
  return (
    // @ts-ignore
    <BaseLayout statusBarColor={R.color.black}>
      <SplashContainer navigation={props.navigation} route={props.route} />
    </BaseLayout>
  );
};

export default SplashScreen;
