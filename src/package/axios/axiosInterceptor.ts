import axios from './axios';
import {axiosNT, axiosNL, axiosNTL} from './axios';

import _get from 'lodash.get';

import toastService from '../../util/toast';

import * as NavigationService from '../../util/NavigationService';
import {Alert} from 'react-native';

const axiosInterceptor = (dispatch: any) => {
  //request interceptors
  async function commonRequest(request: any) {
    try {
    } catch (ex) {}
    return request;
  }

  function commonResponse(response: any) {
    //hide loader

    if (!response) {
      //show alert
      return;
    }

    // set message to toast service
    // if (response.data && response.data.code == -1) {
    //   toastService.show(response.data.message);
    // }

    //got response success
    if (response.status === 200) {
      // TODO: Add handling for bad request
    }
    if (response.status === 401) {
      //TODO CHANGE :: Handle 401 and navigate to Auth Screen:

      Alert.alert(
        'D-Date',
        'Session expired, Please login again to continue',
        [
          {
            text: 'Ok',
            onPress: () => NavigationService.navigate('AuthSelection'),
          },
        ],
        {cancelable: false},
      );
    }

    //got response success
    if (response.data.status !== 1) {
      // TODO: Add handling for invalid response
    }
  }

  axios.interceptors.request.use(async (request) => {
    //show loader
    request = await commonRequest(request);
    return request;
  });

  axiosNT.interceptors.request.use(async (request) => {
    //show loader

    request = await commonRequest(request);
    return request;
  });

  axiosNL.interceptors.request.use(async (request) => {
    request = await commonRequest(request);
    return request;
  });

  axiosNTL.interceptors.request.use(async (request) => {
    request = await commonRequest(request);
    return request;
  });

  //response interceptors
  axios.interceptors.response.use(
    (response: any) => {
      //hide loader

      if (!response.data) {
        return response;
      }
      //hide loader
      commonResponse(response);

      // Do something with response data
      return _get(response, 'data', {});
    },
    (error) => {
      console.log('Api Error', error);
      //hide loader
      commonResponse(error.response);

      // Do something with response error
      return Promise.reject(error);
    },
  );

  axiosNT.interceptors.response.use(
    (response) => {
      //hide loader
      if (!response.data) {
        return response;
      }

      commonResponse(response);

      // Do something with response data
      return response.data;
    },
    (error) => {
      //hide loader
      commonResponse(error.response);

      // Do something with response error
      return Promise.reject(error);
    },
  );

  axiosNL.interceptors.response.use(
    (response: any) => {
      if (!response.data) {
        return response;
      }

      commonResponse(response);
      // Do something with response data
      return response.data;
    },
    (error) => {
      commonResponse(error.response);

      // Do something with response error
      return Promise.reject(error);
    },
  );

  axiosNTL.interceptors.response.use(
    (response) => {
      if (!response.data) {
        return response;
      }

      commonResponse(response);

      // Do something with response data
      return response.data;
    },
    (error) => {
      commonResponse(error.response);

      // Do something with response error
      return Promise.reject(error);
    },
  );
};

export default axiosInterceptor;
