import * as coreAxios from 'axios';
import config from '../../config';

const axios = coreAxios.default.create({
  baseURL: config.baseURL,
});

//no toast
export const axiosNT = coreAxios.default.create({
  baseURL: config.baseURL,
});

//no global loader
export const axiosNL = coreAxios.default.create({
  baseURL: config.baseURL,
});

//no global loader and toast
export const axiosNTL = coreAxios.default.create({
  baseURL: config.baseURL,
});

//FormData
export const axiosFD = coreAxios.default.create({
  baseURL: config.baseURL,
  headers: {
    //Accept: '*/*',
    'Content-Type': 'application/x-www-form-urlencoded'
    // 'Content-Type': 'application/json'
  },
});

export default axios;
