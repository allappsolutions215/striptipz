import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import * as React from 'react';

// ******************** AUTHUNTICATION ********************
import SplashScreen from '../screens/Splash';
import AuthSelectionScreen from '../screens/Auth/AuthSelection';
import SignInScreen from '../screens/Auth/SignIn';
import SignUpScreen from '../screens/Auth/SignUp';
import OtpScreen from '../screens/Auth/Otp';
import ChangePasswordScreen from '../screens/Auth/ChangePassword';
import ForgotPasswordScreen from '../screens/Auth/ForgotPassword';
import ResetPasswordScreen from '../screens/Auth/ResetPassword';
import CompleteProfileScreen from '../screens/Auth/CompleteProfile';
import AllSetScreen from '../screens/Auth/AllSet';

// ******************** DANCER ********************
import DancerDashboardScreen from '../screens/DancerRole/Dashboard';
import DancerProfileScreen from '../screens/DancerRole/DancerProfile';

// ******************** PATRON ********************
import PatronDashboardScreen from '../screens/PatronRole/Dashboard';
import MoneySwipeScreen from '../screens/PatronRole/MoneySwipe';
import TipDemonimationScreen from '../screens/PatronRole/TipDemonimation';

// ******************** PROFILE ********************
import AddPhotosScreen from '../screens/Profile/AddPhotos';
import MyAccountScreen from '../screens/Profile/MyAccount';
import MyProfileScreen from '../screens/Profile/MyProfile';
import EditProfileScreen from '../screens/Profile/EditProfile';
import AllNotificationScreen from '../screens/Notification/AllNotification';

// ******************** PAYMENTS ********************
import SubscriptionScreen from '../screens/PayPal/Subscription';
import LinkPayPalScreen from '../screens/PayPal/LinkPayPal';
import ManagePaymentScreen from '../screens/PayPal/ManagePayment';
import TransferMoneyScreen from '../screens/PayPal/TransferMoney';
import TransactionHistoryScreen from '../screens/PayPal/TransactionHistory';
import AddAmountScreen from '../screens/PayPal/AddAmount';
import WebViewPageScreen from '../screens/PayPal/WebViewPage';

import {navigationRef} from '../util/NavigationService';

// If you are not familiar with React Navigation, we recommend going through the
// "Fundamentals" guide: https://reactnavigation.org/docs/getting-started
export default function Navigation() {
  return (
    <NavigationContainer ref={navigationRef}>
      <RootNavigator />
    </NavigationContainer>
  );
}

// A root stack navigator is often used for displaying modals on top of all other content
// Read more here: https://reactnavigation.org/docs/modal
const Stack = createStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator 
      screenOptions={({route, navigation}) => ({
        headerShown: false,
        gestureEnabled: true,
        cardOverlayEnabled: true,
        headerStatusBarHeight:
          navigation
            .dangerouslyGetState()
            .routes.findIndex((r) => r.key === route.key) > 0
            ? 0
            : undefined,
        ...TransitionPresets.SlideFromRightIOS,
      })}>
      <Stack.Screen name="Root" component={SplashScreen} />
      <Stack.Screen name="AuthSelection" component={AuthSelectionScreen} />
      <Stack.Screen name="SignIn" component={SignInScreen} />
      <Stack.Screen name="SignUp" component={SignUpScreen} />
      <Stack.Screen name="Otp" component={OtpScreen} />
      <Stack.Screen name="ChangePassword" component={ChangePasswordScreen} />
      <Stack.Screen name="ForgotPassword" component={ForgotPasswordScreen} />
      <Stack.Screen name="ResetPassword" component={ResetPasswordScreen} />

      <Stack.Screen name="CompleteProfile" component={CompleteProfileScreen} />
      <Stack.Screen name="LinkPayPal" component={LinkPayPalScreen} />
      <Stack.Screen name="ManagePayment" component={ManagePaymentScreen} />
      <Stack.Screen name="AddPhotos" component={AddPhotosScreen} />
      <Stack.Screen name="AllSet" component={AllSetScreen} />
      <Stack.Screen name="Subscription" component={SubscriptionScreen} />
      <Stack.Screen name="DancerDashboard" component={DancerDashboardScreen} />
      <Stack.Screen name="DancerProfile" component={DancerProfileScreen} />
      <Stack.Screen name="TransferMoney" component={TransferMoneyScreen} />
      <Stack.Screen
        name="TransactionHistory"
        component={TransactionHistoryScreen}
      />
      <Stack.Screen
        name="AddAmount"
        component={AddAmountScreen}
      />
      <Stack.Screen
        name="WebViewPage"
        component={WebViewPageScreen}
      />
      <Stack.Screen name="PatronDashboard" component={PatronDashboardScreen} />
      <Stack.Screen name="MoneySwipe" component={MoneySwipeScreen} />
      <Stack.Screen name="TipDemonimation" component={TipDemonimationScreen} />
      <Stack.Screen name="MyAccount" component={MyAccountScreen} />
      <Stack.Screen name="MyProfile" component={MyProfileScreen} />
      <Stack.Screen name="EditProfile" component={EditProfileScreen} />
      <Stack.Screen name="AllNotification" component={AllNotificationScreen} />
    </Stack.Navigator>
  );
}
