// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, Dimensions } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import PayPalStore from '../PayPalStore';
import { WebView } from 'react-native-webview';


import axios from '../../package/axios';
import config from '@app/config';
import * as coreAxios from 'axios';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export interface IProps {
  navigation: any;
  route: any;

  userResponse: any;

  getPPAccessTokenResponse: any;
  getPPAccessTokenError: any;

  createProductResponse: any;
  createProductError: any;

  createSubscriptionPlanResponse: any;
  createSubscriptionPlanError: any;

  createSubscriptionResponse: any;
  createSubscriptionError: any;

  createSubscriptionServerResponse: any;
  createSubscriptionServerError: any;

  userAction: any;
  getPPAccessTokenAction: any;
  connentDisconnectPayPalAction: any;
  createProductAction: any;
  createSubscriptionPlanAction: any;
  createSubscriptionAction: any;
  createSubscriptionToServerAction: any;
}

let action_type = undefined; //  'get_access_token',  'link_with_paypal', 'create_product', 'create_plan', 'create_subscription';

const Subscription: React.FC<IProps> = (props) => {
  // props.route.params.calledFrom = 'dashboard'
  const [loading, setLoading] = useState(false);
  const [viewType, setViewType] = useState('subscription');
  const [confirmUrl, setConfirmUrl] = useState(null);

  //---------- life cycle section

  // response for access token
  useEffect(() => {

    if (action_type === 'get_access_token') {

      if (props.getPPAccessTokenResponse) {

        checkUserPayPalAccountLinking(props.getPPAccessTokenResponse);
        // createPocduct()
      } else if (props.getPPAccessTokenError) {

      }
    }
  }, [props.getPPAccessTokenError, props.getPPAccessTokenResponse]);

  // response for create product
  useEffect(() => {

    if (action_type === 'create_product') {
      if (props.createProductResponse) {

        createPlanForSubscription(props.createProductResponse.data);
      } else if (props.createProductError) {

      }
    }

  }, [props.createProductResponse, props.createProductError]);

  // create plan for subscription  
  useEffect(() => {

    if (action_type === 'create_plan') {

      if (props.createSubscriptionPlanResponse) {

        createSubscrition(props.createSubscriptionPlanResponse.data.data);

      } else if (props.createSubscriptionPlanError) {

      }
    }
  }, [props.createSubscriptionPlanResponse, props.createSubscriptionPlanError]);

  // create subscription  
  useEffect(() => {

    if (action_type === 'create_subscription') {

      if (props.createSubscriptionResponse) {

        setConfirmUrl(props.createSubscriptionResponse.approvalUrl);
        setViewType('web_view');
      } else if (props.createSubscriptionError) {

      }
    }
  }, [props.createSubscriptionResponse, props.createSubscriptionError]);

  useEffect(() => {

    if (action_type === 'save_subscription_data') {

      if (props.createSubscriptionServerResponse) {

        setLoading(false);
        gotoNext();
      } else if (props.createSubscriptionServerError) {

      }
    }
  }, [props.createSubscriptionServerResponse, props.createSubscriptionServerError]);

  //---------- user's action

  const getAccessToken = () => {

    setLoading(true);
    action_type = 'get_access_token';
    props.getPPAccessTokenAction();
  };

  const checkUserPayPalAccountLinking = (response: any) => {

    if ((props.userResponse && props.userResponse.is_paypal_connect) && props.userResponse.is_paypal_connect === 1) {

      createPocduct();
    } else {

      linkWithPaypal(response.data.scope);
    }
  };

  const linkWithPaypal = (scopeList: any) => {

    action_type = 'link_with_paypal';

    let return_url = 'https://example.com/return';
    let accessToken = getToken();

    coreAxios.default.get(
      `https://www.sandbox.paypal.com/connect?flowEntry=static&client_id=${config.clientID}&scope=${scopeList}&redirect_uri=${return_url}`,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`,
        },
      })
      .then((response: any) => {

        // setConfirmUrl(response.data.requestUrl);
        let r = `https://www.sandbox.paypal.com${response.data.requestUrl}`;

        setConfirmUrl(r);
        setViewType('web_view');
      })
      .catch((err: any) => {

        if (err.message) {
          alert(err.message);
        }
      });
  };

  const createPocduct = async () => {

    action_type = 'create_product';

    let token = getToken();

    props.createProductAction({
      data: {
        "name": "Striptiz",
        "description": R.strings.payPal.subscription_note,
        "type": "SERVICE",
        "category": "SOFTWARE",
        // "image_url": "https://example.com/streaming.jpg",
        "home_url": "https://example.com/home"
      },
      access_token: token,
    });
  };

  const createPlanForSubscription = (product: any) => {

    action_type = 'create_plan';
    let token = getToken();
    let data = {
      "product_id": product.data.id,
      "name": product.data.name,
      "description": product.data.description,
      "billing_cycles": [
        {
          "frequency": {
            "interval_unit": "MONTH",
            "interval_count": 1
          },
          "tenure_type": "TRIAL",
          "sequence": 1,
          "total_cycles": 1
        },
        {
          "frequency": {
            "interval_unit": "MONTH",
            "interval_count": 1
          },
          "tenure_type": "REGULAR",
          "sequence": 2,
          "total_cycles": 12,
          "pricing_scheme": {
            "fixed_price": {
              "value": "19.99",
              "currency_code": "USD"
            }
          }
        }
      ],
      "payment_preferences": {
        "auto_bill_outstanding": true,
        "setup_fee": {
          "value": 19.99,
          "currency_code": "USD"
        },
        "setup_fee_failure_action": "CONTINUE",
        "payment_failure_threshold": 3
      },
      "taxes": {
        "percentage": "10",
        "inclusive": false
      }
    };
    props.createSubscriptionPlanAction({
      data,
      access_token: token,
    });
  };

  const createSubscrition = (plan: any) => {

    action_type = 'create_subscription';
    let user_detail = props.userResponse;
    let access_token = getToken();
    const myDate = new Date();
    const newDate = new Date(myDate);
    newDate.setHours(newDate.getHours() + 1);

    let data = {
      "plan_id": plan.id,
      "start_time": newDate,
      "subscriber": {
        "name": {
          "given_name": user_detail.name,
          "surname": '',
        },
        // "email_address": 'sb-nxbok1279248@business.example.com',
        "email_address": user_detail.email,
      },
      "application_context": {
        "brand_name": "Striptiz",
        "locale": "en-US",
        "shipping_preference": "SET_PROVIDED_ADDRESS",
        "user_action": "SUBSCRIBE_NOW",
        "payment_method": {
          "payer_selected": "PAYPAL",
          "payee_preferred": "IMMEDIATE_PAYMENT_REQUIRED",
        },
        "return_url": "https://example.com/returnUrl",
        "cancel_url": "https://example.com/cancelUrl",
      },
    };

    props.createSubscriptionAction({
      data,
      access_token: access_token,
    });
  };

  const getToken = () => {

    let token = props.getPPAccessTokenResponse && props.getPPAccessTokenResponse.data && props.getPPAccessTokenResponse.data.access_token;

    return token;
  };

  const gotoNext = () => {

    const userData = props.userResponse;
    if (userData && userData.user_type) {

      action_type = undefined;

      console.log('props.userResponse', props.userResponse)
      props.userAction({
        ...props.userResponse,
        is_subscription_valid: 1,
      });
      props.navigation.navigate('AllSet', { userData });
    } else {

      alert(R.strings.error.something_went_wrong);
    }
  };

  const getSubscriptionDetail = () => {

    const access_token = getToken()
    coreAxios.default.post(
      `https://api-m.sandbox.paypal.com/v1/billing/subscriptions/I-NGNWG4KBRTP1/cancel`,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${access_token}`,
        },
      })
      .then((response: any) => {

      })
      .catch((err: any) => {
      });
  }

  const _onNavigationStateChange = (webViewState: any) => {

    if (webViewState.url.includes('https://example.com/')) {

      if (action_type === 'link_with_paypal') {

        props.connentDisconnectPayPalAction(1);
        createPocduct();
      }
      if (action_type === 'create_subscription') {

        let subscription_id = getParameterByName('subscription_id', webViewState.url);
        let ba_token = getParameterByName('ba_token', webViewState.url);
        let token = getParameterByName('token', webViewState.url);
        let product_id = props.createProductResponse.data.data.id;

        let data = {
          "product_id": product_id,
          "subscription_id": subscription_id,
          "amount": "19.99",
          "cancel_subscription_link": `https://api-m.sandbox.paypal.com/v1/billing/subscriptions/${subscription_id}/cancel`,
          "ba_token": ba_token,
          "token": token,
          "data": "testing....!!",
        };

        action_type = 'save_subscription_data';
        setViewType('subscription');
        props.createSubscriptionToServerAction({ data });
        // setLoading(false);
        // gotoNext();
      }
    }
  };

  const getParameterByName = (name: string, url: string) => {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.payPal.subcription}
        navigation={props.navigation}
      />

      {
        viewType === 'subscription' ?
          <View style={styles.root}>
            <View style={styles.amountContainer}>
              <Text variant={'title2'} style={styles.amountText}>
                {R.strings.payPal.dollar}
              </Text>
              <Text variant={'title2'} style={styles.amountDurationText}>
                {R.strings.payPal.per_month}
              </Text>
            </View>

            <Text variant={'title2'} style={styles.titleText}>
              {R.strings.payPal.subscription_note}
            </Text>
            <Button
              btnWrapperStyles={styles.button}
              onPress={() => getAccessToken()}
              loading={loading}
              // onPress={() => getSubscriptionDetail()}
              gutterTop={R.unit.scale(30)}
              text={R.strings.common.done}
            />
          </View>
          : null
      }
      {
        viewType === 'web_view' ?
          <View style={styles.web_view_container}>
            <WebView
              // style={styles.web_view_container}
              source={{ uri: confirmUrl }}
              onNavigationStateChange={_onNavigationStateChange}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              startInLoadingState={false}
            />
          </View>
          :
          null
      }


    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: '40%',
    //justifyContent: 'center',
  },
  web_view_container: {
    width: '100%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: '10%',
  },
  titleText: { marginTop: R.unit.scale(30), textAlign: 'center' },
  amountText: {
    textAlign: 'center',
    fontSize: R.unit.scale(60),
    fontWeight: 'bold',
  },
  amountDurationText: { textAlign: 'center', fontSize: R.unit.scale(20) },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
  amountContainer: {
    height: '30%',
    backgroundColor: R.color.whiteTrasparent,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: R.unit.scale(15),
  },
});

export default PayPalStore(Subscription);
