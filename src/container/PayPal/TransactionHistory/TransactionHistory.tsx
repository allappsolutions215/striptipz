// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, FlatList } from 'react-native';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import PayPalStore from '../PayPalStore';
import moment from 'moment';

export interface IProps {
  navigation: any;

  patronTransHistoryAction: any;
  patronTransHistoryResponse: any;
  patronTransHistoryError: any;
}

const TransactionHistory: React.FC<IProps> = (props) => {
  const [transactionList, setTransactionList] = useState([]);

  useEffect(() => {
    props.patronTransHistoryAction();
  }, []);

  useEffect(() => {
    if (props.patronTransHistoryResponse) {
      
      if (props.patronTransHistoryResponse.status) {
        console.log('props.patronTransHistoryResponse.data', props.patronTransHistoryResponse)
        setTransactionList(props.patronTransHistoryResponse.data && props.patronTransHistoryResponse.data[0])
      }
      props.patronTransHistoryResponse.data[0]
    } else if (props.patronTransHistoryError) {
      console.log('props.patronTransHistoryError', props.patronTransHistoryError)
      alert(props.patronTransHistoryError && props.patronTransHistoryError.message)
    }
  }, [props.patronTransHistoryResponse, props.patronTransHistoryError]);


  return (
    <View style={styles.container}>
      <AuthHeader
        color={R.color.black}
        tintColor={R.color.black}
        text={R.strings.profile.transaction_history}
        navigation={props.navigation}
      />

      <View style={styles.root}>
        <View
          style={{
            backgroundColor: R.color.white,
          }}>
          <FlatList
            data={transactionList}
            contentContainerStyle={{
              width: '100%',
              alignSelf: 'center',
              paddingBottom: R.unit.scale(30),
              paddingHorizontal: R.unit.scale(10),
            }}
            renderItem={({ item, index }) => {
              return (
                <View style={styles.itemContainer}>
                  <Text variant={'title3'} color={R.color.black}>
                    {item.date}
                  </Text>
                  <View style={styles.itemSubContainer}>
                    <Text variant={'title3'} color={R.color.black}>
                      {R.strings.payPal.status}
                      {' ' + item.status}
                    </Text>
                    <Text variant={'title3'} color={R.color.primary}>
                      ${item.amount}
                    </Text>
                  </View>
                </View>
              )
            }}
            keyExtractor={(index) => index.toString()}
          //ItemSeparatorComponent={() => <View style={styles.separator} />}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.white },
  root: {
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    marginTop: R.unit.scale(10),
    //justifyContent: 'center',
  },
  titleText: { marginTop: R.unit.scale(30), textAlign: 'center' },
  amountText: {
    textAlign: 'center',
    fontSize: R.unit.scale(60),
    fontWeight: 'bold',
  },
  amountDurationText: { textAlign: 'center', fontSize: R.unit.scale(20) },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
  amountContainer: {
    height: '30%',
    backgroundColor: R.color.whiteTrasparent,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: R.unit.scale(15),
  },
  itemContainer: {
    padding: R.unit.scale(10),
    backgroundColor: R.color.dimGray1,
    marginVertical: R.unit.scale(5),
    borderRadius: R.unit.scale(10),
  },
  separator: {
    height: 0.5,
    backgroundColor: R.color.gray1,
  },
  itemSubContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: R.unit.scale(10),
  },
});

export default PayPalStore(TransactionHistory);
