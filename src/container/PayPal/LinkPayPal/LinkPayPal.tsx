// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import PayPalStore from '../PayPalStore';
import * as coreAxios from 'axios';
import config from '@app/config';
import { WebView } from 'react-native-webview';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// props
export interface IProps {
  navigation: any;
  route: any;

  userResponse: any;

  getPPAccessTokenAction: any;
  getPPAccessTokenError: any;
  getPPAccessTokenResponse: any;

  connentDisconnectPayPalAction: any;
  connetDisconnentPayPalResponse: any;
  connetDisconnentPayPalError: any;
}

const LinkPayPal: React.FC<IProps> = (props) => {
  // constants and veriable
  const [accessToken, setAccessToken] = useState(null);
  const [scopeList, setScopeList] = useState(null);
  const [confirmUrl, setConfirmUrl] = useState(null);
  const [loading, setLoading] = useState(false);

  // life cycle's

  // get access token from pokcet and set it localy
  useEffect(() => {

    if (props.getPPAccessTokenResponse && loading) {

      let scoplist = props.getPPAccessTokenResponse && props.getPPAccessTokenResponse.data && props.getPPAccessTokenResponse.data.scope;
      let access_token = props.getPPAccessTokenResponse && props.getPPAccessTokenResponse.data && props.getPPAccessTokenResponse.data.scope;
      connectWithPaypal(scoplist, access_token);

    } else if (props.getPPAccessTokenError && loading) {
    }
  }, [props.getPPAccessTokenError, props.getPPAccessTokenResponse]);

  // connet pay pal response
  useEffect(() => {

    if (props.connetDisconnentPayPalResponse && loading) {

      props.userAction({
        ...props.userResponse,
        is_paypal_connect: 1,
      });
      gotoNext();
    } else if (props.connetDisconnentPayPalError && loading) {
    }
  }, [props.connetDisconnentPayPalResponse, props.connetDisconnentPayPalError]);

  // user's action

  const _onNavigationStateChange = (webViewState: any) => {

    if (webViewState.url.includes('https://example.com/')) {

      setLoading(true);
      setConfirmUrl(null);
      props.connentDisconnectPayPalAction(1);
    }
  };

  const linkWithPaypalAction = () => {

    setLoading(true);
    props.getPPAccessTokenAction();
  };

  const connectWithPaypal = (accessTokenTemp: any, scopeListTemp: any) => {

    setLoading(true);

    // let accessTokenTemp = accessToken;
    // let scopeListTemp = scopeList;
    let k = config.clientID;
    let return_url = 'https://example.com/return';

    coreAxios.default.get(
      `https://www.sandbox.paypal.com/connect?flowEntry=static&client_id=${k}&scope=${scopeListTemp}&redirect_uri=${return_url}`,
      {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessTokenTemp}`,
        },
      })
      .then((response: any) => {

        setConfirmUrl(response.data.requestUrl);
        // let r = `https://www.sandbox.paypal.com${response.data.requestUrl}`
      })
      .catch((err: any) => {

        if (err.message) {
          alert(err.message);
        }
      });
  };

  const gotoNext = () => {

    const userData = props.userResponse;

    if (userData && userData.user_type) {

      if (userData.user_type === 'dancer') {

        props.navigation.navigate('Subscription', { userData });
      } else {

        props.navigation.navigate('AllSet', { userData });
      }
    } else {
      alert(R.strings.error.something_went_wrong);
    }
  };

  //---------- views

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.payPal.link_your_pay_pal}
        navigation={props.navigation}
      />
      {
        !confirmUrl ?
          <View style={styles.root}>
            <Text variant={'title2'} style={styles.titleText}>
              {R.strings.payPal.link_pay_pal_future}
            </Text>
            <Button
              loading={loading}
              btnWrapperStyles={styles.button}
              onPress={() => {
                linkWithPaypalAction();
                // gotoNext()
              }}
              gutterTop={R.unit.scale(30)}
              text={R.strings.payPal.link_pay_pal}
              leftSectionStyle={{ position: 'relative', marginEnd: 20 }}
              leftComponent={
                <Image
                  style={{ width: R.unit.scale(25), height: R.unit.scale(25) }}
                  source={R.image.payPal()}
                />
              }
            />
          </View>
          :
          <View style={styles.root}>
            <WebView
              style={{ height: windowHeight, width: windowWidth }}
              source={{ uri: `https://www.sandbox.paypal.com${confirmUrl}` }}
              onNavigationStateChange={_onNavigationStateChange}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              startInLoadingState={false}
            />
          </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '100%',
    flex: 1,
    alignSelf: 'center',
  },
  titleText: { marginTop: R.unit.scale(50) },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
});

export default PayPalStore(LinkPayPal);
