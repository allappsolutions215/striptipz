// main imports
import React, {RefObject, useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, Image} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import BorderInput from '@app/components/common/BorderInput';

export interface IProps {
  navigation: any;
}

const TransferMoney: React.FC<IProps> = (props) => {
  const [amount, setAmount] = useState();

  const _renderTitle = (text: string) => {
    return (
      <Text variant={'title2'} style={styles.titleText}>
        {text}
      </Text>
    );
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.payPal.transfer_money}
        navigation={props.navigation}
      />

      <View style={styles.root}>
        <Text variant={'title1'}>{R.strings.payPal.current_tally_balance}</Text>

        <Text variant={'title'} style={styles.amountText}>
          {'$1001'}
        </Text>
        {_renderTitle(R.strings.payPal.enter_amount_to_transfer)}
        <BorderInput
          keyboardType={'number-pad'}
          value={amount}
          onChangeText={(text) => setAmount(text)}
          inputContainer={{marginTop: 0}}
          containerStyle={styles.borderInputStyle}
        />

        <Button
          btnWrapperStyles={styles.button}
          onPress={() => props.navigation.navigate('AllSet')}
          gutterTop={R.unit.scale(30)}
          text={R.strings.payPal.transfer}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: '20%',
  },
  titleText: {textAlign: 'auto'},
  amountText: {
    fontSize: R.unit.scale(30),
    fontWeight: 'bold',
    marginTop: R.unit.scale(10),
    marginBottom: R.unit.scale(25),
  },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },

  borderInputStyle: {
    marginTop: R.unit.scale(10),
    marginBottom: R.unit.scale(15),
    width: '95%',
  },
});

export default TransferMoney;
