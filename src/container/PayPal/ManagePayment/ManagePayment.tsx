// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// store
import PayPalStore from '../PayPalStore';
// components
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';

// props
export interface IProps {
  navigation: any;

  userAction: any;
  userResponse: any;

  connentDisconnectPayPalAction: any;
  connetDisconnentPayPalResponse: any;
  connetDisconnentPayPalError: any;
}

const ManagePayment: React.FC<IProps> = (props) => {
  // veriable and constants
  const [userType, setUserType] = useState(props.userResponse && props.userResponse.user_type.toLowerCase())
  const [isPayPalConnected, setIsPayPalConnected] = useState(props.userResponse && props.userResponse.is_paypal_connect);
  const [loading, setLoading] = useState(false);

  //---------- life cycle section

  useEffect(() => {

    if (props.connetDisconnentPayPalResponse && loading) {

      setIsPayPalConnected(false);
      setLoading(false);
      props.userAction({
        ...props.userResponse,
        is_paypal_connect: false,
      });
    } else if (props.connetDisconnentPayPalError && loading) {
      setLoading(false);
    }
  }, [props.connetDisconnentPayPalResponse, props.connetDisconnentPayPalError]);

  // helper's
  const handleUsersAction = (key: any) => {

    setLoading(true);

    if (key === 'add_acc') {

      props.navigation.navigate('LinkPayPal');
    }
    if (key === 'unlink_acc') {

      props.connentDisconnectPayPalAction(0);
    }
    if (key === 'change_acc') {

    }
  }

  //---------- views

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.manage_payment}
        navigation={props.navigation}
      />

      <View style={styles.root}>
        <View style={styles.paypalContainer}>
          <Image
            style={{
              width: R.unit.scale(25),
              height: R.unit.scale(25),
              tintColor: R.color.blue,
              marginStart: R.unit.scale(15),
            }}
            source={R.image.payPal()}
          />
          <Text color={R.color.black} style={styles.txtAccount}>
            {R.strings.payPal.paypal_account}
          </Text>

          {
            userType === 'dancer' ?
              <TouchableOpacity
                onPress={() => handleUsersAction('change_acc')}
              >
                <Text style={styles.txtChangeAccount} color={R.color.primary}>
                  {
                    R.strings.payPal.change_account
                  }
                </Text>
              </TouchableOpacity>
              :
              isPayPalConnected === 1 ?
                <TouchableOpacity
                  onPress={() => handleUsersAction('unlink_acc')}
                >
                  <Text style={styles.txtChangeAccount} color={R.color.primary}>
                    {
                      R.strings.payPal.unlink_acc
                    }
                  </Text>
                </TouchableOpacity>
                :
                <TouchableOpacity
                  onPress={() => handleUsersAction('add_acc')}
                >
                  <Text style={styles.txtChangeAccount} color={R.color.primary}>
                    {
                      R.strings.payPal.add_acc
                    }
                  </Text>
                </TouchableOpacity>
          }

        </View>

        {/* for transfer amount */}
        {
          userType === 'dancer' &&
          <View style={styles.amountContainer}>
            <Text
              color={R.color.black}
              variant={'title2'}
              style={styles.amountText}>
              {
                R.strings.payPal.current_tally_balance
              }
            </Text>
            <Text
              color={R.color.primary}
              variant={'title1'}
              style={styles.amountDurationText}>
              {
                '$1001'
              }
            </Text>
            <Button
              btnWrapperStyles={styles.button}
              onPress={() => props.navigation.navigate('TransferMoney')}
              text={R.strings.payPal.transfer_money}
            />
          </View>
        }

      </View>
    </View>
  );
};

// css
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: '40%',
    //justifyContent: 'center',
  },
  titleText: { marginTop: R.unit.scale(30), textAlign: 'center' },
  amountText: {
    textAlign: 'center',
    fontSize: R.unit.scale(18),
  },
  amountDurationText: {
    textAlign: 'center',
    fontSize: R.unit.scale(25),
    fontWeight: 'bold',
  },
  button: {
    alignSelf: 'center',
  },
  amountContainer: {
    height: '35%',
    backgroundColor: R.color.white,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: R.unit.scale(10),
    marginTop: R.unit.scale(40),
  },
  paypalContainer: {
    paddingVertical: R.unit.scale(15),
    backgroundColor: R.color.white,
    //justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRadius: R.unit.scale(10),
    flexDirection: 'row',
  },
  txtAccount: {
    fontSize: R.unit.scale(16),
    marginStart: R.unit.scale(10),
  },
  txtChangeAccount: {
    textAlign: 'right',
    marginStart: R.unit.scale(25),
  },
});

// connect with store
export default PayPalStore(ManagePayment);
