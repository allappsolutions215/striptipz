// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, ScrollView } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import TipBox from '@app/components/common/TipBox';
import BorderInput from '@app/components/common/BorderInput';
import PayPalStore from '../PayPalStore';

let FIRST = true;

export interface IProps {
  navigation: any;
  getPPAccessTokenAction: any;
  getPPAccessTokenError: any;
  getPPAccessTokenResponse: any;

  getPaymentDataAction: any;
  getPaymentDataResponse: any;
  getPaymentDataError: any;
}

const AddAmount: React.FC<IProps> = (props) => {
  // veriable and constant
  const [selectedAmount, setSelectedTipAmount] = useState(
    R.strings.home.ten_dollar,
  );
  const [amount, setAmount] = useState('10');
  const [loading, setLoading] = useState(false);

  const priceArray = [
    R.strings.home.ten_dollar,
    R.strings.home.fifty_dollar,
    R.strings.home.hundred_dollar,
    R.strings.home.two_hundred_dollar,
  ];

  // life cycle's
  useEffect(() => {

    if (props.getPPAccessTokenResponse) {
      
      const data = {
        intent: 'sale',
        payer: {
          payment_method: 'paypal',
        },
        transactions: [
          {
            amount: {
              total: `${amount}`,
              currency: 'USD', 
              details: {
                subtotal: `${amount}`,
                tax: '0',
                shipping: '0',
                handling_fee: '0',
                shipping_discount: '0',
                insurance: '0',
              },
            },
          },
        ],
        redirect_urls: {
          return_url: 'https://example.com/return',
          cancel_url: 'https://example.com/cancel',
        },
      };
      
      props.getPaymentDataAction({
        access_token: props.getPPAccessTokenResponse.data.access_token,
        data,
      });
    } else if (props.getPPAccessTokenError) {
    }
  }, [props.getPPAccessTokenError, props.getPPAccessTokenResponse]);

  useEffect(() => {

    if (props.getPaymentDataResponse && FIRST) {

      if (props.getPaymentDataResponse.approvalUrl) {
        // TODO START FROM HERE::::
        FIRST = false;
        props.navigation.navigate('WebViewPage', {
          approvalUrl: props.getPaymentDataResponse.approvalUrl,
          access_token: props.getPPAccessTokenResponse.data.access_token,
        });
      }
    } else if (props.getPaymentDataError) {

    }
  }, [props.getPaymentDataError, props.getPaymentDataResponse]);


  // user's action
  
  const initPayPal = (_amount: any) => {
    // PAYPAL Access Token:
    setLoading(true);
    props.getPPAccessTokenAction();
  };

  const getAmount = () => {
    if (amount && amount.toString().trim().length > 0 && Number(amount) >= 1) {
      return Number(amount.toString().trim());
    } else {
      return 0;
    }
  };

  const validateData = () => {

    if (getAmount() === 0) {

      alert(R.strings.error.something_went_wrong);
    } else {
      FIRST = true;
      initPayPal(amount);
    }
  };

  // onchange function 
  const onAmountChange = (item: any) => {
    setSelectedTipAmount(item);
    setAmount(item.replace('$', ''));
  };

  //---------- views

  const _renderTitle = (text: string) => {
    return (
      <Text variant={'title2'} style={styles.titleText}>
        {text}
      </Text>
    );
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.payPal.add_amount_to_wallet}
        navigation={props.navigation}
      />
      <ScrollView
        contentContainerStyle={styles.container}
        style={styles.container}>
        <View style={styles.root}>
          {_renderTitle(R.strings.payPal.enter_amount)}
          <BorderInput
            keyboardType={'number-pad'}
            value={amount}
            onChangeText={(text) => {
              setAmount(text), setSelectedTipAmount('');
            }}
            inputContainer={{ marginTop: 0 }}
            containerStyle={styles.borderInputStyle}
          />

          <Text variant={'title2'} style={styles.orText}>
            {R.strings.common.or}
          </Text>

          {_renderTitle(R.strings.payPal.enter_amount)}
          <View style={styles.rowContainer}>
            {priceArray.map((item, index) => {
              return (
                <TipBox
                  text={item}
                  onPress={() => onAmountChange(item)}
                  isSelected={item == selectedAmount}
                />
              );
            })}
          </View>
          <Button
            btnWrapperStyles={styles.button}
            onPress={() => validateData()}
            text={R.strings.common.add}
            loading={loading}
          />
        </View>
      </ScrollView>
    </View>
  );
};

// css
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: R.unit.scale(40),
  },
  titleText: {
    marginTop: R.unit.scale(15),
  },
  tipTextAmount: {
    fontWeight: 'bold',
    marginStart: R.unit.scale(10),
  },
  rowContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    marginTop: R.unit.scale(15),
  },
  borderInputStyle: {
    marginTop: R.unit.scale(10),
    marginBottom: R.unit.scale(15),
    width: '100%',
  },
  orText: {
    borderRadius: R.unit.scale(20),
    width: R.unit.scale(40),
    height: R.unit.scale(40),
    borderWidth: R.unit.scale(1),
    borderColor: R.color.white,
    alignSelf: 'center',
    textAlign: 'center',
    textAlignVertical: 'top',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: R.unit.scale(10),
  },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
});

export default PayPalStore(AddAmount);
