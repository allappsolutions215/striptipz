// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, Dimensions } from 'react-native';
import AuthHeader from '@app/components/auth/AuthHeader';
import { WebView } from 'react-native-webview';
import PayPalStore from '../PayPalStore';
import * as coreAxios from 'axios';

import config from '../../../config';

// import * as coreAxios from 'axios';
import axios from 'axios';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export interface IProps {
  navigation: any;
  route: any;
  name: any;
  url: any;

  getMyProfileResponse: any;
  userResponse: any;

  addTransactionResponse: any;
  addTransactionError: any;

  addAmountResponse: any;
  addAmountError: any;

  addTransactionAction: any;
  addAmountAction: any;
  getMyProfileAction: any;
  userAction: any;
}

let FIRST = true;
let SECOND = true;

const TransferMoney: React.FC<IProps> = (props) => {
  const [approvalUrl, setApprovalUrl] = useState(props.route.params.approvalUrl);
  const [loading, setLoading] = useState(false);
  const ACCESSTOKEN = props.route.params.access_token;

  const getParameterByName = (name: string, url: string) => {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  };

  useEffect(() => {

    if (props.addAmountResponse && loading) {

      setLoading(false);
      props.navigation.navigate('AllSet', { userData: props.userResponse });
    }

    if (props.addAmountError && loading) {
      setLoading(false);
      alert(props.addTransactionError);
      props.navigation.navigate('PatronDashboard');
    }
  }, [props.addAmountResponse, props.addAmountError]);


  const _onNavigationStateChange = (webViewState: any) => {

    setLoading(true);

    if (webViewState.url.includes('https://example.com/')) {
      setApprovalUrl(null);

      var PayerID = getParameterByName('PayerID', webViewState.url);
      var paymentId = getParameterByName('paymentId', webViewState.url);

      coreAxios.default.post(
        `https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`,
        { payer_id: PayerID },
        {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${ACCESSTOKEN}`,
          },
        },
      )
        .then((response: any) => {

          if (response.status === 200) {

            let user_id = props.getMyProfileResponse && props.getMyProfileResponse.data && props.getMyProfileResponse.data.id;

            if (user_id === undefined) {
              user_id = props.userResponse && props.userResponse.id;
            }

            let data = {
              "amount": parseInt(response.data.transactions[0].amount.total),
              "user_id": user_id,
              "transaction_id": response.data.id,
              "email_id": response.data.payer.payer_info.email,
              "status": 'success',
              "data": "testing....!!",
            }
            props.addAmountAction({ data });
          }
        })
        .catch((err: any) => {

          if (err.message) {
            alert(err.message);
            props.navigation.navigate('AddAmount');
          }
        });
    }
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.payPal.add_money}
        navigation={props.navigation}
      />

      <View style={styles.root}>
        <WebView
          style={{ height: windowHeight, width: windowWidth }}
          source={{ uri: approvalUrl }}
          onNavigationStateChange={_onNavigationStateChange}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={false}
        />
      </View>
    </View>
  );
};



const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '100%',
    flex: 1,
    alignSelf: 'center',
    // paddingTop: '20%',
  },
});

export default PayPalStore(TransferMoney);
