import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import UserAction from '@app/store/User/UserAction';
import PaymentAction from '@app/store/Payment/PaymentAction';
import ProfileAction from '@app/store/Profile/ProfileAction';

const mapStateToProps = (state: any) => ({
  userResponse: state.user.userResponse,

  getMyProfileResponse: state.profile.getMyProfileResponse,

  getPPAccessTokenResponse: state.payment.getPPAccessTokenResponse,
  getPPAccessTokenError: state.payment.getPPAccessTokenError,

  getPaymentDataResponse: state.payment.getPaymentDataResponse,
  getPaymentDataError: state.payment.getPaymentDataError,

  addTransactionResponse: state.payment.addTransactionResponse,
  addTransactionError: state.payment.addTransactionError,

  patronTransHistoryResponse: state.payment.patronTransHistoryResponse,
  patronTransHistoryError: state.payment.patronTransHistoryError,

  connetDisconnentPayPalResponse: state.payment.connetDisconnentPayPalResponse,
  connetDisconnentPayPalError: state.payment.connetDisconnentPayPalError,

  createProductResponse: state.payment.createProductResponse,
  createProductError: state.payment.createProductError,

  createSubscriptionPlanResponse: state.payment.createSubscriptionPlanResponse,
  createSubscriptionPlanError: state.payment.createSubscriptionPlanError,

  createSubscriptionResponse: state.payment.createSubscriptionResponse,
  createSubscriptionError: state.payment.createSubscriptionError,

  addAmountResponse: state.payment.addAmountResponse,
  addAmountError: state.payment.addAmountError,

  createSubscriptionServerResponse: state.payment.createSubscriptionServerResponse,
  createSubscriptionServerError: state.payment.createSubscriptionServerError,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      userAction: UserAction.userAction,
      getPPAccessTokenAction: PaymentAction.getPPAccessTokenAction,
      getPaymentDataAction: PaymentAction.getPaymentDataAction,
      addAmountAction: PaymentAction.addAmountAction,
      addTransactionAction: PaymentAction.addTransactionAction,
      getMyProfileAction: ProfileAction.getMyProfileAction,
      patronTransHistoryAction: PaymentAction.patronTransHistoryAction,

      connentDisconnectPayPalAction: PaymentAction.connentDisconnectPayPalAction,
      createProductAction: PaymentAction.createProductAction,
      createSubscriptionPlanAction: PaymentAction.createSubscriptionPlanAction,
      createSubscriptionAction: PaymentAction.createSubscriptionAction,
      createSubscriptionToServerAction: PaymentAction.createSubscriptionToServerAction,
    },
    dispatch,
  );

export default (Dancer: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Dancer);
};
