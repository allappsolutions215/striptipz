
// ext lib
import FingerprintScanner from 'react-native-fingerprint-scanner';

// react
import React, { useState, useEffect } from 'react';
import { StyleSheet, Animated, View, Image } from 'react-native';

// helper
import axios from '../../package/axios';
import SplashStore from './SplashStore';

// constants
import R from '../../res/R';
import { connentDisconnectPayPalSaga } from '@app/store/Payment/PaymentSaga';

export interface IProps {
  navigation: any;

  registerProfileResponse: any;
  signInResponse: any;
  userResponse: any;

  userAction: any;
}

const Splash: React.FC<IProps> = (props) => {
  const [enableLock, setEnableLock] = useState((props.userResponse && props.userResponse.enable_lock));
  // let isVerified = 'no_verified'
  const [isVerified, setIsVerified] = useState({ update_counter: 0, value: '' })


  useEffect(() => {

    async function verifyUser() {

      /**
       * condition for verified user
       * we add noverified and cancel conditoin here
       */

      if (enableLock && isVerified.value !== 'verified' && await checkSensorIsavailable()) {

        // finger print verification
        await verifyFingerPring()
      } else {

        // redict after checks
        handleRedirect()
      }
    }

    // await function for verification
    verifyUser()

  }, [isVerified]);

  const checkSensorIsavailable = async () => {

    let sensor = false

    await FingerprintScanner.isSensorAvailable().then(biometryType => {
      
      sensor = true
    }).catch((error) => {

      sensor = false
      changeUserActionForDesableLock()
    })
    return sensor
  }

  const verifyFingerPring = async () => {

    /**
     * set counter and value for update when user verified, non verified or cancel
     */

    if (isVerified.update_counter < 3) {

      await FingerprintScanner
        .authenticate({ description: 'Scan your fingerprint on the device scanner to continue' })
        .then((msg) => {

          console.log('success-=-==->', msg)

          // msg ? isVerified = 'verified' : isVerified = 'no_verified'
          msg ? setIsVerified({ update_counter: isVerified.update_counter + 1, value: 'verified' })
            : setIsVerified({ update_counter: isVerified.update_counter + 1, value: 'no_verified' })
          FingerprintScanner.release();
        })
        .catch((error) => {

          console.log('error DeviceLocked-=-==->', error)
          // isVerified = 'cancel'
          FingerprintScanner.release();
          setIsVerified({ update_counter: isVerified.update_counter + 1, value: 'cancel' })
        });
    }
  }

  const handleRedirect = () => {

    setTimeout(() => {

      if (props.userResponse && props.userResponse.token) {
        axios.defaults.headers.common[
          'Authorization'
        ] = `Bearer ${props.userResponse.token}`;
      }

      if (!props.userResponse || !props.userResponse.user_type) {
        props.navigation.reset({
          index: 0,
          routes: [{ name: 'AuthSelection' }],
        });
      } else if (props.userResponse.otp_varified === 1) {

        if (props.userResponse.name) {

          // for dancer 
          if (
            props.userResponse.user_type &&
            props.userResponse.user_type.toLowerCase() == 'dancer'
          ) {

            // check dancer photo
            if (
              !props.userResponse.image_count ||
              props.userResponse.image_count == 0
            ) {
              props.navigation.reset({
                index: 0,
                routes: [
                  {
                    name: 'AddPhotos',
                    params: {
                      userData: {
                        user_id: props.userResponse.user_id,
                        user_type: props.userResponse.user_type,
                      },
                      hideBack: true,
                    },
                  },
                ],
              });

              // goto dashboard
            } else {
              props.navigation.reset({
                index: 0,
                routes: [
                  {
                    name: 'DancerDashboard',
                    params: {
                      userData: props.userResponse,
                    },
                  },
                ],
              });
            }

            // for patron
          } else {
            props.navigation.reset({
              index: 0,
              routes: [
                {
                  name: 'PatronDashboard',
                  params: {
                    userData: props.userResponse,
                  },
                },
              ],
            });
          }
        } else {
          props.navigation.reset({
            index: 0,
            routes: [
              {
                name: 'CompleteProfile',
                params: {
                  userData: {
                    user_id: props.userResponse.user_id,
                    user_type: props.userResponse.user_type,
                  },
                  hideBack: true,
                },
              },
            ],
          });
        }
      } else {
        props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'Otp',
              params: {
                userData: props.userResponse,
                calledFrom: 'SPLASH',
                hideBack: true,
              },
            },
          ],
        });
      }
    }, 3000);
  }

  const changeUserActionForDesableLock = () => {

    if (props.userResponse) {

      props.userAction({
        ...props.userResponse,
        enable_lock: false,
      });

      setEnableLock(false)

    }
  }


  return (
    <View style={styles.container}>

      <Image
        resizeMode={'contain'}
        style={styles.img}
        source={R.image.splash_black()}
      />
    </View>
  );
};

export default SplashStore(Splash);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: R.color.black,
  },
  img: {
    flex: 1,
  },
});
