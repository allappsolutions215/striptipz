import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import UserAction from '@app/store/User/UserAction';

const mapStateToProps = (state: any) => ({
  signInResponse: state.auth.signInResponse,
  signInError: state.auth.signInError,

  registerProfileResponse: state.auth.registerProfileResponse,
  registerProfileError: state.auth.registerProfileError,

  userResponse : state.user.userResponse
});

const mapDispatchToProps = (dispatch: any) => bindActionCreators({

  userAction: UserAction.userAction,

}, dispatch);

export default (IntroSlider: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(IntroSlider);
};
