// main imports
import React, {useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, ScrollView} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import MobileNumberInput from '@app/components/common/MobileNumberInput';
import CountryPicker from 'react-native-country-picker-modal';
import AuthStore from '../AuthStore';
import ShakeText from '@app/components/layout/ShakeText';
import {utils} from '@app/util/utils';

export interface IProps {
  navigation: any;

  forgotPassStep1Action: Function;
  forgotPassStep1Response: any;
  forgotPassStep1Error: any;
}

const ForgotPassword: React.FC<IProps> = (props) => {
  const [countryCode, setCountryCode] = useState('1');
  const [visible, setVisible] = useState(false);
  const [mobileNumber, setMobileNumber] = useState('');
  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');
  const [loading, setLoading] = useState(false);

  const onSelect = (country: any) => {
    setCountryCode(
      country.callingCode && country.callingCode[0]
        ? country.callingCode[0]
        : '1',
    );
  };

  const sendOtp = () => {
    if (!mobileNumber || mobileNumber.length < 8) {
      setFormError(R.strings.error.enter_valid_mobile_number);
      setStartAnimation(true);
    } else {
      setFormError('');
      setLoading(true);
      props.forgotPassStep1Action({
        country_code: countryCode,
        mobile: mobileNumber,
      });
    }
  };

  // *************** useEffect of FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) ***************
  useEffect(() => {
    if (loading) {
      if (
        props.forgotPassStep1Response &&
        props.forgotPassStep1Response.data &&
        props.forgotPassStep1Response.data.user_id
      ) {
        setFormError('');
        setLoading(false);
        props.navigation.navigate('Otp', {
          data: props.forgotPassStep1Response,
          calledFrom: 'FORGOT_PASSWORD',
          userData: {
            country_code: countryCode,
            phone: mobileNumber,
            user_id: props.forgotPassStep1Response.data.user_id,
          },
        });
      } else if (props.forgotPassStep1Error) {
        setFormError(utils.getErrorMessage(props.forgotPassStep1Error));
        setStartAnimation(true);
        setLoading(false);
      }
    }
  }, [props.forgotPassStep1Response, props.forgotPassStep1Error]);

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.auth.forgot_password_title}
        navigation={props.navigation}
      />

      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <MobileNumberInput
            value={mobileNumber}
            onChangeText={(text) => setMobileNumber(text)}
            placeholder={R.strings.auth.enter_mobile}
            countryCode={countryCode}
            onCodePress={() => setVisible(!visible)}
          />

          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          <Button
            loading={loading}
            onPress={() => sendOtp()}
            text={R.strings.common.continue}
          />
          <CountryPicker
            {...{
              onSelect,
            }}
            placeholder={''}
            visible={visible}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textForgot: {
    textAlign: 'center',
    color: 'white',
    marginTop: R.unit.scale(30),
  },
  txtError: {
    textAlign: 'center',
    color: R.color.black,
    marginTop: R.unit.scale(10),
  },
});

export default AuthStore(ForgotPassword);
