// main imports
import React, {useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Keyboard,
  Platform,
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import BorderInput from '@app/components/common/BorderInput';
import SelectionView from '@app/components/common/SelectionView';
import AuthStore from '../AuthStore';
import ShakeText from '@app/components/layout/ShakeText';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {utils} from '@app/util/utils';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';

export interface IProps {
  navigation: any;
  route: any;
  registerProfileAction: Function;
  registerProfileResponse: any;
  registerProfileError: any;
  userAction: any;

  updateDancerProfileAction: Function;
  updateDancerProfileResponse: any;
  updateDancerProfileError: any;

  updatePatronProfileAction: Function;
  updatePatronProfileResponse: any;
  updatePatronProfileError: any;
}

const CompleteProfile: React.FC<IProps> = (props) => {
  const [data, setData] = useState({
    name: '',
    screenName: '',
    dob: '',
    gender: R.strings.profile.male,
    websiteUrl: '',
    password: '',
    confirmPassword: '',
  });

  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');
  const [loading, setLoading] = useState(false);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: any) => {
    hideDatePicker();
    setData({...data, dob: moment(date).format('YYYY/MM/DD')});
  };

  const userData =
    props.route && props.route.params && props.route.params.userData
      ? props.route.params.userData
      : false;

  const handleResponse = (response: any) => {
    setFormError('');
    setLoading(false);
    let data = response.data;
    if (response.data.user_type == 'dancer') {
      data.image_count = 0;
    }
    props.userAction(data);
    props.navigation.navigate(
      response.data.user_type == 'dancer' ? 'AddPhotos' : 'LinkPayPal',
      {userData},
    );
  };
  const handleError = (error: any) => {
    setFormError(utils.getErrorMessage(error));
    setStartAnimation(true);
    setLoading(false);
  };

  useEffect(() => {
    if (loading) {
      if (props.registerProfileResponse) {
        handleResponse(props.registerProfileResponse);
      } else if (props.updateDancerProfileResponse) {
        handleResponse(props.updateDancerProfileResponse);
      } else if (props.updatePatronProfileResponse) {
        handleResponse(props.updatePatronProfileResponse);
      } else if (props.registerProfileError) {
        handleError(props.registerProfileError);
      } else if (props.updateDancerProfileError) {
        handleError(props.updateDancerProfileError);
      } else if (props.updatePatronProfileError) {
        handleError(props.updatePatronProfileError);
      }
    }
  }, [
    props.registerProfileResponse,
    props.registerProfileError,
    props.updateDancerProfileResponse,
    props.updateDancerProfileError,
    props.updatePatronProfileResponse,
    props.updatePatronProfileError,
  ]);

  const showError = (errorMsg: string) => {
    setStartAnimation(true);
    setFormError(errorMsg);
  };

  const validateData = () => {
    if (!data.name || data.name.trim().length == 0) {
      showError(R.strings.error.enter_name);
    } else if (
      (userData.user_type == 'dancer' && !data.screenName) ||
      (userData.user_type == 'dancer' && data.screenName.trim().length == 0)
    ) {
      showError(R.strings.error.enter_screen_name);
    } else if (!data.dob) {
      showError(R.strings.error.enter_dob);
    } else if (moment().diff(data.dob, 'years') < 18) {
      showError(R.strings.error.eighteen_plus_error);
    } else if (userData.user_type == 'dancer' && !data.websiteUrl) {
      showError(R.strings.error.enter_website);
    } else if (!data.password && userData.user_id) {
      showError(R.strings.error.enter_password);
    } else if (data.password.length < 8 && userData.user_id) {
      showError(R.strings.error.password_minimum_8);
    } else if (!data.confirmPassword && userData.user_id) {
      showError(R.strings.error.enter_confirm_pass);
    } else if (data.password != data.confirmPassword && userData.user_id) {
      showError(R.strings.error.passwords_not_match);
    } else {
      setFormError('');
      setLoading(true);

      if (userData.user_type == 'dancer') {
        if (userData.user_id) {
          // WE HAVE USER_ID MEANS USER IS NEW AND DONT HAVE AUTH TOKEN YET : NEED TO REGISTER THE USER PROFILE
          props.registerProfileAction({
            user_id: userData.user_id,
            name: data.name,
            dob: data.dob,
            gender: data.gender.toLowerCase().toLowerCase(),
            password: data.password,
            confirm_password: data.confirmPassword,
            screen_name: data.screenName,
            website_url: data.websiteUrl,
          });
        } else {
          // WE DON'T HAVE USER_ID MEANS WE HAVE AUTH TOKEN : WE NEET TO UPDATE THE USER PROFILE
          props.updateDancerProfileAction({
            name: data.name,
            dob: data.dob,
            gender: data.gender.toLowerCase().toLowerCase(),
            screen_name: data.screenName,
            website_url: data.websiteUrl,
          });
        }
      } else {
        if (userData.user_id) {
          // WE HAVE USER_ID MEANS USER IS NEW AND DONT HAVE AUTH TOKEN YET : NEED TO REGISTER THE USER PROFILE
          props.registerProfileAction({
            user_id: userData.user_id,
            name: data.name,
            dob: data.dob,
            gender: data.gender.toLowerCase(),
            password: data.password,
            confirm_password: data.confirmPassword,
          });
        } else {
          // WE DON'T HAVE USER_ID MEANS WE HAVE AUTH TOKEN : WE NEET TO UPDATE THE USER PROFILE
          props.updatePatronProfileAction({
            name: data.name,
            dob: data.dob,
            gender: data.gender.toLowerCase().toLowerCase(),
          });
        }
      }
    }
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.complete_profile}
        navigation={props.navigation}
        hideBackButton={
          props.route && props.route.params && props.route.params.hideBack
        }
      />
      <KeyboardAwareScrollView
        contentContainerStyle={styles.container}
        //style={{flex: 1}}
        enableAutomaticScroll={Platform.OS === 'ios'}
        // extraHeight={130}
        // extraScrollHeight={130}
        enableOnAndroid={true}>
        <View style={styles.root}>
          <BorderInput
            value={data.name}
            onChangeText={(text) => setData({...data, name: text})}
            placeholder={R.strings.profile.enter_name}
          />

          {/* *********** SCREEN NAME WILL ONLY SHOW TO DANCER *********** */}
          {userData.user_type == 'dancer' && (
            <BorderInput
              value={data.screenName}
              onChangeText={(text) => setData({...data, screenName: text})}
              placeholder={R.strings.profile.on_screen_name}
            />
          )}

          <BorderInput
            value={data.dob}
            onChangeText={(text) => setData({...data, dob: text})}
            editable={false}
            placeholder={R.strings.profile.date_of_birth}
            rightComponent={
              <View>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => showDatePicker()}>
                  <Image
                    style={styles.calendarImg}
                    resizeMode={'contain'}
                    source={R.image.calendar()}
                  />
                </TouchableOpacity>
              </View>
            }
          />

          <SelectionView
            title={R.strings.profile.select_gender}
            onPress={(gender: string) => setData({...data, gender: gender})}
            checkedIndex={data.gender}
            firstText={R.strings.profile.male}
            secondText={R.strings.profile.female}
          />

          {/* *********** WEBSITE URL WILL ONLY SHOW TO DANCER *********** */}
          {userData.user_type == 'dancer' && (
            <BorderInput
              value={data.websiteUrl}
              onChangeText={(text) => setData({...data, websiteUrl: text})}
              placeholder={R.strings.profile.enter_url}
            />
          )}

          {userData.user_id && (
            <BorderInput
              value={data.password}
              onChangeText={(text) => setData({...data, password: text})}
              placeholder={R.strings.profile.create_password}
              secureTextEntry
              textContentType={'oneTimeCode'}
              blurOnSubmit={false}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
          )}
          {userData.user_id && (
            <BorderInput
              value={data.confirmPassword}
              onChangeText={(text) => setData({...data, confirmPassword: text})}
              placeholder={R.strings.profile.confirm_password}
              secureTextEntry
              textContentType={'oneTimeCode'}
              blurOnSubmit={false}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
          )}

          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          <Button
            onPress={() => validateData()}
            gutterTop={R.unit.scale(30)}
            text={R.strings.common.continue}
            loading={loading}
          />

          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  calendarImg: {height: R.unit.scale(22), width: R.unit.scale(22)},
});

export default AuthStore(CompleteProfile);
