// main imports
import React, {useState, useEffect, useRef} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import CheckboxSelector from '@app/components/common/CheckboxSelector';
import ShakeText from '@app/components/layout/ShakeText';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import MobileNumberInput from '@app/components/common/MobileNumberInput';
import SelectionView from '@app/components/common/SelectionView';
import CountryPicker from 'react-native-country-picker-modal';
import AuthStore from '../AuthStore';
import {utils} from '@app/util/utils';

export interface IProps {
  navigation: any;

  signUpAction: Function;
  signUpResponse: any;
  signUpError: any;

  userAction: any;
  userResponse: any;
}

const SignUp: React.FC<IProps> = (props) => {
  const [countryCode, setCountryCode] = useState('1');
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [startAnimation, setStartAnimation] = useState(false);
  const [mobileNumber, setMobileNumber] = useState('');
  const [formError, setFormError] = useState('');
  const [is18Plus, setIs18Plus] = useState(false);
  const [agreeToTnC, setAgreeToTnC] = useState(false);
  const [userType, setUserType] = useState(
    R.strings.common.patron.toLowerCase(),
  );
  const shakeRef = useRef();

  const onSelect = (country: any) => {
    setCountryCode(
      country.callingCode && country.callingCode[0]
        ? country.callingCode[0]
        : '1',
    );
  };

  const signUp = () => {
    if (!mobileNumber || mobileNumber.length < 8) {
      setFormError(R.strings.error.enter_valid_mobile_number);
      setStartAnimation(true);
    } else if (!is18Plus) {
      setFormError(R.strings.error.eighteen_plus_error);
      setStartAnimation(true);
    } else if (!agreeToTnC) {
      setFormError(R.strings.error.accept_tnc);
      setStartAnimation(true);
    } else {
      setFormError('');
      setLoading(true);
      // TODO REPLACE patrone with patron : Coz its typo from backend
      props.signUpAction({
        country_code: countryCode,
        phone: mobileNumber,
        fcm_token: props.userResponse.fcm_token,
        user_type:
          userType == R.strings.common.patron.toLowerCase()
            ? 'patrone'
            : R.strings.common.dancer.toLowerCase(),
      });
    }
  };

  useEffect(() => {
    if (loading) {
      if (props.signUpResponse) {
        setFormError('');
        setLoading(false);
        let data = {
          user_type:
            userType == R.strings.common.patron.toLowerCase()
              ? 'patrone'
              : R.strings.common.dancer.toLowerCase(),
          user_id: props.signUpResponse.data.id,
          phone: props.signUpResponse.data.phone,
          country_code: props.signUpResponse.data.country_code,
        };
        props.userAction(data);
        props.navigation.navigate('Otp', {
          data: props.signUpResponse,
          userData: {
            country_code: countryCode,
            phone: mobileNumber,
            user_type:
              userType == R.strings.common.patron.toLowerCase()
                ? 'patrone'
                : R.strings.common.dancer.toLowerCase(),
            user_id: props.signUpResponse.data.id,
          },
          calledFrom: 'SIGN_UP',
        });
      } else if (props.signUpError) {
        setFormError(utils.getErrorMessage(props.signUpError));
        setStartAnimation(true);
        setLoading(false);
      }
    }
  }, [props.signUpResponse, props.signUpError]);

  return (
    <View style={styles.container}>
      <AuthHeader text={R.strings.auth.sign_up} navigation={props.navigation} />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <MobileNumberInput
            value={mobileNumber}
            onChangeText={(text) => setMobileNumber(text)}
            placeholder={R.strings.auth.enter_mobile}
            countryCode={countryCode}
            onCodePress={() => setVisible(!visible)}
          />
          <SelectionView
            title={R.strings.common.select_type}
            onPress={(type: string) => setUserType(type)}
            checkedIndex={userType}
            firstText={R.strings.common.patron}
            secondText={R.strings.common.dancer}
          />
          <View style={styles.checkContainer}>
            <CheckboxSelector
              onPress={() => setIs18Plus(!is18Plus)}
              isChecked={is18Plus}
              title={R.strings.auth.confirm_18}
            />
            <CheckboxSelector
              onPress={() => setAgreeToTnC(!agreeToTnC)}
              isChecked={agreeToTnC}
              title={R.strings.auth.i_agree}
              highlightText={R.strings.auth.terms_condition}
            />
          </View>

          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          <Button
            onPress={() => signUp()}
            gutterTop={R.unit.scale(20)}
            loading={loading}
            text={R.strings.common.continue}
          />

          <CountryPicker
            {...{
              onSelect,
            }}
            placeholder={''}
            visible={visible}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  checkContainer: {
    marginTop: R.unit.scale(25),
  },
  txtError: {
    textAlign: 'center',
    color: R.color.black,
    marginTop: R.unit.scale(10),
  },
});

export default AuthStore(SignUp);
