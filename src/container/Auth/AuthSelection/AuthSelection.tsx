// main imports
import React, { useEffect } from 'react';

import R from '../../../res/R';

// components
import { StyleSheet, View, ImageBackground } from 'react-native';
import Button from '@app/components/common/Button';
import AuthStore from '../AuthStore';

export interface IProps {
  navigation: any;
  userResponse: any;
  userAction: any;
}

const AuthSelection: React.FC<IProps> = (props) => {

  useEffect(() => {

    let fcm_token = (props.userResponse && props.userResponse.fcm_token);
    props.userAction({ fcm_token: fcm_token });
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMode={'cover'}
        style={styles.root}
        source={R.image.splash()}>
        <Button
          onPress={() => props.navigation.navigate('SignIn')}
          text={R.strings.auth.sign_in}
        />
        <Button
          onPress={() => {
            // props.navigation.navigate('LinkPayPal');
            props.navigation.navigate('SignUp')
          }}
          text={R.strings.auth.sign_up}
        />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 100,
  },
});

export default AuthStore(AuthSelection);
