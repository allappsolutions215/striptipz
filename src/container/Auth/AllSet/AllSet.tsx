// main imports
import React, {RefObject, useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, Image} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';

export interface IProps {
  navigation: any;
  route: any;
}

const AllSet: React.FC<IProps> = (props) => {
  const userData =
    props.route && props.route.params && props.route.params.userData
      ? props.route.params.userData
      : false;
  
  const gotoNext = () => {
    if (userData && userData.user_type) {
      if (userData.user_type === 'dancer') {
        props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'DancerDashboard',
              params: {
                userData,
              },
            },
          ],
        });
      } else {
        props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'PatronDashboard',
              params: {
                userData,
              },
            },
          ],
        });
      }
    } else {
      alert(R.strings.error.something_went_wrong);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.root}>
        <Image style={styles.logoImg} source={R.image.app_512_orange()} />
        <Text variant={'title2'} style={styles.titleText}>
          {R.strings.auth.you_all_set}
        </Text>
        <Button
          btnWrapperStyles={styles.button}
          onPress={() => gotoNext()}
          gutterTop={R.unit.scale(30)}
          text={
            userData.user_type == 'dancer'
              ? R.strings.common.start
              : R.strings.auth.start_tippin
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    marginTop: R.unit.scale(30),
    textAlign: 'center',
    fontSize: R.unit.scale(30),
    fontWeight: 'bold',
  },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
  logoImg: {height: R.unit.scale(200), width: R.unit.scale(200)},
});

export default AllSet;
