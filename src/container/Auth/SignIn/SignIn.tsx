// main imports
import React, { useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, Text, TextInput, ScrollView } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import MobileNumberInput from '@app/components/common/MobileNumberInput';
import BorderInput from '@app/components/common/BorderInput';
import CountryPicker from 'react-native-country-picker-modal';
import AuthStore from '../AuthStore';
import ShakeText from '@app/components/layout/ShakeText';
import { utils } from '@app/util/utils';

export interface IProps {
  navigation: any;

  signInAction: Function;
  signInResponse: any;
  signInError: any;

  userAction: any;
  userResponse: any;
}

const SignIn: React.FC<IProps> = (props) => {
  const [countryCode, setCountryCode] = useState('1');
  const [visible, setVisible] = useState(false);
  const [mobileNumber, setMobileNumber] = useState('');
  const [password, setPassword] = useState('');
  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');
  const [loading, setLoading] = useState(false);

  const onSelect = (country: any) => {
    setCountryCode(
      country.callingCode && country.callingCode[0]
        ? country.callingCode[0]
        : '1',
    );
  };

  const signIn = () => {

    if (!mobileNumber || mobileNumber.length < 8) {
      setFormError(R.strings.error.enter_valid_mobile_number);
      setStartAnimation(true);
    } else if (!password || password.length < 6) {
      setFormError(R.strings.error.enter_valid_password);
      setStartAnimation(true);
    } else {
      setFormError('');
      setLoading(true);
      props.signInAction({
        country_code: countryCode,
        phone: mobileNumber,
        password: password,
        fcm_token: props.userResponse.fcm_token
      });
    }
  };

  useEffect(() => {

    if (loading) {
      if (props.signInResponse) {

        setFormError('');
        setLoading(false);
        props.userAction(props.signInResponse.data);
        if (
          props.signInResponse.data &&
          props.signInResponse.data.otp_varified &&
          props.signInResponse.data.otp_varified === 1
        ) {
          // **** OTP VERIFIED ****
          //Backend change : Change response if user not verifed number : Navigate to OTP verificaition or Dashboard or Complete profile :

          if (props.signInResponse.data.name) {
            // **** GOT THE NAME : MEANS PROFILE IS COMPLETED : NAVIGATE TO DASHBOARD ACCORDING TO USER TYPE: ****
            if (props.signInResponse.data.user_type == 'patrone') {
              props.navigation.navigate('PatronDashboard');
            } else {
              // **** IF DANCER DONT HAVE IMAGE UPLOADED : ****
              if (
                !props.signInResponse.data.image_count ||
                props.signInResponse.data.image_count == 0
              ) {
                props.navigation.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'AddPhotos',
                      params: {
                        userData: {
                          //user_id: "",
                          user_type: props.signInResponse.data.user_type,
                        },
                        hideBack: true,
                      },
                    },
                  ],
                });
              } else {
                props.navigation.navigate('DancerDashboard');
              }
            }
          } else {
            // **** PROFILE NOT COMPLETED : COMPLETE PROFILE FIRST THEN GO TO DASHBOARD ACCORDING TO USER TYPE: ****
            // **** COMPELETE PROFILE SCREEN WILL NOT SHOW PASSWORD FIEDLS : BECAUSE USER LOGGED IN MEANS ALREADY HAVE THE PASSWORD: ****
            // **** IT WILL CALL ()

            props.navigation.reset({
              index: 0,
              routes: [
                {
                  name: 'CompleteProfile',
                  params: {
                    userData: {
                      user_type: props.signInResponse.data.user_type,
                    },
                    hideBack: true,
                    calledFrom: 'SIGN_IN',
                  },
                },
              ],
            });
          }
        } else {
          // **** OTP NOT VERIFIED ****
          props.navigation.navigate('Otp', {
            data: props.signInResponse,
            calledFrom: 'SIGN_IN',
            userData: {
              country_code: countryCode,
              phone: mobileNumber,
              user_type: props.signInResponse.data.user_type,
            },
          });
        }
      } else if (props.signInError) {
        setFormError(utils.getErrorMessage(props.signInError));
        setLoading(false);
        setStartAnimation(true);
      }
    }
  }, [props.signInResponse, props.signInError]);

  return (
    <View style={styles.container}>
      <AuthHeader text={R.strings.auth.sign_in} navigation={props.navigation} />

      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <MobileNumberInput
            value={mobileNumber}
            onChangeText={(text) => setMobileNumber(text)}
            placeholder={R.strings.auth.enter_mobile}
            countryCode={countryCode}
            onCodePress={() => setVisible(!visible)}
          />
          <BorderInput
            value={password}
            onChangeText={(text) => setPassword(text)}
            placeholder={R.strings.auth.enter_password}
            secureTextEntry
          />
          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />
          <View>
            <Text
              onPress={() => props.navigation.navigate('ForgotPassword')}
              style={styles.textForgot}>
              {R.strings.auth.forgot_password}
            </Text>
          </View>

          <Button
            loading={loading}
            onPress={() => signIn()}
            text={R.strings.auth.sign_in}
          />
          <CountryPicker
            {...{
              onSelect,
            }}
            placeholder={''}
            visible={visible}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textForgot: {
    textAlign: 'center',
    color: 'white',
    marginTop: R.unit.scale(30),
  },
  txtError: {
    textAlign: 'center',
    color: R.color.black,
    marginTop: R.unit.scale(10),
  },
});

export default AuthStore(SignIn);
