// main imports
import React, { useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, Text, TextInput, ScrollView } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import MobileNumberInput from '@app/components/common/MobileNumberInput';
import BorderInput from '@app/components/common/BorderInput';
import AuthStore from '../AuthStore';

export interface IProps {
  navigation: any;

  userResponse: any;

  changePasswordResponse: any;
  changePasswordError: any;

  forgotPassStep3Response: any;
  forgotPassStep3Error: any;

  forgotPassStep3Action: any;
  changePasswordAction: any;

}

const ChangePassword: React.FC<IProps> = (props) => {
  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const onUpdatePassword = () => {
    setLoading(true)

    const data = {
      old_password: oldPassword,
      // user_id: props.userResponse.token,
      new_password: newPassword,
      confirm_password: confirmPassword,
    };
    props.changePasswordAction(data);
  };

  useEffect(() => {
    if (props.changePasswordResponse && loading) {
      setLoading(false)

      console.log('response', props.changePasswordResponse)
      // props.userAction(false);
      // props.navigation.reset({
      //   index: 0,
      //   routes: [
      //     {
      //       name: 'Root',
      //     },
      //   ],
      // });
      // props.navigation.navigate('Otp', { calledFrom: 'CHANGE_PASSWORD' })
      props.navigation.navigate('AllSet', { userData: props.userResponse })

    }
    if (props.changePasswordError && loading) {
      setLoading(false)
      alert(props.changePasswordError)
    }
  }, [props.changePasswordResponse, props.changePasswordError]);

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.change_password}
        navigation={props.navigation}
      />

      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <View style={styles.mainContainer}>
            <BorderInput
              value={oldPassword}
              onChangeText={(text) => setOldPassword(text)}
              placeholder={R.strings.auth.old_password}
              secureTextEntry
            />

            <BorderInput
              value={newPassword}
              onChangeText={(text) => setNewPassword(text)}
              placeholder={R.strings.auth.new_password}
              secureTextEntry
            />

            <BorderInput
              value={confirmPassword}
              onChangeText={(text) => setConfirmPassword(text)}
              placeholder={R.strings.auth.confirm_password}
              secureTextEntry
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              loading={loading}
              onPress={() => {
                onUpdatePassword();
              }}
              text={R.strings.common.update}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textForgot: {
    textAlign: 'center',
    color: 'white',
    marginTop: R.unit.scale(30),
  },
  mainContainer: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
    marginTop: R.unit.scale(50),
  },
  buttonContainer: {
    width: '100%',
    flex: 0.2,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default AuthStore(ChangePassword);
