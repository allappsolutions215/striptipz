import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import AuthAction from '../../store/Auth/AuthAction';
import ProfileAction from '../../store/Profile/ProfileAction';
import UserAction from '@app/store/User/UserAction';

const mapStateToProps = (state: any) => ({
  
  signInResponse: state.auth.signInResponse,
  signInError: state.auth.signInError,

  signUpResponse: state.auth.signUpResponse,
  signUpError: state.auth.signUpError,

  verifyOtpResponse: state.auth.verifyOtpResponse,
  verifyOtpError: state.auth.verifyOtpError,

  // *************** FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) ***************
  forgotPassStep1Response: state.auth.forgotPassStep1Response,
  forgotPassStep1Error: state.auth.forgotPassStep1Error,

  registerProfileResponse: state.auth.registerProfileResponse,
  registerProfileError: state.auth.registerProfileError,

  userResponse: state.user.userResponse,

  forgotPassStep3Response: state.auth.forgotPassStep3Response,
  forgotPassStep3Error: state.auth.forgotPassStep3Error,

  updateDancerProfileResponse: state.profile.updateDancerProfileResponse,
  updateDancerProfileError: state.profile.updateDancerProfileError,

  updatePatronProfileResponse: state.profile.updatePatronProfileResponse,
  updatePatronProfileError: state.profile.updatePatronProfileError,
  changePasswordResponse: state.auth.changePasswordResponse,
  changePasswordError: state.auth.changePasswordError,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      signInAction: AuthAction.signInAction,
      signUpAction: AuthAction.signUpAction,
      verifyOtpAction: AuthAction.verifyOtpAction,
      // *************** FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) ***************
      forgotPassStep1Action: AuthAction.forgotPassStep1Action,
      forgotPassStep3Action: AuthAction.forgotPassStep3Action,
      registerProfileAction: AuthAction.registerProfileAction,

      updateDancerProfileAction: ProfileAction.updateDancerProfileAction,
      updatePatronProfileAction: ProfileAction.updatePatronProfileAction,

      userAction: UserAction.userAction,

      changePasswordAction: AuthAction.changePasswordAction
    },
    dispatch,
  );

export default (Auth: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Auth);
};
