// main imports
import React, {RefObject, useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Alert,
  Animated,
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import OtpInputs from 'react-native-otp-inputs';
import AuthStore from '../AuthStore';
import {utils} from '@app/util/utils';

export interface IProps {
  navigation: any;
  route: any;
  verifyOtpAction: Function;
  verifyOtpResponse: any;
  verifyOtpError: any;

  forgotPassStep1Action: Function;
  forgotPassStep1Response: any;
  forgotPassStep1Error: any;

  userAction: any;
  userResponse: any;
}

const Otp: React.FC<IProps> = (props) => {
  const [mobileNumber, setMobileNumber] = useState('');
  const [password, setPassword] = useState('');
  const otpRef: RefObject<OtpInputs> = useRef();
  const [s, setS] = useState(true);
  const [fourDigit, setFourDigit] = useState(false);
  const [timeLeft, setTimeLeft] = useState(120);
  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');
  const [loading, setLoading] = useState(false);
  const [resendLoading, setResendLoading] = useState(false);
  const shakeAnimation = new Animated.Value(0);

  useEffect(() => {
    if (startAnimation) {
      startShake();
    }
    // exit early when we reach 0
    if (!timeLeft) return;
    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft(timeLeft - 1);
    }, 1000);
    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft, startAnimation]);

  // *************** useEffect of VERIFY OTP ***************
  useEffect(() => {
    if (loading) {
      if (props.verifyOtpResponse) {
        setFormError('');
        setLoading(false);
        if (
          props.route &&
          props.route.params &&
          props.route.params.calledFrom
        ) {
          const userData =
            props.route &&
            props.route.params &&
            props.route.params.userData &&
            props.route.params.userData &&
            props.route.params.userData;

          if (
            props.route.params.calledFrom == 'SIGN_IN' ||
            props.route.params.calledFrom == 'SIGN_UP' ||
            props.route.params.calledFrom == 'SPLASH'
          ) {
            let data = props.userResponse
              ? {...props.userResponse, otp_varified: 1}
              : {otp_varified: 1};

            console.log('****** SAVE OTP TO USER_ACTION data', data);

            props.userAction(data);
            if (userData.user_type) {
              props.navigation.navigate('CompleteProfile', {
                userData: {
                  user_id: userData.user_id,
                  user_type: userData.user_type,
                },
                // userData,
                // profileData: props.route.params.data,
              });
            } else {
              showErrorAlert();
            }
          } else {
            if (props.route.params.calledFrom == 'FORGOT_PASSWORD') {
              props.navigation.navigate('ResetPassword', {
                userData: {
                  user_id: userData.user_id,
                },
              });
            } else {
              alert('In Progress IP78');
            }
          }
        } else {
          showErrorAlert();
        }
      } else if (props.verifyOtpError) {
        setFormError(utils.getErrorMessage(props.verifyOtpError));
        setStartAnimation(true);
        setLoading(false);
      }
    }
  }, [props.verifyOtpResponse, props.verifyOtpError]);

  // *************** useEffect of FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) ***************
  useEffect(() => {
    if (resendLoading) {
      if (props.forgotPassStep1Response) {
        setFormError('');
        setResendLoading(false);
        setTimeLeft(120);
      } else if (props.forgotPassStep1Error) {
        setFormError(utils.getErrorMessage(props.forgotPassStep1Error));
        setStartAnimation(true);
        setResendLoading(false);
      }
    }
  }, [props.forgotPassStep1Response, props.forgotPassStep1Error]);

  const focusOTP = () => {
    otpRef.current.focus();
  };

  const resetOTP = () => {
    otpRef.current.reset();
  };

  const toggle = () => {
    setFourDigit((fourDigit) => !fourDigit);
  };

  const handleChange = (code: string) => {
    setFourDigit(code);
    setS((s) => !s);
  };

  const verifyOtp = () => {
    if (
      props.route &&
      props.route.params &&
      props.route.params.userData &&
      props.route.params.userData.user_id
    ) {
      if (fourDigit && fourDigit.length == 4) {
        setLoading(true);
        props.verifyOtpAction({
          user_id: props.route.params.userData.user_id,
          otp: fourDigit,
        });
      } else {
        setStartAnimation(true);
        setFormError(R.strings.error.enter_valid_otp);
      }
    } else {
      showErrorAlert();
    }
  };

  const showErrorAlert = () => {
    Alert.alert(
      R.strings.app_name,
      R.strings.error.something_went_wrong_try_again,
      [{text: 'Ok', onPress: () => props.navigation.goBack()}],
      {cancelable: false},
    );
  };

  const resendOtp = () => {
    if (
      props.route &&
      props.route.params &&
      props.route.params.userData &&
      props.route.params.userData &&
      props.route.params.userData.country_code &&
      props.route.params.userData.phone
    ) {
      // *************** FORGOT PASSWORD STEP 1 (ALSO USED AS RESEND OTP) ***************
      setResendLoading(true);
      setFormError('');
      props.forgotPassStep1Action({
        country_code: props.route.params.userData.country_code,
        mobile: props.route.params.userData.phone,
      });
    } else {
      console.warn('CHECK CHECK 207');
      showErrorAlert();
    }
  };

  const startShake = () => {
    Animated.sequence([
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: -10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 10,
        duration: 100,
        useNativeDriver: true,
      }),
      Animated.timing(shakeAnimation, {
        toValue: 0,
        duration: 100,
        useNativeDriver: true,
      }),
    ]).start(() => {
      setStartAnimation(false);
    });
  };

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.auth.verification}
        navigation={props.navigation}
        hideBackButton={
          props.route && props.route.params && props.route.params.hideBack
        }
      />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <Text style={styles.txtNote}>{R.strings.auth.verify_note}</Text>
          <View style={{width: '80%', height: R.unit.scale(100)}}>
            <OtpInputs
              inputContainerStyles={{
                width: R.unit.scale(70),
                height: R.unit.scale(50),
                borderColor: R.color.white,
                borderWidth: R.unit.scale(1),
              }}
              inputStyles={{
                flex: 1,
                color: R.color.white,
                textAlign: 'center',
                fontSize: 18,
                fontWeight: 'bold',
              }}
              autofillFromClipboard={false}
              clearTextOnFocus
              handleChange={handleChange}
              keyboardType="phone-pad"
              returnKeyType={'done'}
              numberOfInputs={4}
              ref={otpRef}
              selectTextOnFocus={false}
            />
          </View>
          <View style={styles.timerContainer}>
            <Text
              onPress={() => timeLeft == 0 && resendOtp()}
              style={styles.txtResend}>
              {timeLeft == 0
                ? R.strings.auth.resend_code
                : R.strings.auth.resend_code_in}{' '}
            </Text>
            {timeLeft != 0 && <Text style={styles.txtTime}> {timeLeft}</Text>}
          </View>

          {/* <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          /> */}

          {/* We are not using ShakeText Component here becuase : setInterval method updating UI in every 1 second and it is blocking animation. */}

          <Animated.Text
            style={[
              styles.titleText,
              {transform: [{translateX: shakeAnimation}]},
            ]}>
            {formError}
          </Animated.Text>

          <Button
            onPress={() => verifyOtp()}
            gutterTop={R.unit.scale(10)}
            text={R.strings.auth.verify}
            loading={loading}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtResend: {
    textAlign: 'center',
    color: 'white',
    fontSize: R.unit.scale(16),
  },
  timerContainer: {
    marginTop: R.unit.scale(30),
    flexDirection: 'row',
  },
  txtTime: {
    // textAlign: 'left',
    color: 'white',
    fontSize: R.unit.scale(16),
    width: R.unit.scale(35),
  },
  txtNote: {
    textAlign: 'left',
    width: '80%',
    color: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    fontSize: R.unit.scale(16),
  },
  titleText: {marginTop: R.unit.scale(40), color: R.color.black},
});

export default AuthStore(Otp);
