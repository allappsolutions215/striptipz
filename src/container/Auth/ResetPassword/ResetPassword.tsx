// main imports
import React, {useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  ScrollView,
  Keyboard,
  Text,
  Alert,
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import AuthStore from '../AuthStore';
import ShakeText from '@app/components/layout/ShakeText';
import {utils} from '@app/util/utils';
import BorderInput from '@app/components/common/BorderInput';

export interface IProps {
  navigation: any;
  route: any;

  forgotPassStep3Action: Function;
  forgotPassStep3Response: any;
  forgotPassStep3Error: any;

  userAction: any;
}

const ResetPassword: React.FC<IProps> = (props) => {
  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const resetPassword = () => {
    if (!password) {
      showError(R.strings.error.enter_password);
    } else if (password.length < 8) {
      showError(R.strings.error.password_minimum_8);
    } else if (!confirmPassword) {
      showError(R.strings.error.enter_confirm_pass);
    } else if (password != confirmPassword) {
      showError(R.strings.error.passwords_not_match);
    } else {
      const userData =
        props.route &&
        props.route.params &&
        props.route.params.userData &&
        props.route.params.userData &&
        props.route.params.userData;
      if (userData) {
        setLoading(true);
        setFormError('');
        props.forgotPassStep3Action({
          user_id: userData.user_id,
          new_password: password,
          confirm_password: confirmPassword,
        });
      } else {
        showError(R.strings.error.something_went_wrong);
      }
    }
  };

  const showError = (errorMsg: string) => {
    setStartAnimation(true);
    setFormError(errorMsg);
  };

  useEffect(() => {
    if (loading) {
      if (props.forgotPassStep3Response && props.forgotPassStep3Response.data) {
        setFormError('');
        setLoading(false);
        props.userAction(false);
        Alert.alert(
          R.strings.app_name,
          R.strings.auth.success_set_new_pass,
          [
            {
              text: 'OK',
              onPress: () => {
                props.userAction(false);
                props.navigation.reset({
                  index: 0,
                  routes: [
                    {
                      name: 'Root',
                    },
                  ],
                });
              },
            },
          ],
          {cancelable: false},
        );
      } else if (props.forgotPassStep3Error) {
        setFormError(utils.getErrorMessage(props.forgotPassStep3Error));
        setStartAnimation(true);
        setLoading(false);
      }
    }
  }, [props.forgotPassStep3Response, props.forgotPassStep3Error]);

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.auth.resetPassword}
        navigation={props.navigation}
      />

      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <Text style={styles.txtNote}>{R.strings.auth.set_new_pass}</Text>

          <BorderInput
            value={password}
            onChangeText={(text) => setPassword(text)}
            placeholder={R.strings.profile.create_password}
            secureTextEntry
            textContentType={'oneTimeCode'}
            blurOnSubmit={false}
            onSubmitEditing={() => Keyboard.dismiss()}
          />

          <BorderInput
            value={confirmPassword}
            onChangeText={(text) => setConfirmPassword(text)}
            placeholder={R.strings.profile.confirm_password}
            secureTextEntry
            textContentType={'oneTimeCode'}
            blurOnSubmit={false}
            onSubmitEditing={() => Keyboard.dismiss()}
          />

          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          <Button
            loading={loading}
            onPress={() => resetPassword()}
            text={R.strings.common.reset}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtNote: {
    textAlign: 'left',
    width: '80%',
    color: 'white',
    alignSelf: 'center',
    alignItems: 'center',
    fontSize: R.unit.scale(16),
  },
});

export default AuthStore(ResetPassword);
