// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import PhotoBox from '@app/components/profile/PhotoBox';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import Image from '@app/components/common/Image';
import PositionView from '@app/components/layout/PositionView';
import PatronStore from '../PatronStore';
import config from '@app/config';

// props
export interface IProps {
  navigation: any;

  getDancersListAction: any;
  dancersListResponse: any;
  dancersListError: any;

  updateLocationAction: any;
  updateLocationResponse: any;
  updateLocationError: any;

  getMyProfileAction: any;
  getMyProfileResponse: any;

  userResponse: any;
  userAction: any;

}

const Dashboard: React.FC<IProps> = (props) => {
  // constants and veriables
  const [totalAmount, setTotalAmount] = useState(0);

  // life cycles
  useEffect(() => {
    props.getMyProfileAction();
    props.getDancersListAction({ range: 200 });
  }, []);

  useEffect(() => {

    if (props.getMyProfileResponse) {

      let wallet_amount = ((props.getMyProfileResponse && props.getMyProfileResponse.data && props.getMyProfileResponse.data.wallet) || 0);

      setTotalAmount(wallet_amount);

      let val_obj = { ...props.userResponse };

      if (props.getMyProfileResponse.data) {

        val_obj = {
          ...val_obj,
          ...props.getMyProfileResponse.data,
          wallet: wallet_amount,
        };
      }

      if (props.getMyProfileResponse.images) {

        val_obj = {
          ...val_obj,
          ...props.getMyProfileResponse.images,
          wallet: wallet_amount,
        };
      }

      props.userAction({
        ...val_obj,
      });
    }
  }, [props.getMyProfileResponse]);

  useEffect(() => {

    if (props.userResponse) {

      setTotalAmount(props.userResponse.wallet);
    }
  }, [props.userResponse]);

  //---------- views

  const _renderHeaderView = () => {

    const { userResponse } = props;

    return (
      <View style={styles.headerContainer}>
        <View>
          <Text variant={'title3'} color={R.color.gray5}>
            {R.strings.home.tip_limit}
          </Text>
          <View style={styles.amountContainer}>
            <Text
              variant={'title'}
              color={R.color.primary}
              style={styles.amountStyle}>
              $
              {totalAmount}
            </Text>
            <Image
              onPressImage={() => props.navigation.navigate("AddAmount")}
              resizeMode={'contain'}
              imageStyles={styles.plusIcon}
              containerStyles={styles.plusIconContainer}
              source={R.image.plus()}
            />
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => props.navigation.navigate('MyAccount')}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text variant={'title2'} color={R.color.white}>
              {props.userResponse && props.userResponse.name
                ? props.userResponse.name
                : 'NA'}
            </Text>
            <Image
              onPressImage={() => props.navigation.navigate('MyAccount')}
              resizeMode={'contain'}
              imageStyles={styles.profileIcon}
              containerStyles={styles.profileIconContainer}
              source={
                userResponse &&
                  userResponse.images &&
                  userResponse.images[0] &&
                  userResponse.images[0].image
                  ? { uri: config.imageBaseUrl + userResponse.images[0].image }
                  : R.image.girl()
              }
            // source={R.image.user()}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          {_renderHeaderView()}
          <View style={styles.mapContainer}>
            <View>
              <Text
                style={styles.titleText}
                variant={'title'}
                color={R.color.white}>
                {R.strings.home.nearby_dancers}
              </Text>
              <Text
                style={styles.titleText}
                variant={'title4'}
                color={R.color.primary}>
                {R.strings.home.selected_dancer_near_your}
              </Text>
            </View>
            <PositionView
              currentLocation={(position: any) => {
                props.updateLocationAction(position)
              }
              }
              navigation={props.navigation}
              dancerList={
                props.dancersListResponse &&
                  props.dancersListResponse.data &&
                  props.dancersListResponse.data.length > 0
                  ? props.dancersListResponse.data
                  : []
              }
            />
            <View>
              <Text
                style={styles.tipText}
                variant={'title4'}
                color={R.color.white}>
                {R.strings.home.tip_denomition}{' '}
                <Text
                  style={styles.tipTextAmount}
                  variant={'title'}
                  color={R.color.primary}>
                  {/* {'$1'} */}
                  {
                    (props.userResponse && props.userResponse.tip_amount) || '$1'
                  }
                </Text>
              </Text>

              <Button
                btnWrapperStyles={styles.button}
                onPress={() => props.navigation.navigate('TipDemonimation')}
                text={R.strings.common.change}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

// css
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.black },
  root: {
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: R.unit.scale(40),
  },
  headerContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  plusIcon: { width: R.unit.scale(12), height: R.unit.scale(12) },
  profileIcon: { width: R.unit.scale(25), height: R.unit.scale(25) },

  plusIconContainer: {
    width: R.unit.scale(20),
    height: R.unit.scale(20),
    borderRadius: R.unit.scale(20 / 2),
    backgroundColor: R.color.primary,
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileIconContainer: {
    width: R.unit.scale(30),
    height: R.unit.scale(30),
    borderRadius: R.unit.scale(30 / 2),
    backgroundColor: R.color.primary,
    marginStart: R.unit.scale(15),
    alignItems: 'center',
    justifyContent: 'center',
  },
  amountStyle: {
    fontWeight: 'bold',
    marginEnd: R.unit.scale(5),
  },
  amountContainer: {
    flexDirection: 'row',
    marginTop: R.unit.scale(5),
    alignItems: 'center',
  },
  titleText: {
    textAlign: 'center',
    marginTop: R.unit.scale(5),
  },
  tipText: {
    textAlign: 'center',
  },
  mapContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  tipTextAmount: {
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: R.color.primary,
    width: R.unit.scale(100),
    height: R.unit.scale(20),
    alignSelf: 'center',
  },
});

export default PatronStore(Dashboard);

// import React from 'react';
// import {
//   Platform,
//   View,
//   StyleSheet,
//   TouchableOpacity,
//   ScrollView,
//   Text,
//   Switch,
// } from 'react-native';
// import {PROVIDER_GOOGLE, PROVIDER_DEFAULT} from 'react-native-maps';
// import DisplayLatLng from './examples/DisplayLatLng';
// import ViewsAsMarkers from './examples/ViewsAsMarkers';
// import EventListener from './examples/EventListener';
// import MarkerTypes from './examples/MarkerTypes';
// import DraggableMarkers from './examples/DraggableMarkers';
// import PolygonCreator from './examples/PolygonCreator';
// import PolylineCreator from './examples/PolylineCreator';
// import GradientPolylines from './examples/GradientPolylines';
// import AnimatedViews from './examples/AnimatedViews';
// import AnimatedMarkers from './examples/AnimatedMarkers';
// import Callouts from './examples/Callouts';
// import Overlays from './examples/Overlays';
// import DefaultMarkers from './examples/DefaultMarkers';
// import CustomMarkers from './examples/CustomMarkers';
// import CachedMap from './examples/CachedMap';
// import LoadingMap from './examples/LoadingMap';
// import MapBoundaries from './examples/MapBoundaries';
// import TakeSnapshot from './examples/TakeSnapshot';
// import FitToSuppliedMarkers from './examples/FitToSuppliedMarkers';
// import FitToCoordinates from './examples/FitToCoordinates';
// import LiteMapView from './examples/LiteMapView';
// import CustomTiles from './examples/CustomTiles';
// import WMSTiles from './examples/WMSTiles';
// import ZIndexMarkers from './examples/ZIndexMarkers';
// import StaticMap from './examples/StaticMap';
// import MapStyle from './examples/MapStyle';
// import LegalLabel from './examples/LegalLabel';
// import SetNativePropsOverlays from './examples/SetNativePropsOverlays';
// import CustomOverlay from './examples/CustomOverlay';
// import MapKml from './examples/MapKml';
// import BugMarkerWontUpdate from './examples/BugMarkerWontUpdate';
// import ImageOverlayWithAssets from './examples/ImageOverlayWithAssets';
// import ImageOverlayWithURL from './examples/ImageOverlayWithURL';
// import AnimatedNavigation from './examples/AnimatedNavigation';
// import OnPoiClick from './examples/OnPoiClick';
// import TestIdMarkers from './examples/TestIdMarkers';
// import IndoorMap from './examples/IndoorMap';
// import CameraControl from './examples/CameraControl';
// import MassiveCustomMarkers from './examples/MassiveCustomMarkers';
// import GeojsonMap from './examples/Geojson';

// const IOS = Platform.OS === 'ios';
// const ANDROID = Platform.OS === 'android';

// function makeExampleMapper(useGoogleMaps) {
//   if (useGoogleMaps) {
//     return (example) => [
//       example[0],
//       [example[1], example[3]].filter(Boolean).join(' '),
//     ];
//   }
//   return (example) => example;
// }

// type Props = {};
// export default class App extends React.Component<Props> {
//   constructor(props) {
//     super(props);

//     this.state = {
//       Component: null,
//       useGoogleMaps: ANDROID,
//     };
//   }

//   renderExample([Component, title]) {
//     return (
//       <TouchableOpacity
//         key={title}
//         style={styles.button}
//         onPress={() => this.setState({Component})}>
//         <Text>{title}</Text>
//       </TouchableOpacity>
//     );
//   }

//   renderBackButton() {
//     return (
//       <TouchableOpacity
//         style={styles.back}
//         onPress={() => this.setState({Component: null})}>
//         <Text style={styles.backButton}>&larr;</Text>
//       </TouchableOpacity>
//     );
//   }

//   renderGoogleSwitch() {
//     return (
//       <View>
//         <Text>Use GoogleMaps?</Text>
//         <Switch
//           onValueChange={(value) => this.setState({useGoogleMaps: value})}
//           style={styles.googleSwitch}
//           value={this.state.useGoogleMaps}
//         />
//       </View>
//     );
//   }

//   renderExamples(examples) {
//     const {Component, useGoogleMaps} = this.state;

//     return (
//       <View style={styles.container}>
//         {Component && (
//           <Component
//             provider={useGoogleMaps ? PROVIDER_GOOGLE : PROVIDER_DEFAULT}
//           />
//         )}
//         {Component && this.renderBackButton()}
//         {!Component && (
//           <ScrollView
//             style={StyleSheet.absoluteFill}
//             contentContainerStyle={styles.scrollview}
//             showsVerticalScrollIndicator={false}>
//             {IOS && this.renderGoogleSwitch()}
//             {examples.map((example) => this.renderExample(example))}
//           </ScrollView>
//         )}
//       </View>
//     );
//   }

//   render() {
//     return this.renderExamples(
//       [
//         // [<component>, <component description>, <Google compatible>, <Google add'l description>]
//         [StaticMap, 'StaticMap', true],
//         [DisplayLatLng, 'Tracking Position', true, '(incomplete)'],
//         [ViewsAsMarkers, 'Arbitrary Views as Markers', true],
//         [EventListener, 'Events', true, '(incomplete)'],
//         [MarkerTypes, 'Image Based Markers', true],
//         [DraggableMarkers, 'Draggable Markers', true],
//         [PolygonCreator, 'Polygon Creator', true],
//         [PolylineCreator, 'Polyline Creator', true],
//         [GradientPolylines, 'Gradient Polylines', true],
//         [AnimatedViews, 'Animating with MapViews'],
//         [AnimatedMarkers, 'Animated Marker Position'],
//         [Callouts, 'Custom Callouts', true],
//         [Overlays, 'Circles, Polygons, and Polylines', true],
//         [DefaultMarkers, 'Default Markers', true],
//         [CustomMarkers, 'Custom Markers', true],
//         [TakeSnapshot, 'Take Snapshot', true, '(incomplete)'],
//         [CachedMap, 'Cached Map'],
//         [LoadingMap, 'Map with loading'],
//         [MapBoundaries, 'Get visible map boundaries', true],
//         [FitToSuppliedMarkers, 'Focus Map On Markers', true],
//         [FitToCoordinates, 'Fit Map To Coordinates', true],
//         [LiteMapView, 'Android Lite MapView'],
//         [CustomTiles, 'Custom Tiles', true],
//         [WMSTiles, 'WMS Tiles', true],
//         [ZIndexMarkers, 'Position Markers with Z-index', true],
//         [MapStyle, 'Customize the style of the map', true],
//         [LegalLabel, 'Reposition the legal label', true],
//         [SetNativePropsOverlays, 'Update native props', true],
//         [CustomOverlay, 'Custom Overlay Component', true],
//         [TestIdMarkers, 'Test ID for Automation', true],
//         [MapKml, 'Load Map with KML', true],
//         [BugMarkerWontUpdate, "BUG: Marker Won't Update (Android)", true],
//         [ImageOverlayWithAssets, 'Image Overlay Component with Assets', true],
//         [ImageOverlayWithURL, 'Image Overlay Component with URL', true],
//         [AnimatedNavigation, 'Animated Map Navigation', true],
//         [OnPoiClick, 'On Poi Click', true],
//         [IndoorMap, 'Indoor Map', true],
//         [CameraControl, 'CameraControl', true],
//         [MassiveCustomMarkers, 'MassiveCustomMarkers', true],
//         [GeojsonMap, 'Geojson', true],
//       ]
//         // Filter out examples that are not yet supported for Google Maps on iOS.
//         .filter(
//           (example) =>
//             ANDROID || (IOS && (example[2] || !this.state.useGoogleMaps)),
//         )
//         .map(makeExampleMapper(IOS && this.state.useGoogleMaps)),
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     ...StyleSheet.absoluteFillObject,
//     justifyContent: 'flex-end',
//     alignItems: 'center',
//   },
//   scrollview: {
//     alignItems: 'center',
//     paddingVertical: 40,
//   },
//   button: {
//     flex: 1,
//     marginTop: 10,
//     backgroundColor: 'rgba(220,220,220,0.7)',
//     paddingHorizontal: 18,
//     paddingVertical: 12,
//     borderRadius: 20,
//   },
//   back: {
//     position: 'absolute',
//     top: 20,
//     left: 12,
//     backgroundColor: 'rgba(255,255,255,0.4)',
//     padding: 12,
//     borderRadius: 20,
//     width: 80,
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   backButton: {fontWeight: 'bold', fontSize: 30},
//   googleSwitch: {marginBottom: 10},
// });
