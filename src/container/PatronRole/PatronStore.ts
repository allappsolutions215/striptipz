import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import UserAction from '@app/store/User/UserAction';
import DashboardAction from '@app/store/Dashboard/DashboardAction';
import PaymentAction from '@app/store/Payment/PaymentAction';
import ProfileAction from '@app/store/Profile/ProfileAction';

const mapStateToProps = (state: any) => ({
  userResponse: state.user.userResponse,

  updateLocationResponse: state.dashboard.updateLocationResponse,
  updateLocationError: state.dashboard.updateLocationError,

  dancersListResponse: state.dashboard.dancersListResponse,
  dancersListError: state.dashboard.dancersListError,

  getMyProfileResponse: state.profile.getMyProfileResponse,
  getMyProfileError: state.profile.getMyProfileError,

  changeUserTypeResponse: state.profile.changeUserTypeResponse,
  changeUserTypeError: state.profile.changeUserTypeError,

  tipByPatronResponse: state.payment.tipByPatronResponse,
  tipByPatronError: state.payment.tipByPatronError,

  dancerDetailsResponse: state.profile.dancerDetailsResponse,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      userAction: UserAction.userAction,
      getDancersListAction: DashboardAction.getDancersListAction,
      updateLocationAction: DashboardAction.updateLocationAction,
      getMyProfileAction: ProfileAction.getMyProfileAction,
      changeUserType: ProfileAction.changeUserType,

      tipByPatronAction: PaymentAction.tipByPatronAction,
    },
    dispatch,
  );

export default (Dancer: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Dancer);
};