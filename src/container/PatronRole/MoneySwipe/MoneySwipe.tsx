// main imports
import React, { useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import Text from '@app/components/common/Text';
import BackArrow from '@app/components/common/BackArrow';
import TipBox from '@app/components/common/TipBox';
import Swiper from 'react-native-deck-swiper';
import Card from '@app/components/common/card';
import PatronStore from '../PatronStore';
// import { parse } from '@babel/core';

export interface IProps {
  navigation: any;

  userResponse: any;
  userAction: any;

  dancerDetailsResponse: any;
  getMyProfileResponse: any;

  tipByPatronAction: any;
  tipByPatronResponse: any;
  tipByPatronError: any;
}

const MoneySwipe: React.FC<IProps> = (props) => {

  // constants and veriables
  // const [tipAmount, setTipAmount] = useState((props.userResponse && props.userResponse.tip_amount) || R.strings.home.one_dollar);
  let [wallet, setWallet] = useState(props.userResponse && props.userResponse.wallet)
  let tipAmount = (props.userResponse && props.userResponse.tip_amount) || R.strings.home.one_dollar;
  const [selectedAmount, setSelectedTipAmount] = useState(tipAmount);
  const [loading, setLoading] = useState(false);
  const [priceArray, setPriceArray] = useState([
    R.strings.home.five_dollar,
    R.strings.home.ten_dollar,
    R.strings.home.hundred_dollar,
    R.strings.home.plus,
  ]);

  // life cycle's
  useEffect(() => {

    // setup 
    setSelectedTipAmount(tipAmount)
    let prePriceArray = priceArray
    let index = prePriceArray.findIndex(x => Number(x.toString().replace('$', '')) > Number(tipAmount.toString().replace('$', '')))

    /** logic
     * find index of the tip amount 
     * remove previous and latest tip in array
     * add new tip in price array
     * manage price array length of 5
     */

    console.log('props.userResponse.wallet', JSON.stringify(props.userResponse))
    setWallet(props.userResponse.wallet);

    if (prePriceArray.length < 5) {

      if (prePriceArray.includes(tipAmount)) {

        prePriceArray.splice(index, 0, R.strings.home.one_dollar)
      } else {

        prePriceArray.splice(index, 0, tipAmount)
      }
    }

    if (prePriceArray.length = 5) {

      if (!prePriceArray.includes(tipAmount)) {

        if (index >= 0) {

          prePriceArray.splice(index, 1, tipAmount)
        }

        if (index < 0) {

          prePriceArray.splice((prePriceArray.length - 2), 1, tipAmount)
        }
      }
    }

    setPriceArray(prePriceArray)

  }, [props.userResponse]);

  useEffect(() => {

    if (loading) {

      if (props.tipByPatronResponse) {

        let tip_amount = Number(selectedAmount.toString().replace('$', ''));
        let wallet_amount = parseInt(wallet) - parseInt(tip_amount)

        props.userAction({
          ...props.userResponse,
          wallet: wallet_amount,
        })
        setLoading(false);
      }
      if (props.tipByPatronError) {

        setLoading(false);
      }
    }

  }, [props.tipByPatronResponse, props.tipByPatronError]);

  // user's action

  const onAmountChange = (item: any) => {

    if (item === R.strings.home.plus) {

      props.navigation.navigate('TipDemonimation');
    } else {

      setSelectedTipAmount(item);
      if (item !== tipAmount) {

        props.userAction({
          ...props.userResponse,
          tip_amount: item,
        });
      }
    }
  };

  const onSwipedTop = () => {

    setLoading(true);

    if (selectedAmount && props.dancerDetailsResponse) {


      let amount
      if (selectedAmount) {

        amount = Number(selectedAmount.toString().replace('$', ''));
      }

      if (parseInt(wallet) >= parseInt(amount)) {

        let val_obj = {

          dancer_id: props.dancerDetailsResponse.data.id,
          amount: amount.toString(),
        };

        props.tipByPatronAction(val_obj);
      } else {

        props.navigation.navigate('AddAmount');
      }
    }
  }

  //---------- views

  return (
    <View style={styles.container}>
      <BackArrow navigation={props.navigation} />
      <Text variant={'title3'} style={styles.titleText}>
        {R.strings.home.swipe_rain}
      </Text>

      <View style={styles.container}>
        <View
          style={{
            flex: 3,
          }}>
          <Swiper
            containerStyle={{
              flex: 1,
              backgroundColor: R.color.black,
              alignSelf: 'center',
            }}
            cardStyle={{
              height: '100%',
              width: '100%',
              //flex: 1,
              backgroundColor: R.color.transparent,
              justifyContent: 'center',
              alignContent: 'center',
              alignItems: 'center',
            }}
            // ref={(swiper) => {
            //   this.swiper = swiper;
            // }}
            infinite
            horizontalSwipe={false}
            verticalSwipe={true}
            //goBackToPreviousCardOnSwipeLeft={calledFrom == 'MATCH'}
            // disableLeftSwipe
            // disableRightSwipe
            // disableTopSwipe

            disableBottomSwipe
            animateCardOpacity
            animateOverlayLabelsOpacity
            showSecondCard={true}
            cards={[1, 2, 3, 4, 5, 1, 2, 3, 4, 5]}
            renderCard={(card) => <Card card={card} />}
            cardIndex={0}
            stackSize={5}
            onTapCard={(position: number) => {
              //this.gotoDetails(dataArray[position]);
            }}
            //onSwipedLeft={(index) => this.onSwipedLeft(index)}
            //onSwipedRight={(index) => this.onSwipedRight(index)}
            onSwipedTop={() => onSwipedTop()}
            cardVerticalMargin={0}
            cardHorizontalMargin={0}
            backgroundColor={R.color.black}
            stackSeparation={15}
          />
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: R.color.primary,
            justifyContent: 'space-evenly',
            paddingHorizontal: R.unit.scale(20),
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <Text variant={'title3'} color={R.color.white}>
              {R.strings.home.tip_limit}
            </Text>
            <Text
              style={styles.tipTextAmount}
              variant={'title'}
              color={R.color.white}>
              ${
                wallet
              }
              {/* {R.strings.home.two_thousand_dollars} */}
            </Text>
          </View>
          <Text variant={'title3'} color={R.color.white}>
            {R.strings.home.select_denomition}
          </Text>
          <View style={styles.rowContainer}>
            {
              priceArray && priceArray.map((item, index) => {
                return (
                  <TipBox
                    key={index}
                    text={item}
                    onPress={() => onAmountChange(item)}
                    isSelected={item === selectedAmount}
                  />
                );
              })}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.black },
  root: {
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: R.unit.scale(40),
  },
  titleText: {
    textAlign: 'center',
    marginTop: R.unit.scale(15),
  },
  tipTextAmount: {
    fontWeight: 'bold',
    marginStart: R.unit.scale(10),
  },
  rowContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
  },
});

export default PatronStore(MoneySwipe);
