// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, TouchableOpacity, ScrollView, Alert } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import TipBox from '@app/components/common/TipBox';
import BorderInput from '@app/components/common/BorderInput';
import PatronStore from '../PatronStore';

// props
export interface IProps {
  navigation: any;
  route: any;

  userResponse: any;
  userAction: any;
}

const TipDenomination: React.FC<IProps> = (props) => {
  // veriable and constants
  const [tipAmount, setTipAmount] = useState((props.userResponse && props.userResponse.tip_amount) || R.strings.home.one_dollar);
  const [selectedAmount, setSelectedTipAmount] = useState(tipAmount);
  const [amount, setAmount] = useState(tipAmount);
  const [loading, setLoading] = useState(false);
  // tipAmount || R.strings.home.one_dollar

  const priceArray = [
    R.strings.home.one_dollar,
    R.strings.home.five_dollar,
    R.strings.home.ten_dollar,
    R.strings.home.hundred_dollar,
  ];

  // user's action
  const handleAddTip = () => {

    // check for previous tip and current tip amount is same or not
    if (tipAmount !== amount) {

      props.userAction({
        ...props.userResponse,
        tip_amount: amount
      })

      // if tip amout is same as previous show error
    } else {

      let alert_obj = {
        title: R.strings.app_name,
        desc: R.strings.error.tip_err_desc
      }
      setupAlertMsg(alert_obj)
    }
  }

  // setup alert for change user_type
  const setupAlertMsg = (data: any) => {
    Alert.alert(
      data.title,
      data.desc,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            console.log('Ok Pressed')
          },
        },
      ],
      { cancelable: false },
    );
  }

  // on amount change by selector
  const onAmountChange = (item: any) => {

    setSelectedTipAmount(item);
    setAmount(item);
  };

  // lifecycle
  useEffect(() => {

    // take update after tip amount change in pocket
    console.log('tip denomination amount change', tipAmount, amount)

    if (tipAmount !== amount) {

      console.log('tip denomination navigte')
      props.navigation.goBack()

      // if (props.route && props.route.params && props.route.params.mode==='MoneySwipe'){

      //   props.navigation.navigate('MoneySwipe')
      // }else{

      //   props.navigation.navigate('PatronDashboard')
      //   // props.navigation.goBack()
      // }
    }

  }, [props.userResponse.tip_amount]);

  const _renderTitle = (text: string) => {
    return (
      <Text variant={'title2'} style={styles.titleText}>
        {text}
      </Text>
    );
  };

  //---------- views

  console.log('amount', amount)

  return (

    <View style={styles.container}>
      <AuthHeader
        text={R.strings.home.tip_denomition}
        navigation={props.navigation}
      />
      <ScrollView
        contentContainerStyle={styles.container}
        style={styles.container}>
        <View style={styles.root}>
          {_renderTitle(R.strings.payPal.enter_amount)}
          <BorderInput
            keyboardType={'number-pad'}
            value={(amount && amount.length) > 0 ? amount : '$'}
            onChangeText={(text) => {
              setAmount(text), setSelectedTipAmount('');
            }}
            inputContainer={{ marginTop: 0 }}
            containerStyle={styles.borderInputStyle}
          />

          <Text variant={'title2'} style={styles.orText}>
            {R.strings.common.or}
          </Text>

          {_renderTitle(R.strings.home.select_denomition)}
          <View style={styles.rowContainer}>
            {priceArray.map((item, index) => {
              return (
                <TipBox
                  key={index}
                  text={item}
                  onPress={() => onAmountChange(item)}
                  isSelected={item === selectedAmount}
                />
              );
            })}
          </View>
          <Button
            btnWrapperStyles={styles.button}
            onPress={() => handleAddTip()}
            text={R.strings.common.change}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    width: '90%',
    flex: 1,
    alignSelf: 'center',
    paddingTop: R.unit.scale(40),
  },
  titleText: {
    marginTop: R.unit.scale(15),
  },
  tipTextAmount: {
    fontWeight: 'bold',
    marginStart: R.unit.scale(10),
  },
  rowContainer: {
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-evenly',
    marginTop: R.unit.scale(15),
  },
  borderInputStyle: {
    marginTop: R.unit.scale(10),
    marginBottom: R.unit.scale(15),
    width: '100%',
  },
  orText: {
    borderRadius: R.unit.scale(20),
    width: R.unit.scale(40),
    height: R.unit.scale(40),
    borderWidth: R.unit.scale(1),
    borderColor: R.color.white,
    alignSelf: 'center',
    textAlign: 'center',
    textAlignVertical: 'top',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: R.unit.scale(10),
  },
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
});

export default PatronStore(TipDenomination);
