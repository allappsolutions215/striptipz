/* eslint-disable prettier/prettier */
// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Alert,
  StatusBar,
  Platform,
  PermissionsAndroid,
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import DancerHeader from '@app/components/layout/DancerHomeHeader';
import Text from '@app/components/common/Text';
import PhotoBox from '@app/components/profile/PhotoBox';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import CollapsibleToolbar from '@app/components/layout/CollapsibleToolbar';
import moment from 'moment';
import { ScrollView } from 'react-native-gesture-handler';
import DancerStore from '../DancerStore';
import Geolocation from '@react-native-community/geolocation';

export interface IProps {
  navigation: any;

  geTipsListAction: any;
  tipsListResponse: any;
  tipsListError: any;

  updateLocationAction: any;
  updateLocationResponse: any;
  updateLocationError: any;

  getMyProfileResponse: any;
  getMyProfileAction: any;

  userResponse: any;
  userAction: any;

  onlineOfflineResponse: any;
  onlineOfflineError: any;
  onlineOfflineAction: any;
}

const Dashboard: React.FC<IProps> = (props) => {
  const LATITUDE = 37.787471437115805;
  const LONGITUDE = -122.45422817766666;
  const LATITUDE_DELTA = 0.01;
  const LONGITUDE_DELTA = 0.01;
  const [currentMonth, setCurrentMonth] = useState(moment());
  const [listData, setListData] = useState([]);

  useEffect(() => {
    
    props.geTipsListAction(moment(currentMonth).format('DD-MM-YYYY'));
    getLocationPermission();
    getCurrentPosition();
    props.getMyProfileAction();
  }, []);

  useEffect(() => {
    if (props.tipsListResponse) {
      setListData(props.tipsListResponse);
      setIsLoading(1);
    } else if (props.tipsListError) {
      setIsLoading(2);
    }
  }, [props.tipsListResponse, props.tipsListError]);

  useEffect(() => {
    if (props.onlineOfflineResponse) {
      props.getMyProfileAction();
    } else if (props.onlineOfflineError) {
      props.getMyProfileAction();
    }
  }, [props.onlineOfflineResponse, props.onlineOfflineError]);

  const [isLoading, setIsLoading] = useState(0);


  useEffect(() => {
    if (props.getMyProfileResponse) {

      if (
        props.getMyProfileResponse.data ||
        props.getMyProfileResponse.images
      ) {
        props.userAction({
          ...props.userResponse,
          ...props.getMyProfileResponse.data,
          ...props.getMyProfileResponse.images,
        });
      }
    }
  }, [props.getMyProfileResponse]);

  const getPreviousMonth = () => {
    var prevMonthFirstDay = moment(currentMonth.subtract(1, 'months').date(1));
    setCurrentMonth(prevMonthFirstDay);
    setListData([]);
    setIsLoading(0);
    props.geTipsListAction(moment(prevMonthFirstDay).format('DD-MM-YYYY'));

  };

  const getNextMonth = () => {
    var nextMonthLastDay = moment(currentMonth.add(1, 'months').date(1));
    setCurrentMonth(nextMonthLastDay);
    setListData([]);
    setIsLoading(0);
    props.geTipsListAction(moment(nextMonthLastDay).format('DD-MM-YYYY'));

  };

  const getLocationPermission = async () => {
    if (Platform.OS === 'ios') {
      getCurrentPosition();
    } else {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Device current location permission',
            message: 'Allow app to get your current location',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getCurrentPosition();
        } else {
        }
      } catch (err) {
        console.warn(err);
      }
    }
  };

  const getCurrentPosition = () => {
    try {
      //Geolocation.requestAuthorization();
      Geolocation.getCurrentPosition(
        (position) => {
          const region = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          };
          props.updateLocationAction({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          });
        },
        (error) => {
          console.warn(error);
          if (error.code == 2) {
            // Linking.openSettings();
            Alert.alert(R.strings.app_name, R.strings.common.turn_location_on);
          } else {
            alert(error.message);
          }
        },
      );
    } catch (e) {

      alert(e.message || '');
    }
  };

  const toggleOnline = (value: any) => {

    if (value) {

      let is_subscription_valid =
        props.getMyProfileResponse &&
        props.getMyProfileResponse.data &&
        props.getMyProfileResponse.data.is_subscription_valid;
      if (is_subscription_valid === 1) {
        props.onlineOfflineAction({ is_online: value });
      } else {

        // for title and desc on alert
        let alert_obj = {
          title: R.strings.app_name,
          desc: R.strings.profile.dancer_subscription_desc,
        };
        setupAlertMsg(alert_obj);
      }
    }
  };

  const setupAlertMsg = (data: any) => {

    Alert.alert(
      data.title,
      data.desc,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Go to subscription',
          onPress: () => {
            props.navigation.navigate('Subscription', {calledFrom: 'dashboard'});
          },
        },
      ],
      { cancelable: false },
    );
  }

  //---------- views

  const _renderCalanderStrip = () => {
    return (
      <View style={styles.calanderStripContainer}>
        <TouchableOpacity onPress={() => getPreviousMonth()}>
          <Image style={styles.arrowIcon} source={R.image.back_arrow()} />
        </TouchableOpacity>
        <Text
          variant={'title3'}
          style={{ fontWeight: 'bold' }}
          color={R.color.black}>
          {currentMonth.format('MMM YYYY')}
        </Text>
        <TouchableOpacity onPress={() => getNextMonth()}>
          <Image
            style={[styles.arrowIcon, { transform: [{ rotate: '180deg' }] }]}
            source={R.image.back_arrow()}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.root}>
        <ScrollView
          stickyHeaderIndices={[1]}
          showsVerticalScrollIndicator={false}
          style={styles.container}>
          <DancerHeader
            toggleOnline={(val: any) => toggleOnline(val)}
            navigation={props.navigation}
            userResponse={props.userResponse}
          />
          <View>{_renderCalanderStrip()}</View>

          <View
            style={{
              backgroundColor: R.color.white,
            }}>
            <FlatList
              data={listData ? listData : []}
              extraData={listData}
              key={isLoading}
              contentContainerStyle={{
                width: '80%',
                alignSelf: 'center',
                paddingBottom: R.unit.scale(30),
              }}
              scrollEnabled={false}
              renderItem={({ item, index }) => {
                return (
                  <View style={styles.itemContainer}>
                    <Text variant={'title3'} color={R.color.gray1}>
                      {moment(item.created_at).format('DD/MM/YYYY')}
                    </Text>
                    <View style={styles.itemSubContainer}>
                      <Text variant={'title3'} color={R.color.black}>
                        {R.strings.common.total_tips}
                      </Text>
                      <Text variant={'title3'} color={R.color.primary}>
                        ${item.amount}
                      </Text>
                    </View>
                  </View>
                )
              }}
              ListEmptyComponent={() => (
                <View style={styles.emptyContainer}>
                  <Text variant={'title3'} color={R.color.primary}>
                    {R.strings.error.no_tip_for_this_month}
                  </Text>
                </View>
              )}
              keyExtractor={(index) => index.toString()}
              ItemSeparatorComponent={() => <View style={styles.separator} />}
            />
          </View>
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.white },
  root: {
    flex: 1,
    borderTopRightRadius: R.unit.scale(10),
    borderTopLeftRadius: R.unit.scale(10),
  },
  itemContainer: {
    paddingVertical: R.unit.scale(10),
  },
  separator: {
    height: 0.5,
    backgroundColor: R.color.gray1,
  },
  itemSubContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: R.unit.scale(10),
  },
  calanderStripContainer: {
    width: '100%',
    alignSelf: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    height: R.unit.scale(50),
    alignItems: 'center',
    borderBottomColor: R.color.primary,
    borderBottomWidth: R.unit.scale(1),
    paddingHorizontal: '5%',
    backgroundColor: R.color.white,
    borderTopRightRadius: R.unit.scale(10),
    borderTopLeftRadius: R.unit.scale(10),
  },
  arrowIcon: {
    resizeMode: 'contain',
    tintColor: R.color.black,
    width: R.unit.scale(15),
    height: R.unit.scale(15),
  },
  emptyContainer: {
    width: '100%',
    marginTop: R.unit.scale(50),
    alignSelf: 'center',
    alignItems: 'center',
  },
});

export default DancerStore(Dashboard);
