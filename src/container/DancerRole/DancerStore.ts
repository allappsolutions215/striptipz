import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import UserAction from '@app/store/User/UserAction';
import DashboardAction from '@app/store/Dashboard/DashboardAction';
import ProfileAction from '@app/store/Profile/ProfileAction';

const mapStateToProps = (state: any) => ({
  userResponse: state.user.userResponse,

  updateLocationResponse: state.dashboard.updateLocationResponse,
  updateLocationError: state.dashboard.updateLocationError,

  onlineOfflineResponse: state.dashboard.onlineOfflineResponse,
  onlineOfflineError: state.dashboard.onlineOfflineError,

  tipsListResponse: state.dashboard.tipsListResponse,
  tipsListError: state.dashboard.tipsListError,

  getMyProfileResponse: state.profile.getMyProfileResponse,
  getMyProfileError: state.profile.getMyProfileError,

  dancerDetailsResponse: state.profile.dancerDetailsResponse,
  dancerDetailsError: state.profile.dancerDetailsError,

  changeUserTypeResponse: state.profile.changeUserTypeResponse,
  changeUserTypeError: state.profile.changeUserTypeError
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      userAction: UserAction.userAction,
      geTipsListAction: DashboardAction.geTipsListAction,
      onlineOfflineAction: DashboardAction.onlineOfflineAction,
      updateLocationAction: DashboardAction.updateLocationAction,
      getMyProfileAction: ProfileAction.getMyProfileAction,
      viewDancerDetailsAction: ProfileAction.viewDancerDetailsAction,
      changeUserType: ProfileAction.changeUserType
    },
    dispatch,
  );

export default (Dancer: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Dancer);
};
