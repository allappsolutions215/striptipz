// main imports
import React, {RefObject, useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Image as ImageR,
  SafeAreaView,
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import DancerHeader from '@app/components/layout/DancerHomeHeader';
import Text from '@app/components/common/Text';
import PhotoBox from '@app/components/profile/PhotoBox';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import CollapsibleToolbar from '@app/components/layout/CollapsibleToolbar';
import moment from 'moment';
import {ScrollView} from 'react-native-gesture-handler';
import AppIntroSlider from 'react-native-app-intro-slider';
import BackArrow from '@app/components/common/BackArrow';
import DancerStore from '../DancerStore';
import ImageUrl from '../../../config'

export interface IProps {
  navigation: any;
  route: any;
  viewDancerDetailsAction: any;
  dancerDetailsResponse: any;
  dancerDetailsError: any;
}

const DancerProfile: React.FC<IProps> = (props) => {
  //const slider: AppIntroSlider | undefined;
  const tempDancerData = props.route.params.dancerData;
  const slider: RefObject<AppIntroSlider> = useRef();

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [dancerDetails, setDancerDetails] = useState(null);
  const [imageSlides, setImageSlides] = useState([]);

  const slides = [
    {
      key: 0,
      title: 'Title 1',
      text: 'Description.\nSay something cool',
      image: R.image.girl(),
      backgroundColor: '#59b2ab',
    },

    {
      key: 2,
      title: 'Rocket guy',
      text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
      image: R.image.girl2(),
      backgroundColor: '#22bcb5',
    },

    {
      key: 4,
      title: 'Title 2',
      text: 'Other cool stuff',
      image: R.image.girl(),
      backgroundColor: '#febe29',
    },
    {
      key: 5,
      title: 'Rocket guy',
      text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
      image: R.image.girl2(),
      backgroundColor: '#22bcb5',
    },

    {
      key: 7,
      title: 'Title 2',
      text: 'Other cool stuff',
      image: R.image.girl(),
      backgroundColor: '#febe29',
    },
    {
      key: 8,
      title: 'Rocket guy',
      text: "I'm already out of descriptions\n\nLorem ipsum bla bla bla",
      // video:
      // "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WhatCarCanYouGetForAGrand.mp4",
      image: R.image.girl2(),
      backgroundColor: '#22bcb5',
    },
  ];

  useEffect(() => {
    props.viewDancerDetailsAction(tempDancerData && tempDancerData.id);
  }, []);

  useEffect(() => {
    if (props.dancerDetailsResponse) {
      if(props.dancerDetailsResponse.status){
        setDancerDetails(props.dancerDetailsResponse.data)
        let tempArray = [];
        props.dancerDetailsResponse.data && props.dancerDetailsResponse.data.images &&
        props.dancerDetailsResponse.data.images.forEach((item) =>{
          tempArray.push({
            key: item.id,
            title: 'Title',
            text: '',
            image: item.image,
            backgroundColor: '#59b2ab',
          })
        })
        setImageSlides(tempArray)
      }
    } else if (props.dancerDetailsError) {
    }
  }, [props.dancerDetailsResponse, props.dancerDetailsError]);

  const _renderItem = ({item}) => {
    let media = ImageUrl.imageBaseUrl+item.image;
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ImageR
          resizeMode={'cover'}
          style={{width: '100%', height: '100%'}}
          source={{uri: media}}
        />
      </View>
    );
  };

  const _renderPagination = (activeIndex: number) => {
    return (
      <View style={styles.paginationContainer}>
        <SafeAreaView>
          <View style={styles.paginationDots}>
            {imageSlides.length > 0 &&
              imageSlides.map((_: any, i: any) => (
                <TouchableOpacity
                  key={i}
                  style={[
                    styles.dot,
                    i === activeIndex
                      ? {backgroundColor: R.color.white}
                      : {backgroundColor: R.color.whiteTrasparent},
                  ]}
                  // onPress={() => imageSlides?.goToSlide(i, true)}
                />
              ))}
          </View>
        </SafeAreaView>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <BackArrow navigation={props.navigation} />

      {/* <TouchableOpacity
        style={styles.touchable}
        onPress={() => props.navigation.goBack()}>
        <View style={styles.touchableView}>
          <ImageR style={styles.img} source={R.image.back()} />
        </View>
      </TouchableOpacity> */}

      <View style={styles.container}>
        <View
          style={{
            flex: 3,
          }}>
          <AppIntroSlider
            renderItem={_renderItem}
            data={imageSlides && imageSlides.length > 0 ? imageSlides : slides}
            ref={slider}
            showPrevButton={false}
            showNextButton={false}
            showSkipButton={false}
            showDoneButton={false}
            onSlideChange={(index, lastIndex) => setSelectedIndex(index)}
            dotStyle={{
              backgroundColor: R.color.whiteTrasparent,
            }}
            activeDotStyle={{
              backgroundColor: R.color.primary,
            }}
            renderPagination={_renderPagination}
          />
        </View>
        <View
          style={{
            flex: 1,
            backgroundColor: R.color.white,
            justifyContent: 'space-evenly',
          }}>
          <Text
            style={styles.tipTextAmount}
            variant={'title'}
            color={R.color.black}>
            {dancerDetails && dancerDetails.name}
          </Text>
          <Text
            style={styles.tipTextAmount}
            variant={'title3'}
            color={R.color.primary}>
            {R.strings.profile.visit_website}
          </Text>
          <Button
            btnWrapperStyles={styles.button}
            onPress={() => props.navigation.navigate('MoneySwipe')}
            text={R.strings.auth.start_tippin}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.black},
  root: {
    flex: 1,
    backgroundColor: R.color.greenColor,
  },
  paginationContainer: {
    position: 'absolute',
    bottom: R.unit.scale(0),
    left: R.unit.scale(5),
    right: R.unit.scale(5),
  },
  paginationDots: {
    height: R.unit.scale(10),
    margin: R.unit.scale(10),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dot: {
    flex: 1,
    backgroundColor: R.color.white,
    height: 5,
    borderRadius: R.unit.scale(5),
    marginHorizontal: R.unit.scale(4),
  },

  tipTextAmount: {
    //fontWeight: 'bold',
    marginStart: R.unit.scale(30),
  },
  button: {
    backgroundColor: R.color.black,
    height: R.unit.scale(20),
    alignSelf: 'center',
  },
});

export default DancerStore(DancerProfile);
