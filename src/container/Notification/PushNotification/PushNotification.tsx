// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import { View } from 'react-native';
import {
  Platform
} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import NotificationStore from '../AllNotification/NotificationStore';

// props
export interface IProps {
  navigation: any;
  userResponse: any;
  userAction: any;
}

const PushNotification: React.FC<IProps> = (props) => {

  // veriable and constant
  const [token, setToken] = useState('');

  // life cycle's
  // for notification
  useEffect(() => {

    requestUserPermission()
  }, []);

  // for store token
  useEffect(() => {

    if (token) {

      console.log('save fcm token in user response ---', token)
      props.userAction({
        ...props.userResponse,
        fcm_token: token
      });
    }
  }, [token]);

  // helper's

  // request for permission
  const requestUserPermission = async () => {

    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    console.log('Authorization status:-=-=>', authStatus);

    if (enabled) {
      getToken();
    }
  };

  // get token
  const getToken = async () => {

    await messaging().getToken().then(token => {

      console.log('fcm messaging TOKEN ====>', token);
      setToken(token);

      notificationListner();
    });
  };

  // notification listner
  const notificationListner = async () => {

    // messaging().onMessage(async remoteMessage => {
    //   console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
    // });

    // ================================================================ON MESSAGE LISTENER===============================================================//

    messaging().setBackgroundMessageHandler(remoteMessage => {

      console.log('Message handled in the background!-=-=-->', remoteMessage);
    });

    // ================================================================ON MESSAGE LISTENER===============================================================//

    // onNotificationOpened
    // console.log('messaging()', messaging())
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage,
      );
    });

    // ================================================================ON MESSAGE LISTENER===============================================================//
  }

  return (
    <View></View>
  );
};

export default NotificationStore(PushNotification);
