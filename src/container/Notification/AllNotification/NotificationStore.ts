import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import UserAction from '@app/store/User/UserAction';

import NotificationAction from '@app/store/Notification/NotificationAction';


const mapStateToProps = (state: any) => ({
  userResponse: state.user.userResponse,

  notificationResponse: state.notification.notificationResponse,
  notificationError: state.notification.notificationError,

  deleteNotificationResponse: state.notification.deleteNotificationResponse,
  deleteNotificationError: state.notification.deleteNotificationError

});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      userAction: UserAction.userAction,
      notificationAction: NotificationAction.notificationAction,
      deleteNotificationAction: NotificationAction.deleteNotificationAction
    },
    dispatch,
  );

export default (Dancer: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Dancer);
};