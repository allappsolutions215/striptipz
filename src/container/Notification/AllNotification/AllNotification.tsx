// main imports
import React, {RefObject, useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, FlatList} from 'react-native';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import moment from 'moment';

import NotificationStore from './NotificationStore';

export interface IProps {
  navigation: any;
  notificationAction: any;
  notificationResponse: any;
  notificationError: any;

  deleteNotificationAction: any;
  deleteNotificationResponse: any;
  deleteNotificationError: any;
}

const AllNotification: React.FC<IProps> = (props) => {
  const [notificationList, setNotification] = useState([]);
  const exercises = [
    {
      title: 'Subscription Fee Deducted',
      created_at: '02.00 AM',
      amount: '$19.99',
    },
    {
      title: 'New Tipper Has Started Tipping',
      created_at: '01.00 AM',
    },
  ];

  useEffect(() => {
    props.notificationAction();
  }, []);

  useEffect(() => {
    if(props.notificationResponse){
      if(props.notificationResponse.status){
        setNotification(props.notificationResponse.data)
      }
    }else if(props.notificationError){
    }
  }, [props.notificationResponse, props.notificationError]);

  useEffect(() => {
    if(props.deleteNotificationResponse){
      if(props.deleteNotificationResponse.status){
        props.notificationAction();
       alert(props.deleteNotificationResponse.msg)
      }
    }else if(props.deleteNotificationError){
    }
  }, [props.deleteNotificationResponse, props.deleteNotificationError]);

  const onClear = () =>{
    props.deleteNotificationAction()
  }

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.notification.notification}
        navigation={props.navigation}
        color={R.color.black}
        tintColor={R.color.black}
        showClearText
        onClearPress={() => onClear()}
      />
      <View style={styles.root}>
        <FlatList
          data={notificationList}
          contentContainerStyle={{
            alignSelf: 'center',
            width: '85%',
            paddingBottom: R.unit.scale(30),
          }}
          renderItem={({item, index}) => (
            <View style={styles.itemContainer}>
              <View style={styles.itemSubContainer}>
                <Text variant={'title4'} color={R.color.black}>
                  {item.notification}
                </Text>
                <Text
                  style={{marginTop: R.unit.scale(5)}}
                  variant={'content'}
                  color={R.color.gray1}>
                  {moment(item.updated_at).format('LT')}
                </Text>
              </View>
              {item.amount && (
                <View style={{flex: 1.2}}>
                  <Text
                    variant={'title0'}
                    style={{fontWeight: 'bold'}}
                    color={R.color.primary}>
                    {item.amount}
                  </Text>
                </View>
              )}
            </View>
          )}
          ListEmptyComponent={() => (
            <View style={styles.emptyContainer}>
              <Text variant={'title3'} color={R.color.primary}>
                {R.strings.notification.no_notification}
              </Text>
            </View>
          )}
          keyExtractor={(index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.seprator} />}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.white},
  root: {
    flex: 1,
    marginTop: R.unit.scale(20),
  },
  itemContainer: {
    flexDirection: 'row',
    paddingHorizontal: R.unit.scale(20),
    paddingVertical: R.unit.scale(20),
    backgroundColor: R.color.gray23,
    borderRadius: R.unit.scale(10),
    alignItems: 'center',
  },
  itemSubContainer: {
    justifyContent: 'space-between',
    flex: 3,
    paddingEnd: R.unit.scale(10),
  },
  seprator: {
    height: R.unit.scale(20),
  },
  emptyContainer:{
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center'
  }
});


export default NotificationStore(AllNotification);

