// main imports
import React, {useRef, useState, useEffect} from 'react';
import R from '../../../res/R';
// components
import {StyleSheet, View, ScrollView} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import SelectionSheet from '@app/components/layout/SelectionSheet';
import PhotoBox from '@app/components/profile/PhotoBox';
import ShakeText from '@app/components/layout/ShakeText';
import ProfileStore from '../ProfileStore';
import {utils} from '@app/util/utils';
import config from '@app/config';
import axios from 'axios';
export interface IProps {
  navigation: any;
  route: any;

  addPhotoAction: any;
  addPhotoResponse: any;
  addPhotoError: any;

  userAction: any;
  userResponse: any;

  changePasswordAction: any;
  showHeadersAction: any;
}

const AddPhotos: React.FC<IProps> = (props) => {
  const refRBSheet = useRef();

  const [image1, setImage1] = useState('');
  const [image2, setImage2] = useState('');
  const [image3, setImage3] = useState('');
  const [image4, setImage4] = useState('');
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [startAnimation, setStartAnimation] = useState(false);
  const [formError, setFormError] = useState('');

  const [loading1, set1Loading] = useState(false);
  const [loading2, set2Loading] = useState(false);
  const [loading3, set3Loading] = useState(false);
  const [loading4, set4Loading] = useState(false);

  const userData =
    props.route && props.route.params && props.route.params.userData
      ? props.route.params.userData
      : false;

  const setImageUri = (uri: string) => {
    if (selectedIndex == 1) {
      set1Loading(true);
      setImage1(uri);
      props.addPhotoAction({uri: uri, index: 1});
    } else if (selectedIndex == 2) {
      set2Loading(true);
      setImage2(uri);
      props.addPhotoAction({uri: uri, index: 2});
    } else if (selectedIndex == 3) {
      set3Loading(true);
      setImage3(uri);
      props.addPhotoAction({uri: uri, index: 3});
    } else if (selectedIndex == 4) {
      set4Loading(true);
      setImage4(uri);
      props.addPhotoAction({uri: uri, index: 4});
    }
  };

  const openBottomSheet = (index: number) => {
    setSelectedIndex(index);
    refRBSheet.current.open();
  };

  const validateData = () => {
    const userData =
      props.route && props.route.params && props.route.params.userData
        ? props.route.params.userData
        : false;
    if (!image1 && !image2 && !image3 && !image4) {
      setFormError(R.strings.error.upload_at_lease_one_photo);
      setStartAnimation(true);
    } else {
      props.navigation.navigate('LinkPayPal', {userData});
    }
  };

  const changePassword = () => {
    props.showHeadersAction();

    // const headers = {
    //   Accept: 'application/json',
    //   Authorization:
    //     'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC8zLjIzOC4yNDkuMTIyXC9pbmRleC5waHBcL2FwaVwvbG9naW4iLCJpYXQiOjE2MTYzOTc0NzMsImV4cCI6MTYxNjQwMTA3MywibmJmIjoxNjE2Mzk3NDczLCJqdGkiOiJySE5pbEJGUWZhU0sxc1g1Iiwic3ViIjo1MywicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.15__QZ5Y6CjotpoUADLuwVgFQTGiHTO-u5Bg4NHuYhU',
    // };
    // // const axios = require('axios').default;

    // // const instance = axios.create({
    // //   baseURL: config.baseURL,
    // //   timeout: 1000,
    // //   headers: headers,
    // // });
    // // console.log(303030, instance);

    // axios
    //   .post(
    //     config.baseURL + 'add-image-for-dancer',
    //     {
    //       old_password: '12345678',
    //       new_password: '12345678',
    //       confirm_password: '12345678',
    //     },
    //     {headers: headers},
    //   )
    //   .then((res) => {
    //     console.log(113113, res);
    //   })
    //   .catch((err) => {
    //     console.log(117117, err.response);
    //   });

    // props.changePasswordAction({
    //   old_password: '12345678',
    //   new_password: '12345678',
    //   confirm_password: '12345678',
    // });
  };

  useEffect(() => {
    if (loading1 || loading2 || loading3 || loading4) {
      if (props.addPhotoResponse) {
        setFormError('');
        let index = props.addPhotoResponse.index;
        index == 1
          ? set1Loading(false)
          : index == 2
          ? set2Loading(false)
          : index == 3
          ? set3Loading(false)
          : set4Loading(false);

        props.userAction({
          ...props.userResponse,
          image_count: props.userResponse.image_count
            ? props.userResponse.image_count + 1
            : 1,
        });
      } else if (props.addPhotoError) {
        setFormError(utils.getErrorMessage(props.addPhotoError));
        setStartAnimation(true);
      }
    }
  }, [props.addPhotoResponse, props.addPhotoError]);

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.add_photos}
        navigation={props.navigation}
        hideBackButton={
          props.route && props.route.params && props.route.params.hideBack
        }
      />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <View style={styles.boxContainer}>
            <PhotoBox
              containerStyle={{width: R.unit.scale(140)}}
              image={image1}
              onPress={() => openBottomSheet(1)}
              loading={loading1}
            />
            <PhotoBox
              containerStyle={{width: R.unit.scale(140)}}
              image={image2}
              onPress={() => openBottomSheet(2)}
              loading={loading2}
            />
          </View>
          <View style={[styles.boxContainer, {marginBottom: R.unit.scale(20)}]}>
            <PhotoBox
              containerStyle={{width: R.unit.scale(140)}}
              image={image3}
              onPress={() => openBottomSheet(3)}
              loading={loading3}
            />
            <PhotoBox
              containerStyle={{width: R.unit.scale(140)}}
              image={image4}
              onPress={() => openBottomSheet(4)}
              loading={loading4}
            />
          </View>

          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          <Button
            btnWrapperStyles={styles.button}
            onPress={() => validateData()}
            gutterTop={R.unit.scale(30)}
            text={R.strings.common.complete}
          />

          {/* <Button
            // btnWrapperStyles={styles.button}
            onPress={() => changePassword()}
            gutterTop={R.unit.scale(30)}
            text={'Change Password'}
          /> */}
        </View>
      </ScrollView>
      <SelectionSheet
        refRBSheet={refRBSheet}
        setImageUri={(uri: string) => setImageUri(uri)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: R.color.primary},
  root: {
    width: '80%',
    flex: 1,
    alignSelf: 'center',
    alignItems: 'center',
    paddingTop: R.unit.scale(40),
  },
  titleText: {marginTop: R.unit.scale(50)},
  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
  boxContainer: {
    flexDirection: 'row',
    width: '100%',
    height: R.unit.scale(140),
    justifyContent: 'space-between',
    marginTop: R.unit.scale(30),
  },
  optionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  sheetContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignContent: 'center',
    flexDirection: 'row',
  },
  optionImg: {width: '80%', height: '50%'},
  optionTxt: {
    textAlign: 'center',
    marginTop: R.unit.scale(15),
    fontWeight: 'bold',
  },
  touch: {
    flex: 1,
  },
});

export default ProfileStore(AddPhotos);
