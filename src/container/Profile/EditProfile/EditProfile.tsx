// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import MobileNumberInput from '@app/components/common/MobileNumberInput';
import BorderInput from '@app/components/common/BorderInput';
import OtpInputs from 'react-native-otp-inputs';
import SelectionView from '@app/components/common/SelectionView';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import ProfileStore from '../ProfileStore';
import CountryPicker from 'react-native-country-picker-modal';

// props
export interface IProps {
  navigation: any;
  route: any;

  updateDancerProfileAction: any;
  updateDancerProfileResponse: any;
  updateDancerProfileError: any;

  getMyProfileAction: any;

  updatePatronProfileAction: any;
  updatePatronProfileResponse: any;
  updatePatronProfileError: any;
}

const EditProfile: React.FC<IProps> = (props) => {
  // constants and veriables
  const tempData = props.route && props.route.params && props.route.params.userData;
  const [countryCode, setCountryCode] = useState(tempData.country_code || '1');
  const [loading, setLoading] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [visible, setVisible] = useState(false);
  const [data, setData] = useState({
    name: tempData.name,
    screenName: tempData.screen_name,
    dob: moment(tempData.dob).format('DD-MM-YYYY'),
    // dob : new Date(tempData.dob),
    gender: tempData.gender,
    websiteUrl: tempData.website_url,
    mobileNumber: tempData.phone,
    country_code: tempData.country_code,
  });

  // life cycle's

  // dancer response
  useEffect(() => {

    if (props.updateDancerProfileResponse && loading) {

      props.getMyProfileAction();
      setLoading(false);
      // props.navigation.navigate('Otp', { calledFrom: 'EDIT_PROFILE' });
      props.navigation.navigate('AllSet', {
        userData: {
          user_id: tempData.user_id,
          user_type: tempData.user_type,
        },
      });
    }
    if (props.updateDancerProfileError && loading) {

      alert(props.updateDancerProfileError);
      setLoading(false);
    }
  }, [props.updateDancerProfileResponse, props.updateDancerProfileError]);

  // patron response
  useEffect(() => {

    if (props.updatePatronProfileResponse && loading) {

      props.getMyProfileAction();
      setLoading(false);
      props.navigation.navigate('AllSet', {
        userData: {
          user_id: tempData.user_id,
          user_type: tempData.user_type,
        },
      });
      // props.navigation.navigate('Otp', { calledFrom: 'EDIT_PROFILE' });
    }
    if (props.updatePatronProfileError && loading) {

      alert(props.updatePatronProfileError)
      setLoading(false);
    }
  }, [props.updatePatronProfileResponse, props.updatePatronProfileError]);

  // user's action

  const onSelect = (country: any) => {

    // country selection
    setCountryCode(
      country.callingCode && country.callingCode[0]
        ? country.callingCode[0]
        : '1',
    );
  };

  const updateData = () => {

    setLoading(true);

    /**
     * construct post data 
     * check user type for update profile
     */
    let temp_data = {};

    let new_dob = data.dob;
    new_dob = new_dob.replace('-', '/');
    new_dob = new_dob.replace('-', '/');
    
    if (tempData.user_type === 'dancer') {

      temp_data = {
        name: data.name,
        dob: new_dob,

        gender: data.gender,
        screen_name: data.screenName,
        website_url: data.websiteUrl,
        // mobileNumber: data.mobileNumber,
        // country_code: countryCode,
      };
    } else {

      temp_data = {
        dob: new_dob,
        gender: data.gender,
        name: data.name,
      };
    }

    if (temp_data.dob) {

      temp_data.dob = moment(temp_data.dob).format('YYYY-DD-MM');
    }

    console.log('update profile data', temp_data)

    if (tempData.user_type == "patrone") {

      props.updatePatronProfileAction(temp_data);
    } else {

      props.updateDancerProfileAction(temp_data);
    }
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: any) => {
    hideDatePicker();
    setData({ ...data, dob: moment(date).format('DD-MM-YYYY') });
  };

  //---------- views

  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.edit_profile}
        navigation={props.navigation}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.root}>
          <BorderInput
            value={data.name}
            onChangeText={(text) => setData({ ...data, name: text })}
            placeholder={R.strings.profile.enter_name}
          />

          {/* *********** SCREEN NAME WILL ONLY SHOW TO DANCER *********** */}
          {
            tempData.user_type === 'dancer' &&
            <BorderInput
              value={data.screenName}
              onChangeText={(text) => setData({ ...data, screenName: text })}
              placeholder={R.strings.profile.on_screen_name}
            />
          }

          <BorderInput
            value={data.dob}
            onChangeText={(text) => setData({ ...data, dob: text })}
            placeholder={R.strings.profile.date_of_birth}
            rightComponent={
              <View>
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => showDatePicker()}>
                  <Image
                    style={styles.calendarImg}
                    resizeMode={'contain'}
                    source={R.image.calendar()}
                  />
                </TouchableOpacity>
              </View>
            }
          />

          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            // date={(moment(tempData.dob).format('DD-MM-YYYY'))}
            date={new Date(tempData.dob)}
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />
          <SelectionView
            title={R.strings.profile.select_gender}
            onPress={(gender: string) => setData({ ...data, gender: gender })}
            checkedIndex={data.gender}
            firstText={R.strings.profile.male}
            secondText={R.strings.profile.female}
          />

          <MobileNumberInput
            value={data.mobileNumber}
            onChangeText={(text) => setData({ ...data, mobileNumber: text })}
            placeholder={R.strings.auth.enter_mobile}
            countryCode={countryCode}
            onCodePress={() => setVisible(!visible)}
          />
          {/* *********** WEBSITE URL WILL ONLY SHOW TO DANCER *********** */}

          {
            tempData.user_type === 'dancer' &&
            <BorderInput
              value={data.websiteUrl}
              onChangeText={(text) => setData({ ...data, websiteUrl: text })}
              placeholder={R.strings.profile.enter_url}
            />
          }
          <CountryPicker
            {...{
              onSelect,
            }}
            placeholder={''}
            visible={visible}
          />

          <Button
            onPress={() => updateData()}
            gutterTop={R.unit.scale(30)}
            loading={loading}
            text={R.strings.profile.update_profile}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.primary },
  root: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  calendarImg: { height: R.unit.scale(22), width: R.unit.scale(22) },
});

export default ProfileStore(EditProfile);
