import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import UserAction from '@app/store/User/UserAction';
import ProfileAction from '@app/store/Profile/ProfileAction';
import AuthAction from '@app/store/Auth/AuthAction';

const mapStateToProps = (state: any) => ({
  userResponse: state.user.userResponse,

  addPhotoResponse: state.profile.addPhotoResponse,
  addPhotoError: state.profile.addPhotoError,

  getMyProfileResponse: state.profile.getMyProfileResponse,
  getMyProfileError: state.profile.getMyProfileError,

  updateDancerProfileResponse: state.profile.updateDancerProfileResponse,
  updateDancerProfileError: state.profile.updateDancerProfileError,

  updatePatronProfileResponse: state.profile.updatePatronProfileResponse,
  updatePatronProfileError: state.profile.updatePatronProfileError,

  changeUserTypeResponse: state.profile.changeUserTypeResponse,
  changeUserTypeError: state.profile.changeUserTypeError
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      userAction: UserAction.userAction,
      addPhotoAction: ProfileAction.addPhotoAction,
      showHeadersAction: ProfileAction.showHeadersAction,
      getMyProfileAction: ProfileAction.getMyProfileAction,
      changePasswordAction: AuthAction.changePasswordAction,
      updateDancerProfileAction: ProfileAction.updateDancerProfileAction,
      updatePatronProfileAction: ProfileAction.updatePatronProfileAction,
      changeUserType: ProfileAction.changeUserType
    },
    dispatch,
  );

export default (Auth: any) => {
  return connect(mapStateToProps, mapDispatchToProps)(Auth);
};
