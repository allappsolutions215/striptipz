// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, ScrollView, Alert } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import AccountItem from '@app/components/profile/AccountItem';
import CancelSubscriptionModal from '@app/components/layout/CancelSubscriptionModal';
import ProfileStore from '../ProfileStore';
import FingerprintScanner from 'react-native-fingerprint-scanner';

export interface IProps {
  navigation: any;
  userAction: any;
  userResponse: any;
  changeUserTypeResponse: any;
  changeUserTypeError: any;
  changeUserType: any;
}

const MyAccount: React.FC<IProps> = (props) => {
  // veriable
  let list = []
  const [showModal, setShowModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [enableLock, setEnableLock] = useState(props.userResponse && props.userResponse.enable_lock);

  // constants
  const [isPayPalConnected, setIsPayPalConnected] = useState(props.userResponse && props.userResponse.is_paypal_connect);
  const [isSubscriptionValid, setIsSubscriptionValid] = useState(props.userResponse && props.userResponse.is_subscription_valid);
  const [userType, setUserType] = useState(props.userResponse && props.userResponse.user_type && props.userResponse.user_type.toLowerCase())

  const dancerItems = [
    R.strings.profile.my_profile,
    R.strings.profile.change_password,
    R.strings.profile.manage_payment,
    isSubscriptionValid === 1 ? R.strings.profile.cancel_subscription : R.strings.profile.add_subscription,
    enableLock ? R.strings.profile.disable_lock : R.strings.profile.enable_lock,
    R.strings.profile.become_a_patron
  ];

  const patroneItems = [
    R.strings.profile.my_profile,
    R.strings.profile.change_password,
    isPayPalConnected === 1 ? R.strings.profile.add_amount : R.strings.payPal.link_pay_pal,
    R.strings.profile.manage_payment_account,
    R.strings.profile.transaction_history,
    enableLock ? R.strings.profile.disable_lock : R.strings.profile.enable_lock,
    R.strings.profile.become_a_dancer
  ];

  useEffect(() => {

    if (props.userResponse) {

      setIsPayPalConnected(props.userResponse && props.userResponse.is_paypal_connect);
      setIsSubscriptionValid(props.userResponse && props.userResponse.is_subscription_valid);
      setUserType(props.userResponse && props.userResponse.user_type && props.userResponse.user_type.toLowerCase());
    }
  }, [props.userResponse]);

  // helper's

  const handleUsersAction = async (type: string) => {

    let alert_obj
    switch (type) {
      case R.strings.profile.my_profile:
        props.navigation.navigate('MyProfile');
        break;
      case R.strings.profile.change_password:
        props.navigation.navigate('ChangePassword');
        break;
      case R.strings.profile.manage_payment:
        props.navigation.navigate('ManagePayment');
        break;
      case R.strings.profile.manage_payment_account:
        props.navigation.navigate('ManagePayment');
        break;
      case R.strings.profile.cancel_subscription:
        setShowModal(true);
        break;
      case R.strings.profile.add_subscription:
        props.navigation.navigate('Subscription');
        break;
      case R.strings.profile.add_amount:
        props.navigation.navigate('AddAmount');
        break;
      case R.strings.payPal.link_pay_pal:
        props.navigation.navigate('LinkPayPal');
        break;

      case R.strings.profile.transaction_history:
        props.navigation.navigate('TransactionHistory');
        break;
      case R.strings.profile.enable_lock:

        /**
         * lock/unlock action
         * save lock app option in user response pocker
         */

        if (await checkSensorIsavailable()) {

          props.userAction({
            ...props.userResponse,
            enable_lock: true
          });
          setEnableLock(true)
        } else {

          alert_obj = {
            title: R.strings.error,
            desc: R.strings.profile.enable_lock_error,
          }
          setupAlertMsg(alert_obj, 'check_sensor')
        }

        break;

      case R.strings.profile.disable_lock:

        /**
         * lock/unlock action
         * save lock app option in user response pocker
         */

        props.userAction({
          ...props.userResponse,
          enable_lock: false
        });
        setEnableLock(false)
        break;

      case R.strings.profile.become_a_patron:

        // for title and desc on alert
        alert_obj = {
          title: R.strings.app_name,
          desc: R.strings.profile.change_user_type_patrone,
        }
        setupAlertMsg(alert_obj, 'patrone')
        break;

      case R.strings.profile.become_a_dancer:

        // for title and desc on alert
        alert_obj = {
          title: R.strings.app_name,
          desc: R.strings.profile.change_user_type_dancer,
        }
        setupAlertMsg(alert_obj, 'dancer')
        break;

      case 'signout':

        // for title and desc on alert
        alert_obj = {
          title: R.strings.app_name,
          desc: R.strings.error.are_you_sure_sign_out
        }
        setupAlertMsg(alert_obj, 'signout')
        break;

    }
  };

  // setup alert for change user_type
  const setupAlertMsg = (data: any, key: any) => {
    Alert.alert(
      data.title,
      data.desc,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            handleAlertMsgClick(key)
          },
        },
      ],
      { cancelable: false },
    );
  }

  const handleModalOption = (status: any) => {

    // get status in boolean as per selection
    if (status) {

      console.log('status', status)
    }
    setShowModal(false);
  }

  // handle change of user_type 
  const handleAlertMsgClick = (key) => {

    /**
     * take action for dancer/patrone/signout
     * take redux action for change user_type
     * set loading for stop continous update
     * check response in useEffect
     */
    if (key === 'dancer') {

      props.changeUserType('dancer')
      setLoading(true);
    }
    if (key === 'patrone') {

      props.changeUserType('patrone')
      setLoading(true);
    }
    if (key === 'signout') {

      props.userAction({ fcm_token: props.userResponse.fcm_token });
      props.navigation.reset({
        index: 0,
        routes: [
          {
            name: 'Root',
          },
        ],
      });
    }
  }

  // check sensor 
  const checkSensorIsavailable = async () => {

    let sensor = false

    await FingerprintScanner.isSensorAvailable().then(biometryType => {

      sensor = true
    }).catch((error) => {

      sensor = false
    })
    return sensor
  }

  // life cycle
  useEffect(() => {

    // get previous user profile response
    let previous_user_pocket = props.userResponse

    // check loading and change user response
    if (loading && props.changeUserTypeResponse) {

      // replace previous values with new user_type
      Object.assign(previous_user_pocket, { ...props.changeUserTypeResponse })

      // save data in user response for current user_type
      props.userAction({
        ...previous_user_pocket
      });

      // redirect or start app again
      props.navigation.navigate('Root');
      setLoading(false);

      // error case
    } else if (props.changeUserTypeError) {

      setLoading(false);
    }
  }, [props.changeUserTypeResponse, props.changeUserTypeError]);


  // assign value to verible as per user type
  userType === 'dancer' ? list = dancerItems : list = patroneItems

  //---------- views
  return (
    <View style={styles.container}>
      <AuthHeader
        text={R.strings.profile.my_account}
        navigation={props.navigation}
        color={R.color.black}
        tintColor={R.color.black}
      />
      <ScrollView
        contentContainerStyle={styles.container}
        style={styles.container}>
        <View style={styles.root}>

          {
            list.map((item, index) => {

              return (
                <AccountItem
                  key={index}
                  title={item}
                  onPress={(item: string) => handleUsersAction(item)}
                />
              );
            })
          }

          <Button
            btnWrapperStyles={styles.button}
            onPress={() => handleUsersAction('signout')}
            //onPress={() => alert('In Progress')}
            gutterTop={R.unit.scale(30)}
            text={R.strings.auth.sign_out}
            leftSectionStyle={{ position: 'relative', marginEnd: 20 }}
          />

          <CancelSubscriptionModal
            onPress={(status: boolean) => handleModalOption(status)}
            isVisible={showModal}
          />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.white },
  root: {
    width: '85%',
    flex: 1,
    alignSelf: 'center',
    marginTop: R.unit.scale(50),
  },

  button: {
    bottom: R.unit.scale(30),
    alignSelf: 'center',
    position: 'absolute',
  },
});

export default ProfileStore(MyAccount);
