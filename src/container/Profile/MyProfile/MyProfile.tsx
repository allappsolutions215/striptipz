// main imports
import React, { RefObject, useRef, useState, useEffect } from 'react';
import R from '../../../res/R';
// components
import { StyleSheet, View, ScrollView } from 'react-native';
import Button from '@app/components/common/Button';
import AuthHeader from '@app/components/auth/AuthHeader';
import Text from '@app/components/common/Text';
import AccountItem from '@app/components/profile/AccountItem';
import PhotoBox from '@app/components/profile/PhotoBox';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import SelectionSheet from '@app/components/layout/SelectionSheet';
import ProfileStore from '../ProfileStore';
import moment from 'moment';
import ShakeText from '@app/components/layout/ShakeText';
import { utils } from '@app/util/utils';
import config from '@app/config';

export interface IProps {
  navigation: any;

  userResponse: any;
  userAction: any;

  addPhotoAction: any;
  addPhotoResponse: any;
  addPhotoError: any;

  getMyProfileAction: any;
  getMyProfileResponse: any;
  getMyProfileError: any;
}

const MyProfile: React.FC<IProps> = (props) => {
  const refRBSheet = useRef();

  const [image1, setImage1] = useState('');
  const [image2, setImage2] = useState('');
  const [image3, setImage3] = useState('');
  const [image4, setImage4] = useState('');
  const [loading1, set1Loading] = useState(false);
  const [loading2, set2Loading] = useState(false);
  const [loading3, set3Loading] = useState(false);
  const [loading4, set4Loading] = useState(false);
  const [formError, setFormError] = useState('');
  const [startAnimation, setStartAnimation] = useState(false);
  const [selectedIndex, setSelectedIndex] = useState(0);
  const userData = props.userResponse ? props.userResponse : {};
  const openBottomSheet = (index: number) => {
    setSelectedIndex(index);
    refRBSheet.current.open();
  };

  const inputTitles = [
    R.strings.profile.name,
    //Dancer has {on_screen_name} diffrent field:
    userData && userData.user_type === 'dancer'
      ? R.strings.profile.on_screen_name
      : false,
    R.strings.profile.date_of_birth,
    R.strings.profile.gender,
    R.strings.profile.mobile_number,
  ];

  const value = [
    userData && userData.name ? userData.name : '',
    //Dancer has {on_screen_name} diffrent field:
    userData && userData.screen_name ? userData.screen_name : '',
    userData && userData.dob
      ? moment(userData.dob).format('DD - MM - YYYY')
      : '',
    userData && userData.gender ? userData.gender : '',
    userData && userData.phone ? userData.phone : '',
  ];

  const setImageUri = (uri: string) => {

    console.log('selected imgae url', uri);
    if (selectedIndex === 1) {
      set1Loading(true);
      setImage1(uri);
      props.addPhotoAction({ uri: uri, index: 1 });
    } else if (selectedIndex === 2) {
      set2Loading(true);
      setImage2(uri);
      props.addPhotoAction({ uri: uri, index: 2 });
    } else if (selectedIndex === 3) {
      set3Loading(true);
      setImage3(uri);
      props.addPhotoAction({ uri: uri, index: 3 });
    } else if (selectedIndex === 4) {
      set4Loading(true);
      setImage4(uri);
      props.addPhotoAction({ uri: uri, index: 4 });
    }
  };

  useEffect(() => {
    props.getMyProfileAction();
    if (
      props.userResponse &&
      props.userResponse.images &&
      props.userResponse.images.length > 0
    ) {
      props.userResponse.images.map((item: any, index: any) => {
        if (item.image) {
          index === 0
            ? setImage1(config.imageBaseUrl + item.image)
            : index === 1
              ? setImage2(config.imageBaseUrl + item.image)
              : index === 2
                ? setImage3(config.imageBaseUrl + item.image)
                : setImage4(config.imageBaseUrl + item.image);
        }
      });
    }
  }, []);

  useEffect(() => {
    if (props.getMyProfileResponse) {
      if (
        props.getMyProfileResponse.data ||
        props.getMyProfileResponse.images
      ) {
        props.userAction({
          ...props.userResponse,
          ...props.getMyProfileResponse.data,
          ...props.getMyProfileResponse.images,
        });
      }
    } else if (props.getMyProfileError) {
      setFormError(utils.getErrorMessage(props.getMyProfileError));
      setStartAnimation(true);
    }
  }, [props.getMyProfileResponse, props.getMyProfileError]);

  useEffect(() => {
    if (loading1 || loading2 || loading3 || loading4) {
      if (props.addPhotoResponse) {
        setFormError('');
        let index = props.addPhotoResponse.index;
        index === 1
          ? set1Loading(false)
          : index === 2
            ? set2Loading(false)
            : index === 3
              ? set3Loading(false)
              : set4Loading(false);

        props.userAction({
          ...props.userResponse,
          image_count: props.userResponse.image_count
            ? props.userResponse.image_count + 1
            : 1,
        });
      } else if (props.addPhotoError) {

        // let error = utils.getErrorMessage(props.addPhotoError)
        // console.log('00=0-==0=-0=0=>', error)
        setFormError(utils.getErrorMessage(props.addPhotoError));
        setStartAnimation(true);
      }
    }
  }, [props.addPhotoResponse, props.addPhotoError]);

  return (
    <View style={styles.container}>
      <AuthHeader
        showEditIcon
        text={R.strings.profile.my_profile}
        navigation={props.navigation}
        color={R.color.black}
        tintColor={R.color.black}
        onEditPress={() => props.navigation.navigate('EditProfile', { userData: userData })}
      />
      <ScrollView
        contentContainerStyle={styles.container}
        style={styles.container}>
        <View style={styles.root}>
          {
            userData.user_type === 'dancer' ?
              <View style={styles.boxContainer}>
                {
                  [1, 2, 3, 4].map((item, index) => (
                    <PhotoBox
                      containerStyle={{ flex: 1, marginHorizontal: R.unit.scale(5) }}
                      borderColor={R.color.gray1}
                      backgroundColorLine={R.color.gray1}
                      loading={
                        item === 1
                          ? loading1
                          : item === 2
                            ? loading2
                            : item === 3
                              ? loading3
                              : loading4
                      }
                      image={
                        item === 1
                          ? image1
                          : item === 2
                            ? image2
                            : item === 3
                              ? image3
                              : image4
                      }
                      onPress={() => openBottomSheet(item)}
                    />
                  ))
                }
              </View>
              :
              null
          }
          <ShakeText
            startAnimation={startAnimation}
            title={formError}
            stopAnimation={() => setStartAnimation(false)}
          />

          {inputTitles.map((item, index) => {
            //item will be false/undefined if user_type is patron bcoz onScreenName is not a field for Patron
            return item ? (
              <AccountItem title={item} value={value[index]} />
            ) : null;
          })}
        </View>
      </ScrollView>
      <SelectionSheet
        refRBSheet={refRBSheet}
        setImageUri={(uri: string) => setImageUri(uri)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: R.color.white },
  root: {
    width: '85%',
    flex: 1,
    alignSelf: 'center',
    marginTop: R.unit.scale(20),
  },
  boxContainer: {
    flexDirection: 'row',
    width: '100%',
    height: R.unit.scale(70),
    justifyContent: 'space-between',
    marginTop: R.unit.scale(30),
  },
});

export default ProfileStore(MyProfile);
