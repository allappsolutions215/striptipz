import React, {useState, useEffect} from 'react';
import {View, StatusBar} from 'react-native';

import {Provider, connect} from 'react-redux';
import store from './src/store';
import Navigation from './src/navigation';

import {PersistGate} from 'redux-persist/integration/react';
import {persistStore, persistReducer} from 'redux-persist';
import R from '@app/res/R';
import SplashScreen from 'react-native-splash-screen';

import NetInfo from '@react-native-community/netinfo';
import NoInternetView from '@app/components/layout/NoInternetView';
import {decode, encode} from 'base-64';

// push notification
import PushNotification from './src/container/Notification/PushNotification'

export default function App(props: any) {
  const [isConnected, setIsConnected] = useState(true);

  useEffect(() => {
    SplashScreen.hide();
    if (!global.btoa) {
      global.btoa = encode;
      }
      
      if (!global.atob) {
      global.atob = decode;
      }
    const unsubscribe = NetInfo.addEventListener((state) => {
      console.log('Connection type', state.type);
      console.log('Is connected?', state.isConnected);
      setIsConnected(state.isConnected ? state.isConnected : false);
    });
    return () => unsubscribe();
  }, []);

  let persistor = persistStore(store);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        {!isConnected && <NoInternetView />}
        <Navigation />
        <PushNotification />
      </PersistGate>
    </Provider>
  );
}
